package cc.gemii.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.SysUser;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.SysUserService;
import cc.gemii.utils.CommonUtil;

@Controller
@RequestMapping("/sysuser")
public class SysUserController {
	
	@Autowired
	private SysUserService sysUserService;
	

	/**
	 * 后台登录
	 * @return
	 */
	@RequestMapping(value="/login", method=RequestMethod.POST)
	@ResponseBody
	public CommonResult login(@RequestParam String username, @RequestParam String password, HttpSession session){
		if(!CommonUtil.hasText(username) || !CommonUtil.hasText(password)){
			return CommonResult.build(500, "参数异常");
		}
		SysUser sysUser = sysUserService.selectForLogin(username, password);
		if(sysUser != null){
			session.setAttribute("user", sysUser);
			return CommonResult.ok();
		}
		return CommonResult.build(400, "登录名或密码错误");
	}
	
	/**
	 * 后台退出登录
	 * @return
	 */
	@RequestMapping(value="/loginOut")
	@ResponseBody
	public CommonResult loginOut(HttpSession session){
		//清除session
		session.invalidate();
		return CommonResult.ok();
	}
}
