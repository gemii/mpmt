package cc.gemii.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Dict;
import cc.gemii.po.TaskMemberRecord;
import cc.gemii.po.TasksWithBLOBs;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.PagingPOJO;
import cc.gemii.service.CommonService;
import cc.gemii.service.DictService;
import cc.gemii.service.ImageUploadService;
import cc.gemii.service.TaskMemberRecordService;
import cc.gemii.service.TasksService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.StrConsts;

/**
 * 任务领取记录控制器
 * @author xudj20170207
 */
@Controller
@RequestMapping("/tmrecord")
public class TaskMemberRecordController {
	
	private static final Logger log = LoggerFactory.getLogger(TaskMemberRecordController.class);
	
	@Autowired
	private TaskMemberRecordService taskMemberRecordService;
	
	@Autowired
	private ImageUploadService imageUploadService;
	
	@Autowired
	private TasksService tasksService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private CommonService commonService;
	
	/**
	 * 点击领取任务
	 * @author xudj20170214
	 * @param memberId：用户标识unionid
	 * @param taskId：任务id
	 */
	@RequestMapping("/receiveTasks")
	@ResponseBody
	public CommonResult receiveTasks(@RequestParam String memberId, @RequestParam Integer taskId){
		//参数校验
		if(!CommonUtil.hasText(memberId) || !CommonUtil.hasText(taskId)){
			return CommonResult.build(Integer.valueOf(500), StrConsts.PARAMETER_ERROR);
		}
		//判断任务是否已下架
		String isShelve = tasksService.selectTasksIsShelveById(taskId);
		if(isShelve == null || isShelve.equals("F")){
			return CommonResult.build(Integer.valueOf(400), "任务已下架");
		}
		//判断试用任务的申领是否到期限制
		TasksWithBLOBs tasksWithBLOBs = tasksService.queryTaskById(taskId);
		//查询Dict
		Dict dict = dictService.getDictById(tasksWithBLOBs.getTypeId());
		if("2".equals(dict.getName())){//试用类型的
			if(tasksWithBLOBs.getApplyEndDate() != null && tasksWithBLOBs.getApplyEndDate().before(new Date())){
				return CommonResult.build(Integer.valueOf(400), "申领已过期");
			}else if(tasksWithBLOBs.getApplyCount() != null && tasksWithBLOBs.getApplyCount() <= 0){
				return CommonResult.build(Integer.valueOf(400), "申领名额已用完");
			}
		}
		//添加数据
		return taskMemberRecordService.insertTaskMemberRecord(memberId,taskId);
	}
	
	/**
	 * 提交任务，上传任务图片
	 * @change xudj20170214
	 * @param data:上传的图片的ids
	 * @param taskMemberRecord：包括memberId及taskId
	 */
	@RequestMapping("/updatePutTask")
	@ResponseBody
	public CommonResult updatePutTask(String[] data, TaskMemberRecord taskMemberRecord){
		if(!CommonUtil.hasText(taskMemberRecord.getMemberId()) || !CommonUtil.hasText(taskMemberRecord.getTaskId())
				|| !CommonUtil.hasText(data)){
			return CommonResult.build(Integer.valueOf(500), StrConsts.PARAMETER_ERROR);
		}
		//判断任务是否已下架
		String isShelve = tasksService.selectTasksIsShelveById(taskMemberRecord.getTaskId());
		if(isShelve == null || isShelve.equals("F")){
			return CommonResult.build(Integer.valueOf(400), "任务已下架");
		}
		//是否已经提交过了
		TaskMemberRecord record = taskMemberRecordService.selectByTaskIdAndMemberId(taskMemberRecord.getTaskId(), taskMemberRecord.getMemberId());
		if(record == null){
			return CommonResult.build(Integer.valueOf(500), StrConsts.HTTP_REQUEST_ERROR);
		}else if(record.getState() != 0){
			return CommonResult.build(400, "请勿重复提交");//已提交的
		}
		//开始文件上传
		String outId = imageUploadService.updateImagesOutId(data);
		if(outId == null){
			log.error("提交任务异常，获取上传图片外键为空，其memberId:{}", taskMemberRecord.getMemberId());
			return CommonResult.build(400, "提交异常,请刷新重试");
		}
		//进行记录的更新
		taskMemberRecord.setOutId(outId);
		taskMemberRecord.setFinishTime(new Date());
		taskMemberRecord.setState(1);//审核中
		String remark = taskMemberRecord.getRemark();
		if(CommonUtil.hasText(remark)){//处理表情
			taskMemberRecord.setRemark(commonService.handleEmoji(taskMemberRecord.getRemark()));
		}
		return taskMemberRecordService.updatePutTask(taskMemberRecord);
	}
	
	/**
	 * 查询个人中心历史任务列表
	 * @param pagingPojo：分页
	 * @param memberId：用户unionid
	 * @return
	 */
	@RequestMapping("/listTaskMembersByMemberId")
	@ResponseBody
	public Map<String, Object> listTaskMembersByMemberId(PagingPOJO pagingPojo, @RequestParam String memberId){
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("result", true);
		resMap.put("page", this.taskMemberRecordService.listTaskMembersByMemberId(pagingPojo, memberId));
		return resMap;
	}
	
	
	
	/*======================= 后台管理方法 ======================*/
	/**
	 * 审核微任务
	 * 	点击审核通过(T) 或者不通过(F)
	 * @author lvshilin20170210
	 * @change xudj20170217
	 * @param id：记录id
	 * @param result：是否审核通过
	 */
	@RequestMapping("/web/verifyTaskPut")
	@ResponseBody
	public CommonResult verifyTaskPut(@RequestParam Long id, @RequestParam String result){
		if(!CommonUtil.hasText(id) || !CommonUtil.hasText(result)){
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		//审核
		return taskMemberRecordService.updateResult(id,result);
	}
	
	/**
	 * 查询领取任务列表 
	 * @author lvshilin20170215
	 * @change xudj20170217
	 */
	@RequestMapping("/web/selectTasksMembers")
	@ResponseBody
	public CommonResult selectTasksMembers(@RequestParam Integer taskId) {
		return this.taskMemberRecordService.selectTasksMembers(taskId);
	}
	
	/**
	 * 查看任务详情
	 * @author lvshilin20170216
	 * @change xudj20170217
	 */
	@RequestMapping("/web/selectTaskPutDetail")
	@ResponseBody
	public CommonResult selectTaskPutDetail(@RequestParam Long id){
		if(!CommonUtil.hasText(id)){
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		return taskMemberRecordService.selectTaskPutDetail(id);
	}
	/**
	 * 批量审核微任务
	 * 	批量点击审核通过(T) 或者不通过(F)
	 * @author zsl
	 * @param ids：记录ids数组
	 * @param result：是否审核通过T F
	 */
	@RequestMapping("/web/verifyTasksPut")
	@ResponseBody
	public CommonResult verifyTasksPut(Long[] ids,String result,HttpServletRequest request){
		if(!CommonUtil.hasText(ids) || !CommonUtil.hasText(result)){
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		try {
			for(int i=0;i<ids.length;i++){
				 taskMemberRecordService.updateResult(ids[i],result);
			}
		} catch (Exception e) {
		 	// TODO: handle exception
			return CommonResult.build(400, e.getMessage());
		}
		return CommonResult.ok();
	}
}
