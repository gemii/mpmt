package cc.gemii.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.service.ImageUploadService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.FileUtil;
import cc.gemii.utils.StrConsts;

/**
 * 图片上传控制器
 * @author xudj20170207
 */
@Controller
@RequestMapping("/iupload")
public class ImageUploadController {
	
	@Autowired
	ImageUploadService imageUploadService;
	
	/**
	 * 通过base64上传图片
	 * @param data：base64数据
	 * @param taskMemberRecord
	 * @param response
	 * @return
	 */
	@RequestMapping("/uploadImage")
	@ResponseBody
	public CommonResult uploadImage(String data){
		if(!CommonUtil.hasText(data)){//数据校验
			return CommonResult.build(Integer.valueOf(500), StrConsts.PARAMETER_ERROR);
		}
		return imageUploadService.insertImage(data, FileUtil.TASKS_UPLOAD_DIR);
	}
	
}
