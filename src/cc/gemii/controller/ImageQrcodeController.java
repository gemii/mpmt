package cc.gemii.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Dict;
import cc.gemii.po.ImageQrcode;
import cc.gemii.po.SysUser;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.ImageQrcodeDTO;
import cc.gemii.service.DictService;
import cc.gemii.service.ImageQrcodeService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.StrConsts;

/**
 * 场景二维码管理(后台功能)
 * @author xudj20170327
 */
@Controller
@RequestMapping("/iqrcode")
public class ImageQrcodeController {

	@Autowired
	private ImageQrcodeService imageQrcodeService;
	
	@Autowired
	private DictService dictService;
	
	/**
	 * 新增场景二维码
	 * @param imageQrcode：二维码
	 * @param sceneValue：投放场景值
	 * @return
	 */
	@RequestMapping("/web/add")
	@ResponseBody
	public CommonResult addImageQrcode(ImageQrcode imageQrcode, String sceneValue, HttpSession session){
		//获取用户身份
		SysUser user = (SysUser) session.getAttribute("user");
		//参数校验
		if(!CommonUtil.hasText(sceneValue)){
			return CommonResult.build(500, StrConsts.PARAMETER_ERROR);
		}
		//新增字典表,获取主键
		Dict dict = new Dict();
		dict.setType(StrConsts.DICT_TYPE_FANS_FW_SOURCE);
		//粉丝来源dict—name
		String dictSourceName = imageQrcode.getDescription() + "_" + sceneValue;
		dict.setName(dictSourceName);
		CommonResult cr = dictService.insertDictForQrcode(dict, imageQrcode);
		if(cr.getStatus() != 200){//异常处理
			return cr;
		}
		//新增二维码
		return imageQrcodeService.insertImageQrcode(imageQrcode, dict, user);
	}
	
	/**
	 * 查询详细
	 * @param id：二维码
	 * @author xudj20170328
	 * @return
	 */
	@RequestMapping("/web/view")
	@ResponseBody
	public CommonResult viewImageQrcode(@RequestParam Integer id){
		return imageQrcodeService.selectDetailById(id);
	}
	
	/**
	 * 更新二维码文件信息（包括删除）
	 * @author xudj20170328
	 * @param imageQrcode:更新的数据
	 */
	@RequestMapping("/web/update")
	@ResponseBody
	public CommonResult updateImageQrcode(ImageQrcode imageQrcode, String sceneValue, HttpSession session){
		//校验id
		if(!CommonUtil.hasText(imageQrcode.getId())){
			return CommonResult.build(500, StrConsts.PARAMETER_ERROR);
		}
		SysUser user = (SysUser)session.getAttribute("user");//登录用户
		imageQrcode.setUpdateUser(user.getId());
		//更新来源dict—name
		String description = imageQrcode.getDescription();
		Integer dictSourceId = imageQrcode.getDictSourceId();
		if(CommonUtil.hasText(description) && sceneValue != null && dictSourceId != null){//非删除
			dictService.updateDictForFansSource(description, dictSourceId, sceneValue);
		}
		//更新
		return imageQrcodeService.updateImageQrcode(imageQrcode);
	}
	
	/**
	 * <p>Description: 删除</p>
	 * @author xudj
	 * @date 2017年4月10日 下午2:25:43
	 * @param imageQrcode
	 * @param session
	 * @return
	 */
	@RequestMapping("/web/del")
	@ResponseBody
	public CommonResult updateImageQrcode(ImageQrcode imageQrcode, HttpSession session){
		//校验id
		if(!CommonUtil.hasText(imageQrcode.getId())){
			return CommonResult.build(500, StrConsts.PARAMETER_ERROR);
		}
		return imageQrcodeService.delImageQrcode(imageQrcode);
	}
	
	/**
	 * 条件查询二维码管理列表
	 * @param imageQrcodeDTO：查询条件（分页）
	 * @author xudj20170406
	 */
	@RequestMapping("/web/list")
	@ResponseBody
	public Map<String, Object> listImageQrcode(ImageQrcodeDTO imageQrcodeDTO){
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("result", true);
		resMap.put("page", imageQrcodeService.listImageQrcode(imageQrcodeDTO));
		return resMap;
	}
	
	/**
	 * 根据文件名及路径加载文件
	 * @param fileName:文件名
	 * @param response
	 */
	@RequestMapping("/web/download")
	public void downloadImageQrcode(HttpServletResponse response, @RequestParam String fileName){
		try {
			//设置header
		    response.setHeader("Pragma", "No-cache");   
		    response.setHeader("Cache-Control", "no-cache");
		    response.setDateHeader("Expires", 0L);
			OutputStream os = response.getOutputStream();
			os.write(imageQrcodeService.downloadImageQrcode(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * <p>Description: 查询扫码数据列表</p>
	 * @author xudj
	 * @date 2017年4月10日 上午9:48:31
	 * @param imageQrcodeDTO：查询条件
	 * @return
	 */
	@RequestMapping("/web/listScan")
	@ResponseBody
	public Map<String, Object> listScan(ImageQrcodeDTO imageQrcodeDTO){
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("result", true);
		resMap.put("page", imageQrcodeService.listScanFans(imageQrcodeDTO));
		return resMap;
	}
	
}
