package cc.gemii.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.service.MatterFwService;

/**
 * 服务号素材控制器
 * @author xudj20170407
 */
@Controller
@RequestMapping("/matterfw")
public class MatterFwController {

	@Autowired
	private MatterFwService matterFwService;
	
	/**
	 * 根据类型查询素材
	 * @author xudj20170407
	 * @return
	 */
	@RequestMapping("/web/list")
	@ResponseBody
	public CommonResult selectMatterFwByType(@RequestParam String type){
		return matterFwService.selectMatterFwByType(type);
	}
	
}
