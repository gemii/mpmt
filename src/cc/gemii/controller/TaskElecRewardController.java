package cc.gemii.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.SysUser;
import cc.gemii.po.TaskElecReward;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.PagingPOJO;
import cc.gemii.pojo.TaskElecRewardOTD;
import cc.gemii.service.TaskElecRewardService;

/**
 * <p>ClassName: TaskElecRewardController</p>
 * <p>Description: 微任务卡券控制器</p>
 * <p>Company: gemii</p>
 * @author xudj
 * @date 2017年4月24日 下午3:18:00
 */
@Controller
@RequestMapping("/tereward")
public class TaskElecRewardController {
	
	@Autowired
	private TaskElecRewardService taskElecRewardService;
	
	/**
	 * <p>Description: 新增</p>
	 * @author xudj
	 * @date 2017年4月24日 下午3:48:57
	 * @return
	 */
	@RequestMapping(value="/web/insert", method=RequestMethod.POST)
	@ResponseBody
	public CommonResult insert(TaskElecReward taskElecReward, HttpSession session){
		// 登录信息
		SysUser user = (SysUser) session.getAttribute("user");
		// 插入
		return taskElecRewardService.insert(taskElecReward, user.getId());//
	}
	
	/**
	 * <p>Description: 分页查询卡券列表</p>
	 * @author xudj
	 * @date 2017年4月24日 下午5:51:57
	 * @return
	 */
	@RequestMapping("/web/list")
	@ResponseBody
	public CommonResult list(TaskElecRewardOTD taskElecRewardOTD){
		return taskElecRewardService.list(taskElecRewardOTD);
	}
	

	/**
	 * <p>Description: 查看卡券详细</p>
	 * @author xudj
	 * @date 2017年4月25日 下午3:06:07
	 * @return
	 */
	@RequestMapping("/web/select/{id}")
	@ResponseBody
	public CommonResult selectDescrition(@PathVariable(value="id") Integer id){
		return taskElecRewardService.selectDescrition(id);
	}
	
	/**
	 * <p>Description: 更新卡券</p>
	 * @author xudj
	 * @date 2017年4月25日 下午3:13:48
	 * @param taskElecRewardOTD：需更新的信息
	 * @return
	 */
	@RequestMapping(value="/web/update", method=RequestMethod.POST)
	@ResponseBody
	public CommonResult update(TaskElecReward taskElecReward, HttpSession session){
		// 登录用户
		SysUser user = (SysUser) session.getAttribute("user");
		taskElecReward.setUpdateUser(user.getId());//
		return taskElecRewardService.update(taskElecReward);
	}
	
	/**
	 * <p>Description: 获取卡券详情</p>
	 * @author xudj
	 * @date 2017年4月28日 上午10:00:09
	 * @param id
	 * @return
	 */
	@RequestMapping("/web/selectElecRecord/{pageNo}/{taskId}")
	@ResponseBody
	public CommonResult selectElecRecord(@PathVariable Integer pageNo, @PathVariable Integer taskId){
		PagingPOJO pagingPOJO = new PagingPOJO();
		pagingPOJO.setPageNo(pageNo);
		return taskElecRewardService.selectElecRecord(pagingPOJO, taskId);
	}
	
	/**
	 * <p>Description: 查询所有卡券：id为key，name为value</p>
	 * @author xudj
	 * @date 2017年5月3日 上午11:42:24
	 * @return
	 */
	@RequestMapping("/web/selectAllElecReward")
	@ResponseBody
	public CommonResult selectAllElecReward(){
		return taskElecRewardService.selectAllElecReward();
	}
	
	
	/**
	 * <p>Description: 进入卡券的链接</p>
	 * @author xudj
	 * @date 2017年4月28日 下午2:28:23
	 * @param id：微任务id
	 * @param openid：用户openid
	 * @return
	 */
	@RequestMapping("/loadElecUrl")
	public String loadElecInfo(Integer id, String openid){
		// 查询根据微任务id查询卡券信息
		TaskElecReward taskElecReward = taskElecRewardService.selectElecRewardByTaskId(id);
		// 添加点击记录
		taskElecRewardService.insertElecRecord(id, openid);
		return "redirect:" + taskElecReward.getElecUrl();
	}
	
}
