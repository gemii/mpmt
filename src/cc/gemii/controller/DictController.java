package cc.gemii.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Dict;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.DictService;

/**
 * 字典控制器
 * @author xudj20170217
 */
@Controller
@RequestMapping("/dict")
public class DictController {
	
	@Autowired
	private DictService dictService;
	
	/**
	 * 根据类型查询dict
	 * @return
	 */
	@RequestMapping("/web/getDictsByType")
	@ResponseBody
	public CommonResult getDictsByType(@RequestParam String type){
		List<Dict> dicts = dictService.getDictsByType(type);
		return CommonResult.ok(dicts);
	}

	
	/**
	 * 加载个人中心栗子说明页
	 * @author xudj20170309
	 * @return
	 */
	@RequestMapping("/getChestnutExplain")
	@ResponseBody
	public CommonResult getChestnutExplain(){
		return dictService.getChestnutExplain();
	}
	
}
