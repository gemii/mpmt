package cc.gemii.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.TaskMemberRecord;
import cc.gemii.po.TasksWithBLOBs;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.TaskSearchPOJO;
import cc.gemii.service.DictService;
import cc.gemii.service.TaskMemberRecordService;
import cc.gemii.service.TasksService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.StrConsts;

/**
 * 微任务控制器
 * 
 * @author xudj20170207
 */
@Controller
@RequestMapping("/tasks")
public class TasksController {

	@Autowired
	TasksService tasksService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private TaskMemberRecordService taskMemberRecordService;

	/**
	 * 初始化领取任务界面信息
	 * 
	 * @author xudj20170214
	 * @return
	 */
	@RequestMapping("/receiveTasksInit")
	@ResponseBody
	public CommonResult receiveTasksInit(@RequestParam Integer taskId, String memberId) {
		if (!CommonUtil.hasText(taskId) || !CommonUtil.hasText(memberId)) {
			return CommonResult.build(Integer.valueOf(500), StrConsts.PARAMETER_ERROR);
		}
		// 查询领取任务信息
		return this.tasksService.selectTasksToReceive(taskId, memberId);
	}

	/**
	 * 初始化任务提交界面信息
	 * 
	 * @author xudj20170214
	 * @return
	 */
	@RequestMapping("/putTasksInit")
	@ResponseBody
	public CommonResult putTasksInit(@RequestParam Integer taskId, @RequestParam String memberId) {
		if (!CommonUtil.hasText(taskId) || !CommonUtil.hasText(memberId)) {
			return CommonResult.build(Integer.valueOf(500), StrConsts.PARAMETER_ERROR);
		}
		//是否已经提交过了
		TaskMemberRecord taskMemberRecord = taskMemberRecordService.selectByTaskIdAndMemberId(taskId, memberId);
		if(taskMemberRecord == null){
			return CommonResult.build(Integer.valueOf(500), StrConsts.HTTP_REQUEST_ERROR);
		}else if(taskMemberRecord.getState() != 0){
			return CommonResult.build(-1, "");//已提交的
		}
		// 查询提交信息展示
		return tasksService.selectTasksToPut(taskId);
	}

	/**
	 * 个人中心加载微任务
	 * 
	 * @author xudj20170214
	 */
	@RequestMapping("/personTasksInit")
	@ResponseBody
	public CommonResult personTasksInit(@RequestParam String memberId) {
		if (!CommonUtil.hasText(memberId)) {
			return CommonResult.build(Integer.valueOf(500), StrConsts.PARAMETER_ERROR);
		}
		// 根据用户unionid微任务信息
		return tasksService.selectPersonTasksInit(memberId);
	}

	
	/**
	 * 初始化我的栗子界面数据
	 * @author xudj20170215
	 * @return
	 */
	@RequestMapping("/myIntegraTasksInit")
	@ResponseBody
	public CommonResult myIntegraTasksInit(@RequestParam String memberId){
		//非空校验
		if(!CommonUtil.hasText(memberId)){
			return CommonResult.build(Integer.valueOf(500), StrConsts.PARAMETER_ERROR);
		}
		return tasksService.selectMyIntegraTasksInit(memberId);
	}
	
	
	/*======================= 后台管理方法 ======================*/
	/**
	 * 发布一个微任务(增加)
	 * @author lvshilin20170215
	 * @change xudj20170217
	 * @param types:关键词类型ids
	 */
	@RequestMapping("/web/addTasks")
	@ResponseBody
	public CommonResult addTasks(TasksWithBLOBs tasks, String shelveDateStr, String endDateStr, String applyEndDateStr,
			String[] keywords) {
		if(!CommonUtil.hasText(keywords)){
			return CommonResult.build(Integer.valueOf(500), StrConsts.PARAMETER_ERROR);
		}
		//新增
		return tasksService.insertTasks(tasks, shelveDateStr, endDateStr, applyEndDateStr, keywords);
	}
	
	/**
	 * 根据条件查询微任务
	 * @author lvshilin20170213
	 * @change xudj
	 */
	@RequestMapping("/web/getTasksManage")
	@ResponseBody
	public Map<String, Object> getTasksManage(TaskSearchPOJO taskSearchPOJO) {
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("result", true);
		resMap.put("page", tasksService.getTasksManage(taskSearchPOJO));
		return resMap;
	}
	
	/**
	 * 新增或编辑初始化界面数据
	 * @author lvshilin20170215
	 * @change xudj20170217
	 */
	@RequestMapping("/web/queryTaskById")
	@ResponseBody
	public CommonResult queryTaskById(Integer taskId) {
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("tasktype", dictService.getDictsByType("tasktype"));
		resMap.put("taskbrand", dictService.getDictsByType("taskbrand"));
		resMap.put("taskkeyword", dictService.getDictsByType("taskkeyword"));
		if (CommonUtil.hasText(taskId)) {// 编辑
			TasksWithBLOBs task = tasksService.queryTaskById(taskId);
			resMap.put("task", task);
		}
		return CommonResult.ok(resMap);
	}

	/**
	 * 编辑保存, id还是之前的
	 * @author lvshilin20170215
	 */
	@RequestMapping("/web/updateTask")
	@ResponseBody
	public CommonResult updateTask(TasksWithBLOBs tasks, String shelveDateStr, String endDateStr, String applyEndDateStr, 
			String[] keywords) {
		if(!CommonUtil.hasText(tasks.getId()) || !CommonUtil.hasText(tasks.getIsShelve()) || !CommonUtil.hasText(keywords)){
			return CommonResult.build(500, StrConsts.PARAMETER_ERROR);
		}
		//上架的任务不可编辑
		if("T".equals(tasks.getIsShelve())){
			return CommonResult.build(400, "上架中的任务不可编辑，请先下架");
		}
		//更新
		int count = tasksService.updateTask(tasks, shelveDateStr, endDateStr, applyEndDateStr, keywords);
		if (count == 0) {
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		return CommonResult.ok();
	}

	/**
	 * 上下架按钮
	 * @author lvshilin20170215
	 */
	@RequestMapping("/web/shelveTask")
	@ResponseBody
	public CommonResult shelveTask(Integer id, String isShelve) {
		if(!CommonUtil.hasText(id) || !CommonUtil.hasText(isShelve)){
			return CommonResult.build(500, StrConsts.PARAMETER_ERROR);
		}
		//上下架
		return tasksService.updateIsShelve(id, isShelve);
	}

	/**
	 * 删除按钮
	 * 删除任务同时下架任务
	 * @author lvshilin20170216
	 * @change xudj20170217
	 */
	@RequestMapping("/web/deleteTask")
	@ResponseBody
	public CommonResult deleteTask(Integer id, String isDel) {
		if(!CommonUtil.hasText(id)){
			return CommonResult.build(500, StrConsts.PARAMETER_ERROR);
		}
		//删除任务
		int count = tasksService.deleteTask(id, isDel);
		if (count == 0) {
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		return CommonResult.ok();
	}
}
