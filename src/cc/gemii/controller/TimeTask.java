package cc.gemii.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import cc.gemii.mapper.OtherMapper;
import cc.gemii.mapper.RETaskMemberRecordMapper;
import cc.gemii.mapper.RETasksMapper;
import cc.gemii.pojo.TemplateServerRemind;
import cc.gemii.pojo.TemplateTaskExpireRemind;
import cc.gemii.pojo.TemplateTaskHandleRemind;
import cc.gemii.service.ApiService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.TemplateUtil;
import cc.gemii.utils.UrlConsts;

/**
 * 定时任务
 * @author xudj20170213
 */
@Component
public class TimeTask {
	//日志
	private static final Logger log = LoggerFactory.getLogger(TimeTask.class);
	
	@Autowired
	private OtherMapper otherMapper;
	
	@Autowired
	private RETasksMapper reTaskMapper;
	
	@Autowired
	private RETaskMemberRecordMapper reTaskMemberRecordMapper;
	
	@Autowired
	private TaskExecutor taskExecutor;
	
	@Autowired
	private ApiService apiService;
	
	/**
	 * 查询发送上架的微任务模板消息的提醒
	 * cron："0 0 16 * * ?"
	 * @author xudj20170213
	 */
//	@Scheduled(cron = "0 0 16 * * ?")
	public void tasksRmindTemplate(){
		//查询今天新上架的并且需要系统提醒的微任务个数
		List<Integer> ids = reTaskMapper.getShelveRemindTasksToday();
		if(ids == null || ids.size() == 0){//无数据
			log.info("没有新上架提醒任务.");
			return;
		}
		//模板消息
		TemplateTaskHandleRemind taskHandleRemind = new TemplateTaskHandleRemind();
		taskHandleRemind.setFirst("亲爱的妈妈，本周微任务上线了！");
		taskHandleRemind.setKeyword1("[栗妈闺蜜团微任务]");
		taskHandleRemind.setKeyword2("新任务提醒");
		taskHandleRemind.setRemark("完成每个微任务时都会获得相应的栗子，这不是奖品，是对每一个喜爱育儿知识妈妈们的鼓励。");
		//跳个人中心
		String url = UrlConsts.PERSON_BASE_URL;
		//查询所有未领取当天任务的用户微信粉丝
		List<Map<String, String>> fansOpenids = otherMapper.selectFansFw(ids);
		int size = fansOpenids.size();
		
		//跨度数量(对4取整)
		int skipNum = size / 4;
		//启动线程4个
		this.taskExecutorToTemplate(0, skipNum, taskHandleRemind, fansOpenids, url);
		this.taskExecutorToTemplate(skipNum, skipNum * 2, taskHandleRemind, fansOpenids, url);
		this.taskExecutorToTemplate(skipNum * 2, skipNum * 3, taskHandleRemind, fansOpenids, url);
		this.taskExecutorToTemplate(skipNum * 3, size, taskHandleRemind, fansOpenids, url);
		
		log.info("上架新任务提醒模板主线程结束.");
	}

//	/**
//	 * 临时任务
//	 */
//	@Scheduled(cron = "0 0 18 25 5 ?")
//	public void temporaryTast(){
//		//模板消息
//		TemplateTaskHandleRemind taskHandleRemind = new TemplateTaskHandleRemind();
//		taskHandleRemind.setFirst("亲爱的妈妈，本周重点任务！");
//		taskHandleRemind.setKeyword1("[问卷调研]职场妈妈母乳现状调查");
//		taskHandleRemind.setKeyword2("任务提醒");
//		taskHandleRemind.setRemark("完成每个微任务时都会获得相应的栗子，这不是奖品，是对每一个喜爱育儿知识妈妈们的鼓励。");
//		//跳个人中心
//		String url = UrlConsts.PERSON_BASE_URL;
//		//查询所有微信粉丝
//		List<Map<String, String>> fansOpenids = otherMapper.selectAllFans();
//		int size = fansOpenids.size();
//		
//		//跨度数量(对4取整)
//		int skipNum = size / 4;
//		//启动线程4个
//		this.taskExecutorToTemplate(0, skipNum, taskHandleRemind, fansOpenids, url);
//		this.taskExecutorToTemplate(skipNum, skipNum * 2, taskHandleRemind, fansOpenids, url);
//		this.taskExecutorToTemplate(skipNum * 2, skipNum * 3, taskHandleRemind, fansOpenids, url);
//		this.taskExecutorToTemplate(skipNum * 3, size, taskHandleRemind, fansOpenids, url);
//		
//		log.info("上架新任务提醒模板主线程结束.");
//	}
	/**
	 * 启动线程发起任务处理通知
	 * @param fansOpenids 
	 * @param j:开始下标
	 * @param i:结束下标
	 * @param accessToken 
	 */
	private void taskExecutorToTemplate(final int indexStart, final int indexEnd, final TemplateTaskHandleRemind taskHandleRemind, final List<Map<String, String>> fansOpenids, final String url) {
		taskExecutor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					String accessToken = "";	//accessToken
					Map<String, Object> fieldValue = taskHandleRemind.getFieldValueMap();//模板内容
					Map<String, String> map = new HashMap<String, String>();//用户openid与unionid
					for(int i=indexStart ; i<indexEnd ; i++){
						accessToken = apiService.getAccessTokenByFw();
						map = fansOpenids.get(i);
						
						TemplateUtil.sendTemplateGen(map.get("openid"), fieldValue, taskHandleRemind.getTemplateId(), url, accessToken);
						System.out.println(map.get("openid") + ":第"+i+"个");
					}
					log.info("上架新任务提醒模板发送成功.下标从{}至{}", indexStart, indexEnd);
				} catch (Exception e) {
					e.printStackTrace();
					log.error("上架新任务提醒模板发送异常");
				}
			}
		});
		
	}

	
	/**
	 * 查询即将到期的待提交的微任务
	 * cron："0 0 10 * * ?"
	 */
//	@Scheduled(cron = "0 0 10 * * ?")
	public void tasksExpireRemindTemplate(){
		//查询有即将到期的待提交微任务的粉丝
		List<Map<String, Object>> openidList = reTaskMemberRecordMapper.getTasksRemindExpire();
		if(openidList == null || openidList.size() == 0){
			log.info("无待提交任务提醒用户");
			return;
		}
		String accessToken = "";//accessToken
		int count = 0; //计数
		TemplateTaskExpireRemind taskExpireRemind = new TemplateTaskExpireRemind();
		taskExpireRemind.setFirst("亲爱的妈妈，您领取的任务即将过期，快提交成果吧！");
		taskExpireRemind.setRemark("立即提交任务~");
		//循环发送
		for (Map<String, Object> map : openidList) {
			accessToken = apiService.getAccessTokenByFw();
			
			taskExpireRemind.setKeyword1((String)map.get("title"));//标题
			taskExpireRemind.setKeyword2((String)map.get("shelveDateStr"));//开始时间
			taskExpireRemind.setKeyword3((String)map.get("endDateStr"));//结束时间
			taskExpireRemind.setKeyword4(CommonUtil.getHourDiff(new Date(), (Date)map.get("endDate")));//剩余时间
			String openid = (String)map.get("openid");//openid
			String url = UrlConsts.SUBMIT_TASKS_URL.replace("TASKID", String.valueOf(map.get("taskId"))).replace("MEMBERID", String.valueOf(map.get("memberId")));
			TemplateUtil.sendTemplateGen(openid, taskExpireRemind.getFieldValueMap(), taskExpireRemind.getTemplateId(), url, accessToken);
			log.info("提醒用户openid:{},第{}个", openid, (++count));
		}
		log.info("待提交任务提醒模板发送成功.共推送{}个", count);
	}
	
	
	/**
	 * 定时查询今天是否有上架或者昨天需要下架的任务
	 * 每小时过两分 "0 2 * * * ?"
	 * @author xudj20170216
	 */
	@Scheduled(cron = "0 2 * * * ?")
	public void taskShelveManage(){
		//更新今天需要上架的任务
		int count = reTaskMapper.updateTaskToShelve();
		log.info("今日上架任务{}个", count);
		//更新今天需要下架的任务
		count = reTaskMapper.updateTaskToUnShelve();
		log.info("今日下架任务{}个", count);
	}

	
	/**
	 * 一次性需求
	 * @author: xudj
	 * @date: 2018年1月11日 上午11:38:13
	 */
//	@Scheduled(cron = "0 0 17 31 1 ?")
	public void rmindTemplate(){
		//模板消息
		TemplateTaskHandleRemind taskHandleRemind = new TemplateTaskHandleRemind();
		taskHandleRemind.setFirst("栗子闺蜜团新任务上线，点击详情查看");
		taskHandleRemind.setKeyword1("康萃乐的益生菌小妙招【看清任务简介哦】");
		taskHandleRemind.setKeyword2("待领取");
		taskHandleRemind.setKeyword3("栗子妈妈闺蜜团");
		taskHandleRemind.setKeyword4("2018-02-02 10:00");
		taskHandleRemind.setRemark("");
		//跳个人中心
		String url = UrlConsts.PERSON_BASE_URL;
		//查询所有未领取当天任务的用户微信粉丝
		List<Map<String, String>> fansOpenids = otherMapper.selectHasTaskFansFw();
		int size = fansOpenids.size();
		log.info("fans count:" + size);
		
		//跨度数量(对4取整)
		int skipNum = size / 4;
		//启动线程4个
		this.taskExecutorToTemplate(0, skipNum, taskHandleRemind, fansOpenids, url);
		this.taskExecutorToTemplate(skipNum, skipNum * 2, taskHandleRemind, fansOpenids, url);
		this.taskExecutorToTemplate(skipNum * 2, skipNum * 3, taskHandleRemind, fansOpenids, url);
		this.taskExecutorToTemplate(skipNum * 3, size, taskHandleRemind, fansOpenids, url);
		
		log.info("上架新任务提醒模板主线程结束.");
	}

	
	
	/**
	 * 一次性需求
	 * @author: xudj
	 * @date: 2018年1月11日 上午11:38:13
	 */
	@Scheduled(cron = "0 0 19 21 3 ?")
	public void serverRemindTemplate(){
		//模板消息
		TemplateServerRemind templateServerRemind = new TemplateServerRemind();
		templateServerRemind.setFirst("新任务来啦~栗妈闺蜜团再出动~");
		templateServerRemind.setGood("栗子妈妈微任务");
		templateServerRemind.setContentType("任务领取中");
		templateServerRemind.setRemark("");
		String url = "https://w.url.cn/s/A23pL96";
		// 查询某个任务
		List<String> fansOpenids = otherMapper.selectHasCommitTaskFansFw();
		int size = fansOpenids.size();
		log.info("fans count:" + size);
		
		for (String openId : fansOpenids) {
			String accessToken = apiService.getAccessTokenByFw();
			
			TemplateUtil.sendTemplateGen(openId, templateServerRemind.getFieldValueMap(), templateServerRemind.getTemplateId(), url, accessToken);
			System.out.println("openId 发送："+ openId);
		}
		log.info("上架新任务提醒模板主线程结束：" + size);
	}
	
	
}
