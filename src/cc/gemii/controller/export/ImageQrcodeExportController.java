package cc.gemii.controller.export;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cc.gemii.po.ImageQrcode;
import cc.gemii.service.ImageQrcodeService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.export.ManySheetExcelUtils;
import cc.gemii.utils.export.ManySheetExportUtils;
import cc.gemii.utils.export.PropSetterUtils;

/**
 * 二维码导出excel控制器
 * @author xudj20170406
 */
@Controller
@RequestMapping("/iqrcode/sysExport")
public class ImageQrcodeExportController {
	private static final Logger log = LoggerFactory.getLogger(ImageQrcodeExportController.class);
	
	@Autowired
	private ImageQrcodeService imageQrcodeService;
	
	/**
	 * 导出二维码相关用户信息
	 * @throws Exception 
	 */
	@RequestMapping("/fans")
	public void exportFansScanData(HttpServletRequest request, HttpServletResponse response, String attTimeStartStr, 
			String attTimeEndStr, @RequestParam String idStr) throws Exception{
		if(!CommonUtil.hasText(idStr)){
			log.warn("参数为空");
			return;
		}
		//查询条件
  		HashMap<String, Object> cons = new HashMap<String, Object>();
  		cons.put("attTimeStartStr", attTimeStartStr);
  		cons.put("attTimeEndStr", attTimeEndStr);
  		
  		//参数处理
  		String[] idsStr = idStr.split(",");
  		int[] ids = new int[idsStr.length];
  		for (int i=0 ; i<idsStr.length; i++) {
  			ids[i] = Integer.valueOf(idsStr[i]);
		}
  		
  		OutputStream out = response.getOutputStream();
  		ZipOutputStream zipOutputStream = new ZipOutputStream(out);
  		String fileName = "二维码数据来源导出" + System.currentTimeMillis() + ".zip";
		 response.setContentType("application/octet-stream ");  
		 response.setContentType("application/OCTET-STREAM;charset=UTF-8");
	     response.setHeader("Connection", "close"); // 表示不能用浏览器直接打开  
	     response.setHeader("Accept-Ranges", "bytes");// 告诉客户端允许断点续传多线程连接下载  
	     response.setHeader("Content-Disposition",  
	             "attachment;filename=" + new String(fileName.getBytes("GB2312"), "ISO8859-1"));  
	     response.setCharacterEncoding("UTF-8");  
  		
  		PropSetterUtils propSetterUtils = new PropSetterUtils();
  		for (Integer id : ids) {
  			cons.put("id", id);
  			// 根据id查询
  			ImageQrcode imageQrcode = imageQrcodeService.selectById(Long.valueOf(id));
  			if (imageQrcode == null) {
				continue;
			}
  	        //查询关注列表
  			List<Map<String, Object>> imageQrcodeAtts = imageQrcodeService.exportFansAttData(cons);
  			//查询扫码列表
  			List<Map<String, Object>> imageQrcodeScans = imageQrcodeService.exportFansScanData(cons);
  			//查询取关列表
  			List<Map<String, Object>> imageQrcodeNotAtts = imageQrcodeService.exportFansNotAttData(cons);

  			//导出excel
  			List<ManySheetExcelUtils> excelUtils = new ArrayList<ManySheetExcelUtils>();  
  	        excelUtils.add(new ManySheetExcelUtils("关注列表", propSetterUtils.getImageQrcodeAttsProps(), imageQrcodeAtts));  
  	        excelUtils.add(new ManySheetExcelUtils("扫码列表", propSetterUtils.getImageQrcodeScanProps(), imageQrcodeScans));
  	        excelUtils.add(new ManySheetExcelUtils("取关列表", propSetterUtils.getImageQrcodeNotAttProps(), imageQrcodeNotAtts)); 
  			
  	        Workbook workbook = new ManySheetExportUtils().createExcel(excelUtils);
			ZipEntry entry = new ZipEntry(imageQrcode.getDescription() + "_" + CommonUtil.dateFormat(new Date(), CommonUtil.dateFormat_yyMMdd) + "_" + id + ".xls");  
            zipOutputStream.putNextEntry(entry);
            workbook.write(zipOutputStream);
  		}
  		 zipOutputStream.flush();
         zipOutputStream.close();
	}
	
}
