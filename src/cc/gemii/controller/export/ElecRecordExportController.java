package cc.gemii.controller.export;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cc.gemii.service.TaskElecRewardService;
import cc.gemii.utils.CommonUtil;

/**
 * <p>ClassName: ElecRecordExportController</p>
 * <p>Description: 卡券领取记录导出控制器</p>
 * <p>Company: gemii</p>
 * @author xudj
 * @date 2017年4月27日 下午4:12:37
 */
@Controller
@RequestMapping("/elec/sysExport")
public class ElecRecordExportController {

	@Autowired
	private TaskElecRewardService taskElecRewardService;
	
	/**
	 * <p>Description: 批量导出功能</p>
	 * @author xudj
	 * @date 2017年4月27日 下午4:20:23
	 * @param request
	 * @param response
	 * @param idStr：卡券主键id
	 * @throws Exception 
	 */
	@RequestMapping("/exportElecRecord")
	public void exportElecRecord(HttpServletRequest request, HttpServletResponse response, @RequestParam String idStr){
		try {
			// 参数校验
			if(!CommonUtil.hasText(idStr)){
				response.getWriter().write("<script>alert('参数异常!');window.location='"+request.getContextPath()+"';</script>");
				return;
			}
			//参数处理
	  		String[] idsStr = idStr.split(",");
	  		int[] ids = new int[idsStr.length];
	  		for (int i=0 ; i<idsStr.length; i++) {
	  			ids[i] = Integer.valueOf(idsStr[i]);
			}
	  		// 导出
			taskElecRewardService.exportElecRecord(response, ids);
		} catch (Exception e) {
			try {
				response.getWriter().write("<script>alert('导出异常!');window.location='"+request.getContextPath()+"';</script>");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}
	
	
}
