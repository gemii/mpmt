package cc.gemii.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.PagingPOJO;
import cc.gemii.service.IntegrationRecordService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.StrConsts;
import cc.gemii.utils.UrlConsts;

/**
 * <p>ClassName: InterationRecordController</p>
 * <p>Description: 积分控制器</p>
 * <p>Company: gemii</p>
 * @author xudj
 * @date 2017年4月8日 下午4:19:13
 */
@Controller
@RequestMapping("/irecord")
public class InterationRecordController {
	private static final Logger log = LoggerFactory.getLogger(InterationRecordController.class);
	
	
	@Autowired
	IntegrationRecordService integrationRecordService;
	
	/**
	 * 分页获取积分明细
	 * @author xudj20170210
	 * @return
	 */
	@RequestMapping("/listInterationRecord")
	@ResponseBody
	public Map<String, Object> listInterationRecord(PagingPOJO pagingPojo, @RequestParam String memberId){
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("result", true);
		resMap.put("page", this.integrationRecordService.listByUnoinId(pagingPojo, memberId));
		return resMap;
	}
	
	/**
	 * <p>Description: 商品兑换初始化</p>
	 * @author xudj
	 * @date 2017年4月8日 下午4:19:22
	 * @param openId：用户openid
	 * @return
	 */
	@RequestMapping("/goodsVerify")
	@ResponseBody
	public CommonResult selectGoodsVerify(@RequestParam String openId){
		//参数校验
		if(!CommonUtil.hasText(openId)){
			return CommonResult.build(500, StrConsts.PARAMETER_ERROR);
		}
		return integrationRecordService.selectGoodsVerify(openId);
	}
	
	/**
	 * <p>Description: 商品兑换积分扣除</p>
	 * @author xudj
	 * @date 2017年4月11日 上午11:55:42
	 * @param userid:openid_goodId
	 * @return
	 */
	@RequestMapping("/goodsConvert")
	public String handleGoodsConvert(String userid, String sojumpindex){
		log.info("用户信息及商品编号：{}", userid);
		//参数校验
		if(!CommonUtil.hasText(userid)){
			log.warn("用户信息及商品编号获取失败，商品兑换失败,sojumpindex:{}",sojumpindex);
			//跳转到个人中心
			return "redirect:" + UrlConsts.PERSON_BASE_URL;
		}
		//业务处理
		String res = integrationRecordService.handleGoodsConvert(userid, sojumpindex);
		if(res != null){
			return "redirect:" + res;
		}
		//跳转到个人中心
		return "redirect:" + UrlConsts.PERSON_BASE_URL;
	}
	
}
