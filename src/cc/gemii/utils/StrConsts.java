package cc.gemii.utils;

/**
 * 字符串常量
 * @author xudj20170217
 */
public class StrConsts {

	public static final String PARAMETER_ERROR = "参数异常";		//参数异常
	
	public static final String HTTP_REQUEST_ERROR = "请求异常";	//请求异常
	
	public static final String SELECT_REQUEST_ERROR = "查询失败";	//查询失败
	
	public static final String UPDATE_REQUEST_ERROR = "更新异常";	//更新异常
	
	//字典表：加入栗妈闺蜜团
	public static final String DICT_TYPE_REGISTER = "register";
	
	//字典表：邀请好友
	public static final String DICT_TYPE_INVITE = "invite";
	
	//字典表：签到
	public static final String DICT_TYPE_INTEGRALRULE = "integralrule";
	
	//字典表：问卷调研
	public static final String DICT_TYPE_QUESTION_SURVEY = "questionsurvey";

	//字典表：分享转发
	public static final String DICT_TYPE_SHARE_FORWARD = "shareforward";
	
	//字典表：完善个人资料
	public static final String DICT_TYPE_EVPI = "evpi";
	
	//字典表：服务号粉丝来源
	public static final String DICT_TYPE_FANS_FW_SOURCE = "fansfwsource";
	
	//字典表：场景二维码投放场景
	public static final String DICT_TYPE_QRCODE_SCENE = "qrcodescene";

	//积分明细类型:栗子兑换
	public static final String INTEGRATION_RECORD_TITLE_CONVERT = "栗子兑换";
	
}
