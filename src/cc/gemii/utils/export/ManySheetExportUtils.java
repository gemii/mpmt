package cc.gemii.utils.export;

import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
 * 多sheet导出模板 
 * @author xudj20170407
 */
public class ManySheetExportUtils {
	private static Logger logger = LoggerFactory.getLogger(ManySheetExportUtils.class);  
	  
  
    private CellStyle titleStyle; 
  
    private CellStyle stringStyle; 
  
    private CellStyle longStyle;
  
    private CellStyle doubleStyle;
  
    /** 
     * @param 
     *      utils  全部属性信息 
     *  
     * */  
    public HSSFWorkbook createExcel(List<ManySheetExcelUtils> utils) {  
        long startTime = System.currentTimeMillis();  
          
        Cell cell;  
        Row row;  
        HSSFWorkbook work = new HSSFWorkbook();
        this.setStyle(work);
        for (int i = 0; i < utils.size(); i++) { 
        	HSSFSheet sheet = work.createSheet(); 
            work.setSheetName(i, utils.get(i).getSheetName());// 根据属性创建sheet页  
            logger.info("第" + i + "次sheet创建完成");  
            Row rowOne = sheet.createRow(0);  
            rowOne.setHeight((short) 350);  
            List<PropSetter> props = utils.get(i).getProps();  
            for (int j = 0; j < props.size(); j++) {// 标题的设置  
                cell = rowOne.createCell(j);  
                sheet.setColumnWidth(j, props.get(j).getWidth()); // 宽度  
                cell.setCellStyle(titleStyle);  
                cell.setCellValue(props.get(j).getrOne()); // 标题  
            }  
            logger.info("第" + i + "次表头信息创建完成");  
            // 内容设置  
            List<Map<String, Object>> datas = utils.get(i).getDatas();  
            logger.info("准备执行第" + i + "次数据填充操作");  
            if (datas.size() != 0) {  
                for (int m = 0; m < datas.size(); m++) {  
                    row = sheet.createRow(m + 1);  
                    row.setHeight((short) 310);  
                    for (int n = 0; n < props.size(); n++) {  
                        cell = row.createCell(n);  
                        Object value = datas.get(m).get(props.get(n).getProp());  
                        if (value == null) {  
                            cell.setCellValue("");  
                            cell.setCellStyle(stringStyle);  
                        } else {  
                            try {  
                                cell.setCellType(HSSFCell.CELL_TYPE_STRING);  
                                cell.setCellValue(Long.valueOf(String.valueOf(value)));  
                                cell.setCellStyle(longStyle);  
                            } catch (Exception e) {  
                                try {  
                                    cell.setCellType(HSSFCell.CELL_TYPE_STRING);  
                                    cell.setCellValue((Double.valueOf(String.valueOf(value))));  
                                    cell.setCellStyle(doubleStyle);  
                                } catch (Exception e1) {  
                                    cell.setCellType(HSSFCell.CELL_TYPE_STRING);  
                                    cell.setCellValue(String.valueOf(value));  
                                    cell.setCellStyle(stringStyle);  
                                }  
                            }  
                        }  
                    }  
                }  
            }  
        }  
        logger.info("导出总计耗时: {}", System.currentTimeMillis() - startTime + "毫秒!");  
        return work;  
    }  
    
    /**
     * <p>Description: 设置样式</p>
     * @author xudj
     * @date 2017年4月10日 下午6:08:12
     * @param workbook
     */
    public void setStyle(HSSFWorkbook workbook){
    	DataFormat format = workbook.createDataFormat();  
    	  
        Font titleFont = workbook.createFont();  
        titleFont.setFontName("微软雅黑");  
        titleFont.setFontHeightInPoints((short) 10); // 字体大小  
        titleFont.setBoldweight(Font.BOLDWEIGHT_BOLD);// 加粗  
  
        Font contentFont = workbook.createFont();  
        contentFont.setFontName("微软雅黑");  
        contentFont.setFontHeightInPoints((short) 9);
        
    	titleStyle = workbook.createCellStyle();  
        titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);// 垂直居中  
        titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 水平居中  
        titleStyle.setBorderBottom(CellStyle.BORDER_THIN);  
        titleStyle.setBorderLeft(CellStyle.BORDER_THIN);  
        titleStyle.setBorderRight(CellStyle.BORDER_THIN);  
        titleStyle.setBorderTop(CellStyle.BORDER_THIN);  
        titleStyle.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);// 填暗红色  
        titleStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);  
        titleStyle.setFont(titleFont);  
        titleStyle.setWrapText(true);  
  
        stringStyle = workbook.createCellStyle();  
        stringStyle.setAlignment(CellStyle.ALIGN_LEFT);  
        stringStyle.setBorderBottom(CellStyle.BORDER_THIN);  
        stringStyle.setBorderLeft(CellStyle.BORDER_THIN);  
        stringStyle.setBorderRight(CellStyle.BORDER_THIN);  
        stringStyle.setBorderTop(CellStyle.BORDER_THIN);  
        stringStyle.setFont(contentFont);  
        stringStyle.setWrapText(true);  
  
        longStyle = workbook.createCellStyle();  
        longStyle.setAlignment(CellStyle.ALIGN_LEFT);  
        longStyle.setBorderBottom(CellStyle.BORDER_THIN);  
        longStyle.setBorderLeft(CellStyle.BORDER_THIN);  
        longStyle.setBorderRight(CellStyle.BORDER_THIN);  
        longStyle.setBorderTop(CellStyle.BORDER_THIN);  
        longStyle.setFont(contentFont);  
        longStyle.setDataFormat(format.getFormat("0"));  
        longStyle.setWrapText(true);  
  
        doubleStyle = workbook.createCellStyle();  
        doubleStyle.setAlignment(CellStyle.ALIGN_LEFT);  
        doubleStyle.setBorderBottom(CellStyle.BORDER_THIN);  
        doubleStyle.setBorderLeft(CellStyle.BORDER_THIN);  
        doubleStyle.setBorderRight(CellStyle.BORDER_THIN);  
        doubleStyle.setBorderTop(CellStyle.BORDER_THIN);  
        doubleStyle.setFont(contentFont);  
        doubleStyle.setDataFormat(format.getFormat("0.00"));  
        doubleStyle.setWrapText(true);
    }
 
}
