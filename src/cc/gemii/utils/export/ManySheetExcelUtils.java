package cc.gemii.utils.export;

import java.util.List;
import java.util.Map;

/**
 * excel组成
 * @author xudj20170407
 */
public class ManySheetExcelUtils {
	private String sheetName;// sheet名称  
    
    private List<PropSetter> props;// 属性设置  
      
    private List<Map<String, Object>> datas;// 数据信息   
  
    public ManySheetExcelUtils() {  
        super();  
    }  
  
    public ManySheetExcelUtils(String sheetName, List<PropSetter> props,  
            List<Map<String, Object>> datas) {  
        super();  
        this.sheetName = sheetName;  
        this.props = props;  
        this.datas = datas;  
    }

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public List<PropSetter> getProps() {
		return props;
	}

	public void setProps(List<PropSetter> props) {
		this.props = props;
	}

	public List<Map<String, Object>> getDatas() {
		return datas;
	}

	public void setDatas(List<Map<String, Object>> datas) {
		this.datas = datas;
	}  
    
}
