package cc.gemii.utils.export;

/*** 
 * 组成list数据的实体 
 * @author xudj20170407
 */
public class PropSetter {
	
	private String rOne; //第一行标题  
    
    private String rTwo; //第二行标题  
      
    private String prop;//对应的导出数据的字段名  
      
    private String type;//数据类型  
      
    private int width;//表格宽度  
      
    private boolean color;//颜色  
  
    public PropSetter(String rOne, String prop, int width) {  
        super();  
        this.rOne = rOne;  
        this.prop = prop;  
        this.width = width;  
    }  
      
    public PropSetter(String rOne, String rTwo, String prop, int width) {  
        super();  
        this.rOne = rOne;  
        this.rTwo = rTwo;  
        this.prop = prop;  
        this.width = width;  
    }  
  
    public PropSetter(String rOne, String rTwo, String prop, int width,  
            boolean color) {  
        super();  
        this.rOne = rOne;  
        this.rTwo = rTwo;  
        this.prop = prop;  
        this.width = width;  
        this.color = color;  
    }  
  
    public PropSetter(String rOne, String rTwo, String prop, String type,  
            int width) {  
        super();  
        this.rOne = rOne;  
        this.rTwo = rTwo;  
        this.prop = prop;  
        this.type = type;  
        this.width = width;  
    }  
  
    public PropSetter() {  
        super();  
    }

	public String getrOne() {
		return rOne;
	}

	public void setrOne(String rOne) {
		this.rOne = rOne;
	}

	public String getrTwo() {
		return rTwo;
	}

	public void setrTwo(String rTwo) {
		this.rTwo = rTwo;
	}

	public String getProp() {
		return prop;
	}

	public void setProp(String prop) {
		this.prop = prop;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public boolean isColor() {
		return color;
	}

	public void setColor(boolean color) {
		this.color = color;
	}
    
}
