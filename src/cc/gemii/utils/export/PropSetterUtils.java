package cc.gemii.utils.export;

import java.util.ArrayList;
import java.util.List;

/**
 * 组成list数据的实体帮助类
 * @author xudj20170407
 */
public class PropSetterUtils {

	/**
	 * 关注列表数据属性
	 * @author xudj20170407
	 * @return
	 */
	public List<PropSetter> getImageQrcodeAttsProps(){
		List<PropSetter> props = new ArrayList<PropSetter>();  
        props.add(new PropSetter("openId", "openId", 8000));  
        props.add(new PropSetter("昵称", "nickname", 5000));  
        props.add(new PropSetter("关注时间", "attTime", 6000));
        return props;
	}
	
	/**
	 * 扫码列表数据属性
	 * @author xudj20170407
	 * @return
	 */
	public List<PropSetter> getImageQrcodeScanProps(){
		List<PropSetter> props = new ArrayList<PropSetter>();  
		props.add(new PropSetter("openId", "openId", 8000));  
		props.add(new PropSetter("昵称", "nickname", 5000));  
		props.add(new PropSetter("扫码时间", "scanTime", 6000));
        return props;
	}
	
	/**
	 * 取关列表数据属性
	 * @author xudj20170407
	 * @return
	 */
	public List<PropSetter> getImageQrcodeNotAttProps(){
		List<PropSetter> props = new ArrayList<PropSetter>();  
		props.add(new PropSetter("openId", "openId", 8000));  
		props.add(new PropSetter("扫码时间", "scanTime", 6000));
        return props;
	}
	
	/**
	 * <p>Description: 获取卡券领取记录表头</p>
	 * @author xudj
	 * @date 2017年4月27日 下午6:48:49
	 * @return
	 */
	public List<PropSetter> getElecRecordProps(){
		List<PropSetter> props = new ArrayList<PropSetter>();  
		props.add(new PropSetter("openId", "openid", 8000));  
		props.add(new PropSetter("昵称", "nickname", 8000));
		props.add(new PropSetter("点击次数", "count", 4000));
		props.add(new PropSetter("最后点击", "createTimeStr", 6000));
		props.add(new PropSetter("微任务标题", "title", 16000));
		return props;
	}
	
}
