package cc.gemii.utils;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;

public class TemplateUtil{

	/**
	 * 发送模板消息
	 * @param openid：openid
	 * @param data：data数据
	 * @param templateId：模板消息id
	 * @param url：模板链接
	 */
	public static void sendTemplateGen(String openid, Map<String, Object> data, String templateId, String url){
		String access_token = HttpClientUtil.doGet(UrlConsts.GET_ACCESSTOKEN_FW);
		String httpUrl = UrlConsts.SEND_TEMPLATE.replace("ACCESSTOKEN", access_token);
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("touser", openid);
		response.put("template_id", templateId);
		response.put("url", url==null?"":url);
		response.put("data", data);
		HttpClientUtil.doPostJson(httpUrl, JSON.toJSONString(response));
	}

	/**
	 * 发送模板消息
	* @param openid：openid
	 * @param data：data数据
	 * @param templateId：模板消息id
	 * @param url：模板链接
	 * @param accessToken：accesstoken
	 */
	public static void sendTemplateGen(String openid, Map<String, Object> data, String templateId, String url, String accessToken){
		String httpUrl = UrlConsts.SEND_TEMPLATE.replace("ACCESSTOKEN", accessToken);
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("touser", openid);
		response.put("template_id", templateId);
		response.put("url", url==null?"":url);
		response.put("data", data);
		HttpClientUtil.doPostJson(httpUrl, JSON.toJSONString(response));
	}
	
	
	public static void main(String[] args) {
	}
	
//		public void testTmplate(){
//			TemplateIntegra ti = new TemplateIntegra();
//			ti.setFirst("积分到账通知");
//			ti.setKeyword1("key1");
//			ti.setKeyword2("key2");
//			ti.setKeyword3("key3");
//			ti.setKeyword4("key4");
//			ti.setKeyword5("key5");
//			ti.setRemark("remark");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("keyword1", "#CC00FF");
//			TemplateUtil.sendTemplateGen("oNPcuvwnC1M3Z4sE--iIF3W5VzkE", ti.getFieldValueMap(map), ti.getTemplateId(), null);
//		//	没有颜色设置
//			TemplateUtil.sendTemplateGen("oNPcuvwnC1M3Z4sE--iIF3W5VzkE", ti.getFieldValueMap(), ti.getTemplateId(), null);
//		}
	
}
