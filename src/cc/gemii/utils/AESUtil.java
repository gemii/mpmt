package cc.gemii.utils;

import java.io.UnsupportedEncodingException;  
import java.security.InvalidKeyException;  
import java.security.NoSuchAlgorithmException;  
import java.security.SecureRandom;  
  



import javax.crypto.BadPaddingException;  
import javax.crypto.Cipher;  
import javax.crypto.IllegalBlockSizeException;  
import javax.crypto.KeyGenerator;  
import javax.crypto.NoSuchPaddingException;  
import javax.crypto.SecretKey;  
import javax.crypto.spec.SecretKeySpec;  
  



import org.apache.axis.encoding.Base64;  
  
public class AESUtil {  
    private static int length=128;  
    
    public static final String PASSWORD = "lizimama";
    /** 
     * 加密 
     *  
     * @param content 
     *            需要加密的内容 
     * @param password 
     *            加密密码 
     * @return 
     * @throws NoSuchAlgorithmException 
     * @throws NoSuchPaddingException 
     * @throws UnsupportedEncodingException 
     * @throws InvalidKeyException 
     * @throws BadPaddingException 
     * @throws IllegalBlockSizeException 
     */  
    private static byte[] encrypt(String content, String password)  
            throws Exception {  
  
        KeyGenerator kgen = KeyGenerator.getInstance("AES");  
                SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG" );   
                secureRandom.setSeed(password.getBytes());   
        kgen.init(length, secureRandom);  
        SecretKey secretKey = kgen.generateKey();  
        byte[] enCodeFormat = secretKey.getEncoded();  
        SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");  
        Cipher cipher = Cipher.getInstance("AES");// 创建密码器  
        byte[] byteContent = content.getBytes("utf-8");  
        cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化  
        byte[] result = cipher.doFinal(byteContent);  
        return result; // 加密  
  
    }  
  
    /** 
     * 解密 
     *  
     * @param content 
     *            待解密内容 
     * @param password 
     *            解密密钥 
     * @return 
     */  
    private static byte[] decrypt(byte[] content, String password)  
            throws Exception {  
  
        KeyGenerator kgen = KeyGenerator.getInstance("AES");  
                 SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG" );   
                  secureRandom.setSeed(password.getBytes());   
        kgen.init(length, secureRandom);  
        SecretKey secretKey = kgen.generateKey();  
        byte[] enCodeFormat = secretKey.getEncoded();  
        SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");  
        Cipher cipher = Cipher.getInstance("AES");// 创建密码器  
        cipher.init(Cipher.DECRYPT_MODE, key);// 初始化  
        byte[] result = cipher.doFinal(content);  
        return result; // 加密  
                  
               
  
    }  

    
    /**
     * 
     * @param content 要加密的内容 String
     * @param password 密钥 String
     * @return
     * @throws Exception
     * TODO
     */
    public static String encrypt2Str(String content, String password) throws Exception {  
    	if(content == null || content.equals("")){
    		return null;
    	}
        byte[] encryptResult = encrypt(content, password);  
        return Base64.encode(encryptResult);  
    }  
    
    /**
     * 
     * @param content 要解密的内容 String
     * @param password 密钥 String
     * @return
     * @throws Exception
     * TODO
     */
    public static String decrypt2Str(String content, String password) throws Exception {  
    	if(content == null || content.equals("")){
    		return null;
    	}
        byte[] decryptResult = decrypt(Base64.decode(content), password);  
        return new String(decryptResult,"UTF-8");  
    }  
  
    
    public static void main(String[] args) throws Exception {  
        String content = "xudj";  
        String password = "12345678";  
        // 加密  
        System.out.println(content.length());
        System.out.println("加密前：" + content);  
  
        String tt4 = encrypt2Str(content, password);  
        System.out.println(new String(tt4));  
        System.out.println(new String(tt4).length());
        // 解密  
        String d = decrypt2Str(tt4, password);  
        System.out.println("解密后：" + d);  
    }  
}