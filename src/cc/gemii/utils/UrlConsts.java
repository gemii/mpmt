package cc.gemii.utils;

/**
 * url帮助类
 * @author xudj20170207
 */
public class UrlConsts {

	//跨域请求有效域名
	public static final String ACCESS_ALLOW_ORIGIN = "http://wx.gemii.cc";
	
	//获取服务号accesstoken
	public static final String GET_ACCESSTOKEN_FW = "http://wx.gemii.cc/wx/getAccessTokenByFW";
	
	//发送模板消息url
	public static final String SEND_TEMPLATE = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESSTOKEN";

	//获取服务号accesstoken及时间
	public static final String GET_ACCESSTOKEN_TIME_FW = "http://wx.gemii.cc/wx/getFwAccessTokenAndTime";
	
	 //模板跳转到个人中心任务提交页(过期通知跳转)
	 public static final String SUBMIT_TASKS_URL = "http://wx.gemii.cc/mpmt-web/submit.html?taskId=TASKID&memberId=MEMBERID";

	 // 模板跳转到个人中心栗子账本页(审核通过后，埋点)
	 public static final String INTEGRATION_RECOED_FROM_VERIFY_PASS = "https://w.url.cn/s/AQTgqLk";//"http://t.cn/RiHvDmi";
	 
	//模板跳转到个人中心领取页(可领取任务模板提醒)
	 public static final String RECEIVE_TASKS_URL = "http://wx.gemii.cc/mpmt-web/receive.html?taskId=TASKID&memberId=MEMBERID";
	 
	 //模板跳转到个人中心(审核不通过，埋点)
	 public static final String PERSON_URL_FROM_VERIFY_NOTPASS = "https://w.url.cn/s/AuF9dSS";//"http://t.cn/RiTOrAf";
	 
	 //个人中心授权链接—userinfo
	 public static final String PERSON_BASE_URL = "http://t.cn/R6oWMy9";
	
	 //获取永久二维码
	 public static final String CREATE_FOREVER_QRCODE_AND_GETNAME = "http://wx.gemii.cc/wx/getQrLimitStrScene?scene_str=SCENESTR&path=PATH";
	 //获取临时二维码
	 public static final String CREATE_QRCODE_AND_GETNAME = "http://wx.gemii.cc/wx/getQrScene?scene_id=SCENEID&path=PATH&day=DAY";
	 
	 //wx服务器获取二维码图片的路径
	 public static final String GET_QRCODE_URL = "http://wx.gemii.cc/inviteSub/getQrsceneImage?fileName=FILENAME";
	 
	 //栗子兑换数量完成
	 public static final String GOODS_CONVERT_BACK_URL = "http://wx.gemii.cc/mpmt-web/goodsLack.html";
	 
	 //审核卡券类型微任务后－模板消息跳转
	 public static final String GET_LOAD_ELEC_URL = "https://mpmt.gemii.cc/mpmt/tereward/loadElecUrl?id=TASKID&openid=OPENID";
	 
	// 母亲节活动H5
	public static final String MOAC_ACTIVITY_H5_URL = "http://w.url.cn/s/ASFJD4y";
}
