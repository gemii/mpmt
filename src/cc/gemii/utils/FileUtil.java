package cc.gemii.utils;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jets3t.service.S3Service;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.security.AWSCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileUtil {
	
	private static final Logger log = LoggerFactory.getLogger(FileUtil.class);

	private static final String awsAccessKey = "AKIAOTDCELIZTI4INNVA";
	private static final String awsSecreyKey = "A3baxBnGJSVknuuYYfGS8xImwUF4r3xeKnd/3NR2";
	private static final String defaultBucketName = "nfs.gemii.cc";
	private static final String url_prex = "http://nfs.gemii.cc/";
	
	//微任务相关的图片上传目录
	public static final String TASKS_UPLOAD_DIR = "mpmt/tasks";
	
	/**
	 * 
	 * @param bytes
	 * @return
	 * TODO 上传图片
	 */
	public static String uploadFile(String bucketName, String dir, String filename, byte[] bytes) {
		try {
			String dir_filename = (dir == null?"":(dir + "/")) + filename;
			// 连接s3
			AWSCredentials awsCredentials = new AWSCredentials(awsAccessKey,awsSecreyKey);
			// 创建一个S3Service的对象,与s3交互
			S3Service s3Service = new RestS3Service(awsCredentials);
			// 创建一个bucket，即创建object的容器
			S3Bucket s3Bucket = s3Service.getBucket(bucketName == null?defaultBucketName : bucketName);
			S3Object s3Object = new S3Object(dir_filename);
			ByteArrayInputStream greetingIS = new ByteArrayInputStream(bytes);
			s3Object.setDataInputStream(greetingIS);
			s3Object.setContentLength(bytes.length);
			//S3Service将s3Object上传到相应的bucket中
			s3Service.putObject(s3Bucket, s3Object);
			// return url
			return url_prex + dir_filename;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("上传图片异常.");
			return null;
		}
	}

	public static String uploadFile(String dir, String filename, byte[] bytes){
		return uploadFile(null, dir, filename, bytes);
	}
	
	public static String uploadFile(String filename, byte[] bytes){
		return uploadFile(null, null, filename, bytes);
	}
	
	/**
	 * 
	 * @param file 文件
	 * @return
	 * TODO 文件转字节数组
	 */
	public static byte[] getBytesFromFile(File file) {
		byte[] bytes = null;
		try {
			InputStream is = new FileInputStream(file);
			// 获取文件大小
			long length = file.length();
			if (length > Integer.MAX_VALUE) {
				// 文件太大，无法读取
				throw new IOException("File is to large " + file.getName());
			}
			// 创建一个数据来保存文件数据
			bytes = new byte[(int) length];
			// 读取数据到byte数组中
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length
					&& (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
				offset += numRead;
			}
			// 确保所有数据均被读取
			if (offset < bytes.length) {
				throw new IOException("Could not completely read file " + file.getName());
			}
			// Close the input stream and return bytes
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bytes;
	}
	
	/**
	 * 
	 * @param b 字节数组
	 * @param outputFile 输出文件path
	 * @return 字节数组转文件
	 * TODO
	 */
	public static File getFileFromBytes(byte[] b, String outputFile) {    
        File ret = null;    
        BufferedOutputStream stream = null;    
        try {    
            ret = new File(outputFile);    
            FileOutputStream fstream = new FileOutputStream(ret);    
            stream = new BufferedOutputStream(fstream);    
            stream.write(b);    
        } catch (Exception e) {    
            // log.error("helper:get file from byte process error!");    
            e.printStackTrace();    
        } finally {    
            if (stream != null) {    
                try {    
                    stream.close();    
                } catch (IOException e) {    
                    // log.error("helper:get file from byte process error!");    
                    e.printStackTrace();    
                }    
            }    
        }    
        return ret;    
    }  
}
