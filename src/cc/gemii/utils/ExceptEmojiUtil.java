package cc.gemii.utils;

import java.util.ArrayList;
import java.util.List;
public class ExceptEmojiUtil {
	private static List<String> list = null;
	
	public static List<String> getList(){
		if(list == null){
			List<String> number = new ArrayList<String>();
			number.add("23");
			number.add("30");
			number.add("31");
			number.add("32");
			number.add("33");
			number.add("34");
			number.add("35");
			number.add("36");
			number.add("37");
			number.add("38");
			number.add("39");
			list = number;
			return list;
		}
		return list;
	}
	
	public static boolean isEmojiCharacter(char codePoint) {
        return !((codePoint == 0x0) || 
                (codePoint == 0x9) ||                            
                (codePoint == 0xA) ||
                (codePoint == 0xD) ||
                ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) ||
                ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) ||
                ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF)));
    }
	
	public static String str2unicode(String val) {
		StringBuffer res = new StringBuffer();
		boolean flag ;
		String hex = "";
		for (int i = 0; i < val.length(); i++) {
			char c = val.charAt(i);
			flag = isEmojiCharacter(c);
			hex = Integer.toHexString(c);
			System.out.println(hex);
			if(flag){
				res.append("\\u").append(hex);
			}else{
				if(hex.length() == 4 && (hex.startsWith("e") || hex.startsWith("E"))){
					res.append("\\u").append(hex);
				}else{
					res.append("\\u").append(hex);
				}
			}
		}
		return res.toString();
	}
	
	public static void main(String[] args) {
		String c = "🐵 🐻 🐨 💟 🎶 🇺🇸 🇬🇧 🕑 🕒0⃣ 1⃣ 2⃣ 3";
		
		System.out.println(str2unicode(c));
	}
}
