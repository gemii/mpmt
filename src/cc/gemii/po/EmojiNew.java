package cc.gemii.po;

public class EmojiNew {
    private Integer id;

    private String unicode;

    private String utf8;

    private String utf16;

    private String sbunicode;

    private String filename;

    private String mapcode;

    private byte[] filebyte;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUnicode() {
        return unicode;
    }

    public void setUnicode(String unicode) {
        this.unicode = unicode == null ? null : unicode.trim();
    }

    public String getUtf8() {
        return utf8;
    }

    public void setUtf8(String utf8) {
        this.utf8 = utf8 == null ? null : utf8.trim();
    }

    public String getUtf16() {
        return utf16;
    }

    public void setUtf16(String utf16) {
        this.utf16 = utf16 == null ? null : utf16.trim();
    }

    public String getSbunicode() {
        return sbunicode;
    }

    public void setSbunicode(String sbunicode) {
        this.sbunicode = sbunicode == null ? null : sbunicode.trim();
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename == null ? null : filename.trim();
    }

    public String getMapcode() {
        return mapcode;
    }

    public void setMapcode(String mapcode) {
        this.mapcode = mapcode == null ? null : mapcode.trim();
    }

    public byte[] getFilebyte() {
        return filebyte;
    }

    public void setFilebyte(byte[] filebyte) {
        this.filebyte = filebyte;
    }
}