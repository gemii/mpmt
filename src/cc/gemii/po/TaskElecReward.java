package cc.gemii.po;

import java.util.Date;

public class TaskElecReward {
    private Integer id;

    private String elecName;

    private String elecUrl;

    private Integer dictBrandId;

    private String description;

    private Integer createUser;

    private Date createTime;

    private Integer updateUser;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getElecName() {
        return elecName;
    }

    public void setElecName(String elecName) {
        this.elecName = elecName == null ? null : elecName.trim();
    }

    public String getElecUrl() {
        return elecUrl;
    }

    public void setElecUrl(String elecUrl) {
        this.elecUrl = elecUrl == null ? null : elecUrl.trim();
    }

    public Integer getDictBrandId() {
        return dictBrandId;
    }

    public void setDictBrandId(Integer dictBrandId) {
        this.dictBrandId = dictBrandId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Integer getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Integer createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Integer updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}