package cc.gemii.po;

import java.util.Date;

public class ImageQrcode {
    private Long id;

    private String filename;

    private String filepath;

    private Byte expiryDay;

    private String description;

    private Date createTime;

    private Integer createUser;

    private Date updateTime;

    private Integer updateUser;

    private String isDel;

    private Byte matterFwType;

    private String matterFwContent;

    private Integer dictSceneId;

    private Integer dictSourceId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename == null ? null : filename.trim();
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath == null ? null : filepath.trim();
    }

    public Byte getExpiryDay() {
        return expiryDay;
    }

    public void setExpiryDay(Byte expiryDay) {
        this.expiryDay = expiryDay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Integer createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Integer updateUser) {
        this.updateUser = updateUser;
    }

    public String getIsDel() {
        return isDel;
    }

    public void setIsDel(String isDel) {
        this.isDel = isDel == null ? null : isDel.trim();
    }

    public Byte getMatterFwType() {
        return matterFwType;
    }

    public void setMatterFwType(Byte matterFwType) {
        this.matterFwType = matterFwType;
    }

    public String getMatterFwContent() {
        return matterFwContent;
    }

    public void setMatterFwContent(String matterFwContent) {
        this.matterFwContent = matterFwContent == null ? null : matterFwContent.trim();
    }

    public Integer getDictSceneId() {
        return dictSceneId;
    }

    public void setDictSceneId(Integer dictSceneId) {
        this.dictSceneId = dictSceneId;
    }

    public Integer getDictSourceId() {
        return dictSourceId;
    }

    public void setDictSourceId(Integer dictSourceId) {
        this.dictSourceId = dictSourceId;
    }
}