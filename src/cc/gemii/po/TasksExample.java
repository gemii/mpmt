package cc.gemii.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TasksExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TasksExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("title is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("title is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(String value) {
            addCriterion("title =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(String value) {
            addCriterion("title <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(String value) {
            addCriterion("title >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(String value) {
            addCriterion("title >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(String value) {
            addCriterion("title <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(String value) {
            addCriterion("title <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLike(String value) {
            addCriterion("title like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotLike(String value) {
            addCriterion("title not like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<String> values) {
            addCriterion("title in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<String> values) {
            addCriterion("title not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(String value1, String value2) {
            addCriterion("title between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(String value1, String value2) {
            addCriterion("title not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTypeIdIsNull() {
            addCriterion("type_id is null");
            return (Criteria) this;
        }

        public Criteria andTypeIdIsNotNull() {
            addCriterion("type_id is not null");
            return (Criteria) this;
        }

        public Criteria andTypeIdEqualTo(Integer value) {
            addCriterion("type_id =", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotEqualTo(Integer value) {
            addCriterion("type_id <>", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdGreaterThan(Integer value) {
            addCriterion("type_id >", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("type_id >=", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdLessThan(Integer value) {
            addCriterion("type_id <", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdLessThanOrEqualTo(Integer value) {
            addCriterion("type_id <=", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdIn(List<Integer> values) {
            addCriterion("type_id in", values, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotIn(List<Integer> values) {
            addCriterion("type_id not in", values, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdBetween(Integer value1, Integer value2) {
            addCriterion("type_id between", value1, value2, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("type_id not between", value1, value2, "typeId");
            return (Criteria) this;
        }

        public Criteria andBrandIdIsNull() {
            addCriterion("brand_id is null");
            return (Criteria) this;
        }

        public Criteria andBrandIdIsNotNull() {
            addCriterion("brand_id is not null");
            return (Criteria) this;
        }

        public Criteria andBrandIdEqualTo(Integer value) {
            addCriterion("brand_id =", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotEqualTo(Integer value) {
            addCriterion("brand_id <>", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdGreaterThan(Integer value) {
            addCriterion("brand_id >", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("brand_id >=", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdLessThan(Integer value) {
            addCriterion("brand_id <", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdLessThanOrEqualTo(Integer value) {
            addCriterion("brand_id <=", value, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdIn(List<Integer> values) {
            addCriterion("brand_id in", values, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotIn(List<Integer> values) {
            addCriterion("brand_id not in", values, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdBetween(Integer value1, Integer value2) {
            addCriterion("brand_id between", value1, value2, "brandId");
            return (Criteria) this;
        }

        public Criteria andBrandIdNotBetween(Integer value1, Integer value2) {
            addCriterion("brand_id not between", value1, value2, "brandId");
            return (Criteria) this;
        }

        public Criteria andKeywordIdIsNull() {
            addCriterion("keyword_id is null");
            return (Criteria) this;
        }

        public Criteria andKeywordIdIsNotNull() {
            addCriterion("keyword_id is not null");
            return (Criteria) this;
        }

        public Criteria andKeywordIdEqualTo(String value) {
            addCriterion("keyword_id =", value, "keywordId");
            return (Criteria) this;
        }

        public Criteria andKeywordIdNotEqualTo(String value) {
            addCriterion("keyword_id <>", value, "keywordId");
            return (Criteria) this;
        }

        public Criteria andKeywordIdGreaterThan(String value) {
            addCriterion("keyword_id >", value, "keywordId");
            return (Criteria) this;
        }

        public Criteria andKeywordIdGreaterThanOrEqualTo(String value) {
            addCriterion("keyword_id >=", value, "keywordId");
            return (Criteria) this;
        }

        public Criteria andKeywordIdLessThan(String value) {
            addCriterion("keyword_id <", value, "keywordId");
            return (Criteria) this;
        }

        public Criteria andKeywordIdLessThanOrEqualTo(String value) {
            addCriterion("keyword_id <=", value, "keywordId");
            return (Criteria) this;
        }

        public Criteria andKeywordIdLike(String value) {
            addCriterion("keyword_id like", value, "keywordId");
            return (Criteria) this;
        }

        public Criteria andKeywordIdNotLike(String value) {
            addCriterion("keyword_id not like", value, "keywordId");
            return (Criteria) this;
        }

        public Criteria andKeywordIdIn(List<String> values) {
            addCriterion("keyword_id in", values, "keywordId");
            return (Criteria) this;
        }

        public Criteria andKeywordIdNotIn(List<String> values) {
            addCriterion("keyword_id not in", values, "keywordId");
            return (Criteria) this;
        }

        public Criteria andKeywordIdBetween(String value1, String value2) {
            addCriterion("keyword_id between", value1, value2, "keywordId");
            return (Criteria) this;
        }

        public Criteria andKeywordIdNotBetween(String value1, String value2) {
            addCriterion("keyword_id not between", value1, value2, "keywordId");
            return (Criteria) this;
        }

        public Criteria andIntegrationIsNull() {
            addCriterion("integration is null");
            return (Criteria) this;
        }

        public Criteria andIntegrationIsNotNull() {
            addCriterion("integration is not null");
            return (Criteria) this;
        }

        public Criteria andIntegrationEqualTo(Integer value) {
            addCriterion("integration =", value, "integration");
            return (Criteria) this;
        }

        public Criteria andIntegrationNotEqualTo(Integer value) {
            addCriterion("integration <>", value, "integration");
            return (Criteria) this;
        }

        public Criteria andIntegrationGreaterThan(Integer value) {
            addCriterion("integration >", value, "integration");
            return (Criteria) this;
        }

        public Criteria andIntegrationGreaterThanOrEqualTo(Integer value) {
            addCriterion("integration >=", value, "integration");
            return (Criteria) this;
        }

        public Criteria andIntegrationLessThan(Integer value) {
            addCriterion("integration <", value, "integration");
            return (Criteria) this;
        }

        public Criteria andIntegrationLessThanOrEqualTo(Integer value) {
            addCriterion("integration <=", value, "integration");
            return (Criteria) this;
        }

        public Criteria andIntegrationIn(List<Integer> values) {
            addCriterion("integration in", values, "integration");
            return (Criteria) this;
        }

        public Criteria andIntegrationNotIn(List<Integer> values) {
            addCriterion("integration not in", values, "integration");
            return (Criteria) this;
        }

        public Criteria andIntegrationBetween(Integer value1, Integer value2) {
            addCriterion("integration between", value1, value2, "integration");
            return (Criteria) this;
        }

        public Criteria andIntegrationNotBetween(Integer value1, Integer value2) {
            addCriterion("integration not between", value1, value2, "integration");
            return (Criteria) this;
        }

        public Criteria andTaskUrlIsNull() {
            addCriterion("task_url is null");
            return (Criteria) this;
        }

        public Criteria andTaskUrlIsNotNull() {
            addCriterion("task_url is not null");
            return (Criteria) this;
        }

        public Criteria andTaskUrlEqualTo(String value) {
            addCriterion("task_url =", value, "taskUrl");
            return (Criteria) this;
        }

        public Criteria andTaskUrlNotEqualTo(String value) {
            addCriterion("task_url <>", value, "taskUrl");
            return (Criteria) this;
        }

        public Criteria andTaskUrlGreaterThan(String value) {
            addCriterion("task_url >", value, "taskUrl");
            return (Criteria) this;
        }

        public Criteria andTaskUrlGreaterThanOrEqualTo(String value) {
            addCriterion("task_url >=", value, "taskUrl");
            return (Criteria) this;
        }

        public Criteria andTaskUrlLessThan(String value) {
            addCriterion("task_url <", value, "taskUrl");
            return (Criteria) this;
        }

        public Criteria andTaskUrlLessThanOrEqualTo(String value) {
            addCriterion("task_url <=", value, "taskUrl");
            return (Criteria) this;
        }

        public Criteria andTaskUrlLike(String value) {
            addCriterion("task_url like", value, "taskUrl");
            return (Criteria) this;
        }

        public Criteria andTaskUrlNotLike(String value) {
            addCriterion("task_url not like", value, "taskUrl");
            return (Criteria) this;
        }

        public Criteria andTaskUrlIn(List<String> values) {
            addCriterion("task_url in", values, "taskUrl");
            return (Criteria) this;
        }

        public Criteria andTaskUrlNotIn(List<String> values) {
            addCriterion("task_url not in", values, "taskUrl");
            return (Criteria) this;
        }

        public Criteria andTaskUrlBetween(String value1, String value2) {
            addCriterion("task_url between", value1, value2, "taskUrl");
            return (Criteria) this;
        }

        public Criteria andTaskUrlNotBetween(String value1, String value2) {
            addCriterion("task_url not between", value1, value2, "taskUrl");
            return (Criteria) this;
        }

        public Criteria andApplyEndDateIsNull() {
            addCriterion("apply_end_date is null");
            return (Criteria) this;
        }

        public Criteria andApplyEndDateIsNotNull() {
            addCriterion("apply_end_date is not null");
            return (Criteria) this;
        }

        public Criteria andApplyEndDateEqualTo(Date value) {
            addCriterion("apply_end_date =", value, "applyEndDate");
            return (Criteria) this;
        }

        public Criteria andApplyEndDateNotEqualTo(Date value) {
            addCriterion("apply_end_date <>", value, "applyEndDate");
            return (Criteria) this;
        }

        public Criteria andApplyEndDateGreaterThan(Date value) {
            addCriterion("apply_end_date >", value, "applyEndDate");
            return (Criteria) this;
        }

        public Criteria andApplyEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("apply_end_date >=", value, "applyEndDate");
            return (Criteria) this;
        }

        public Criteria andApplyEndDateLessThan(Date value) {
            addCriterion("apply_end_date <", value, "applyEndDate");
            return (Criteria) this;
        }

        public Criteria andApplyEndDateLessThanOrEqualTo(Date value) {
            addCriterion("apply_end_date <=", value, "applyEndDate");
            return (Criteria) this;
        }

        public Criteria andApplyEndDateIn(List<Date> values) {
            addCriterion("apply_end_date in", values, "applyEndDate");
            return (Criteria) this;
        }

        public Criteria andApplyEndDateNotIn(List<Date> values) {
            addCriterion("apply_end_date not in", values, "applyEndDate");
            return (Criteria) this;
        }

        public Criteria andApplyEndDateBetween(Date value1, Date value2) {
            addCriterion("apply_end_date between", value1, value2, "applyEndDate");
            return (Criteria) this;
        }

        public Criteria andApplyEndDateNotBetween(Date value1, Date value2) {
            addCriterion("apply_end_date not between", value1, value2, "applyEndDate");
            return (Criteria) this;
        }

        public Criteria andApplyCountIsNull() {
            addCriterion("apply_count is null");
            return (Criteria) this;
        }

        public Criteria andApplyCountIsNotNull() {
            addCriterion("apply_count is not null");
            return (Criteria) this;
        }

        public Criteria andApplyCountEqualTo(Integer value) {
            addCriterion("apply_count =", value, "applyCount");
            return (Criteria) this;
        }

        public Criteria andApplyCountNotEqualTo(Integer value) {
            addCriterion("apply_count <>", value, "applyCount");
            return (Criteria) this;
        }

        public Criteria andApplyCountGreaterThan(Integer value) {
            addCriterion("apply_count >", value, "applyCount");
            return (Criteria) this;
        }

        public Criteria andApplyCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("apply_count >=", value, "applyCount");
            return (Criteria) this;
        }

        public Criteria andApplyCountLessThan(Integer value) {
            addCriterion("apply_count <", value, "applyCount");
            return (Criteria) this;
        }

        public Criteria andApplyCountLessThanOrEqualTo(Integer value) {
            addCriterion("apply_count <=", value, "applyCount");
            return (Criteria) this;
        }

        public Criteria andApplyCountIn(List<Integer> values) {
            addCriterion("apply_count in", values, "applyCount");
            return (Criteria) this;
        }

        public Criteria andApplyCountNotIn(List<Integer> values) {
            addCriterion("apply_count not in", values, "applyCount");
            return (Criteria) this;
        }

        public Criteria andApplyCountBetween(Integer value1, Integer value2) {
            addCriterion("apply_count between", value1, value2, "applyCount");
            return (Criteria) this;
        }

        public Criteria andApplyCountNotBetween(Integer value1, Integer value2) {
            addCriterion("apply_count not between", value1, value2, "applyCount");
            return (Criteria) this;
        }

        public Criteria andApplyTotalIsNull() {
            addCriterion("apply_total is null");
            return (Criteria) this;
        }

        public Criteria andApplyTotalIsNotNull() {
            addCriterion("apply_total is not null");
            return (Criteria) this;
        }

        public Criteria andApplyTotalEqualTo(Integer value) {
            addCriterion("apply_total =", value, "applyTotal");
            return (Criteria) this;
        }

        public Criteria andApplyTotalNotEqualTo(Integer value) {
            addCriterion("apply_total <>", value, "applyTotal");
            return (Criteria) this;
        }

        public Criteria andApplyTotalGreaterThan(Integer value) {
            addCriterion("apply_total >", value, "applyTotal");
            return (Criteria) this;
        }

        public Criteria andApplyTotalGreaterThanOrEqualTo(Integer value) {
            addCriterion("apply_total >=", value, "applyTotal");
            return (Criteria) this;
        }

        public Criteria andApplyTotalLessThan(Integer value) {
            addCriterion("apply_total <", value, "applyTotal");
            return (Criteria) this;
        }

        public Criteria andApplyTotalLessThanOrEqualTo(Integer value) {
            addCriterion("apply_total <=", value, "applyTotal");
            return (Criteria) this;
        }

        public Criteria andApplyTotalIn(List<Integer> values) {
            addCriterion("apply_total in", values, "applyTotal");
            return (Criteria) this;
        }

        public Criteria andApplyTotalNotIn(List<Integer> values) {
            addCriterion("apply_total not in", values, "applyTotal");
            return (Criteria) this;
        }

        public Criteria andApplyTotalBetween(Integer value1, Integer value2) {
            addCriterion("apply_total between", value1, value2, "applyTotal");
            return (Criteria) this;
        }

        public Criteria andApplyTotalNotBetween(Integer value1, Integer value2) {
            addCriterion("apply_total not between", value1, value2, "applyTotal");
            return (Criteria) this;
        }

        public Criteria andEntityRewardIsNull() {
            addCriterion("entity_reward is null");
            return (Criteria) this;
        }

        public Criteria andEntityRewardIsNotNull() {
            addCriterion("entity_reward is not null");
            return (Criteria) this;
        }

        public Criteria andEntityRewardEqualTo(String value) {
            addCriterion("entity_reward =", value, "entityReward");
            return (Criteria) this;
        }

        public Criteria andEntityRewardNotEqualTo(String value) {
            addCriterion("entity_reward <>", value, "entityReward");
            return (Criteria) this;
        }

        public Criteria andEntityRewardGreaterThan(String value) {
            addCriterion("entity_reward >", value, "entityReward");
            return (Criteria) this;
        }

        public Criteria andEntityRewardGreaterThanOrEqualTo(String value) {
            addCriterion("entity_reward >=", value, "entityReward");
            return (Criteria) this;
        }

        public Criteria andEntityRewardLessThan(String value) {
            addCriterion("entity_reward <", value, "entityReward");
            return (Criteria) this;
        }

        public Criteria andEntityRewardLessThanOrEqualTo(String value) {
            addCriterion("entity_reward <=", value, "entityReward");
            return (Criteria) this;
        }

        public Criteria andEntityRewardLike(String value) {
            addCriterion("entity_reward like", value, "entityReward");
            return (Criteria) this;
        }

        public Criteria andEntityRewardNotLike(String value) {
            addCriterion("entity_reward not like", value, "entityReward");
            return (Criteria) this;
        }

        public Criteria andEntityRewardIn(List<String> values) {
            addCriterion("entity_reward in", values, "entityReward");
            return (Criteria) this;
        }

        public Criteria andEntityRewardNotIn(List<String> values) {
            addCriterion("entity_reward not in", values, "entityReward");
            return (Criteria) this;
        }

        public Criteria andEntityRewardBetween(String value1, String value2) {
            addCriterion("entity_reward between", value1, value2, "entityReward");
            return (Criteria) this;
        }

        public Criteria andEntityRewardNotBetween(String value1, String value2) {
            addCriterion("entity_reward not between", value1, value2, "entityReward");
            return (Criteria) this;
        }

        public Criteria andEntityTotalIsNull() {
            addCriterion("entity_total is null");
            return (Criteria) this;
        }

        public Criteria andEntityTotalIsNotNull() {
            addCriterion("entity_total is not null");
            return (Criteria) this;
        }

        public Criteria andEntityTotalEqualTo(Integer value) {
            addCriterion("entity_total =", value, "entityTotal");
            return (Criteria) this;
        }

        public Criteria andEntityTotalNotEqualTo(Integer value) {
            addCriterion("entity_total <>", value, "entityTotal");
            return (Criteria) this;
        }

        public Criteria andEntityTotalGreaterThan(Integer value) {
            addCriterion("entity_total >", value, "entityTotal");
            return (Criteria) this;
        }

        public Criteria andEntityTotalGreaterThanOrEqualTo(Integer value) {
            addCriterion("entity_total >=", value, "entityTotal");
            return (Criteria) this;
        }

        public Criteria andEntityTotalLessThan(Integer value) {
            addCriterion("entity_total <", value, "entityTotal");
            return (Criteria) this;
        }

        public Criteria andEntityTotalLessThanOrEqualTo(Integer value) {
            addCriterion("entity_total <=", value, "entityTotal");
            return (Criteria) this;
        }

        public Criteria andEntityTotalIn(List<Integer> values) {
            addCriterion("entity_total in", values, "entityTotal");
            return (Criteria) this;
        }

        public Criteria andEntityTotalNotIn(List<Integer> values) {
            addCriterion("entity_total not in", values, "entityTotal");
            return (Criteria) this;
        }

        public Criteria andEntityTotalBetween(Integer value1, Integer value2) {
            addCriterion("entity_total between", value1, value2, "entityTotal");
            return (Criteria) this;
        }

        public Criteria andEntityTotalNotBetween(Integer value1, Integer value2) {
            addCriterion("entity_total not between", value1, value2, "entityTotal");
            return (Criteria) this;
        }

        public Criteria andTaskBriefIsNull() {
            addCriterion("task_brief is null");
            return (Criteria) this;
        }

        public Criteria andTaskBriefIsNotNull() {
            addCriterion("task_brief is not null");
            return (Criteria) this;
        }

        public Criteria andTaskBriefEqualTo(String value) {
            addCriterion("task_brief =", value, "taskBrief");
            return (Criteria) this;
        }

        public Criteria andTaskBriefNotEqualTo(String value) {
            addCriterion("task_brief <>", value, "taskBrief");
            return (Criteria) this;
        }

        public Criteria andTaskBriefGreaterThan(String value) {
            addCriterion("task_brief >", value, "taskBrief");
            return (Criteria) this;
        }

        public Criteria andTaskBriefGreaterThanOrEqualTo(String value) {
            addCriterion("task_brief >=", value, "taskBrief");
            return (Criteria) this;
        }

        public Criteria andTaskBriefLessThan(String value) {
            addCriterion("task_brief <", value, "taskBrief");
            return (Criteria) this;
        }

        public Criteria andTaskBriefLessThanOrEqualTo(String value) {
            addCriterion("task_brief <=", value, "taskBrief");
            return (Criteria) this;
        }

        public Criteria andTaskBriefLike(String value) {
            addCriterion("task_brief like", value, "taskBrief");
            return (Criteria) this;
        }

        public Criteria andTaskBriefNotLike(String value) {
            addCriterion("task_brief not like", value, "taskBrief");
            return (Criteria) this;
        }

        public Criteria andTaskBriefIn(List<String> values) {
            addCriterion("task_brief in", values, "taskBrief");
            return (Criteria) this;
        }

        public Criteria andTaskBriefNotIn(List<String> values) {
            addCriterion("task_brief not in", values, "taskBrief");
            return (Criteria) this;
        }

        public Criteria andTaskBriefBetween(String value1, String value2) {
            addCriterion("task_brief between", value1, value2, "taskBrief");
            return (Criteria) this;
        }

        public Criteria andTaskBriefNotBetween(String value1, String value2) {
            addCriterion("task_brief not between", value1, value2, "taskBrief");
            return (Criteria) this;
        }

        public Criteria andMoreDetailIsNull() {
            addCriterion("more_detail is null");
            return (Criteria) this;
        }

        public Criteria andMoreDetailIsNotNull() {
            addCriterion("more_detail is not null");
            return (Criteria) this;
        }

        public Criteria andMoreDetailEqualTo(String value) {
            addCriterion("more_detail =", value, "moreDetail");
            return (Criteria) this;
        }

        public Criteria andMoreDetailNotEqualTo(String value) {
            addCriterion("more_detail <>", value, "moreDetail");
            return (Criteria) this;
        }

        public Criteria andMoreDetailGreaterThan(String value) {
            addCriterion("more_detail >", value, "moreDetail");
            return (Criteria) this;
        }

        public Criteria andMoreDetailGreaterThanOrEqualTo(String value) {
            addCriterion("more_detail >=", value, "moreDetail");
            return (Criteria) this;
        }

        public Criteria andMoreDetailLessThan(String value) {
            addCriterion("more_detail <", value, "moreDetail");
            return (Criteria) this;
        }

        public Criteria andMoreDetailLessThanOrEqualTo(String value) {
            addCriterion("more_detail <=", value, "moreDetail");
            return (Criteria) this;
        }

        public Criteria andMoreDetailLike(String value) {
            addCriterion("more_detail like", value, "moreDetail");
            return (Criteria) this;
        }

        public Criteria andMoreDetailNotLike(String value) {
            addCriterion("more_detail not like", value, "moreDetail");
            return (Criteria) this;
        }

        public Criteria andMoreDetailIn(List<String> values) {
            addCriterion("more_detail in", values, "moreDetail");
            return (Criteria) this;
        }

        public Criteria andMoreDetailNotIn(List<String> values) {
            addCriterion("more_detail not in", values, "moreDetail");
            return (Criteria) this;
        }

        public Criteria andMoreDetailBetween(String value1, String value2) {
            addCriterion("more_detail between", value1, value2, "moreDetail");
            return (Criteria) this;
        }

        public Criteria andMoreDetailNotBetween(String value1, String value2) {
            addCriterion("more_detail not between", value1, value2, "moreDetail");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andEndDateIsNull() {
            addCriterion("end_date is null");
            return (Criteria) this;
        }

        public Criteria andEndDateIsNotNull() {
            addCriterion("end_date is not null");
            return (Criteria) this;
        }

        public Criteria andEndDateEqualTo(Date value) {
            addCriterion("end_date =", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotEqualTo(Date value) {
            addCriterion("end_date <>", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateGreaterThan(Date value) {
            addCriterion("end_date >", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("end_date >=", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateLessThan(Date value) {
            addCriterion("end_date <", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateLessThanOrEqualTo(Date value) {
            addCriterion("end_date <=", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateIn(List<Date> values) {
            addCriterion("end_date in", values, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotIn(List<Date> values) {
            addCriterion("end_date not in", values, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateBetween(Date value1, Date value2) {
            addCriterion("end_date between", value1, value2, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotBetween(Date value1, Date value2) {
            addCriterion("end_date not between", value1, value2, "endDate");
            return (Criteria) this;
        }

        public Criteria andIsShelveIsNull() {
            addCriterion("is_shelve is null");
            return (Criteria) this;
        }

        public Criteria andIsShelveIsNotNull() {
            addCriterion("is_shelve is not null");
            return (Criteria) this;
        }

        public Criteria andIsShelveEqualTo(String value) {
            addCriterion("is_shelve =", value, "isShelve");
            return (Criteria) this;
        }

        public Criteria andIsShelveNotEqualTo(String value) {
            addCriterion("is_shelve <>", value, "isShelve");
            return (Criteria) this;
        }

        public Criteria andIsShelveGreaterThan(String value) {
            addCriterion("is_shelve >", value, "isShelve");
            return (Criteria) this;
        }

        public Criteria andIsShelveGreaterThanOrEqualTo(String value) {
            addCriterion("is_shelve >=", value, "isShelve");
            return (Criteria) this;
        }

        public Criteria andIsShelveLessThan(String value) {
            addCriterion("is_shelve <", value, "isShelve");
            return (Criteria) this;
        }

        public Criteria andIsShelveLessThanOrEqualTo(String value) {
            addCriterion("is_shelve <=", value, "isShelve");
            return (Criteria) this;
        }

        public Criteria andIsShelveLike(String value) {
            addCriterion("is_shelve like", value, "isShelve");
            return (Criteria) this;
        }

        public Criteria andIsShelveNotLike(String value) {
            addCriterion("is_shelve not like", value, "isShelve");
            return (Criteria) this;
        }

        public Criteria andIsShelveIn(List<String> values) {
            addCriterion("is_shelve in", values, "isShelve");
            return (Criteria) this;
        }

        public Criteria andIsShelveNotIn(List<String> values) {
            addCriterion("is_shelve not in", values, "isShelve");
            return (Criteria) this;
        }

        public Criteria andIsShelveBetween(String value1, String value2) {
            addCriterion("is_shelve between", value1, value2, "isShelve");
            return (Criteria) this;
        }

        public Criteria andIsShelveNotBetween(String value1, String value2) {
            addCriterion("is_shelve not between", value1, value2, "isShelve");
            return (Criteria) this;
        }

        public Criteria andShelveDateIsNull() {
            addCriterion("shelve_date is null");
            return (Criteria) this;
        }

        public Criteria andShelveDateIsNotNull() {
            addCriterion("shelve_date is not null");
            return (Criteria) this;
        }

        public Criteria andShelveDateEqualTo(Date value) {
            addCriterion("shelve_date =", value, "shelveDate");
            return (Criteria) this;
        }

        public Criteria andShelveDateNotEqualTo(Date value) {
            addCriterion("shelve_date <>", value, "shelveDate");
            return (Criteria) this;
        }

        public Criteria andShelveDateGreaterThan(Date value) {
            addCriterion("shelve_date >", value, "shelveDate");
            return (Criteria) this;
        }

        public Criteria andShelveDateGreaterThanOrEqualTo(Date value) {
            addCriterion("shelve_date >=", value, "shelveDate");
            return (Criteria) this;
        }

        public Criteria andShelveDateLessThan(Date value) {
            addCriterion("shelve_date <", value, "shelveDate");
            return (Criteria) this;
        }

        public Criteria andShelveDateLessThanOrEqualTo(Date value) {
            addCriterion("shelve_date <=", value, "shelveDate");
            return (Criteria) this;
        }

        public Criteria andShelveDateIn(List<Date> values) {
            addCriterion("shelve_date in", values, "shelveDate");
            return (Criteria) this;
        }

        public Criteria andShelveDateNotIn(List<Date> values) {
            addCriterion("shelve_date not in", values, "shelveDate");
            return (Criteria) this;
        }

        public Criteria andShelveDateBetween(Date value1, Date value2) {
            addCriterion("shelve_date between", value1, value2, "shelveDate");
            return (Criteria) this;
        }

        public Criteria andShelveDateNotBetween(Date value1, Date value2) {
            addCriterion("shelve_date not between", value1, value2, "shelveDate");
            return (Criteria) this;
        }

        public Criteria andShelveDateFirstIsNull() {
            addCriterion("shelve_date_first is null");
            return (Criteria) this;
        }

        public Criteria andShelveDateFirstIsNotNull() {
            addCriterion("shelve_date_first is not null");
            return (Criteria) this;
        }

        public Criteria andShelveDateFirstEqualTo(Date value) {
            addCriterion("shelve_date_first =", value, "shelveDateFirst");
            return (Criteria) this;
        }

        public Criteria andShelveDateFirstNotEqualTo(Date value) {
            addCriterion("shelve_date_first <>", value, "shelveDateFirst");
            return (Criteria) this;
        }

        public Criteria andShelveDateFirstGreaterThan(Date value) {
            addCriterion("shelve_date_first >", value, "shelveDateFirst");
            return (Criteria) this;
        }

        public Criteria andShelveDateFirstGreaterThanOrEqualTo(Date value) {
            addCriterion("shelve_date_first >=", value, "shelveDateFirst");
            return (Criteria) this;
        }

        public Criteria andShelveDateFirstLessThan(Date value) {
            addCriterion("shelve_date_first <", value, "shelveDateFirst");
            return (Criteria) this;
        }

        public Criteria andShelveDateFirstLessThanOrEqualTo(Date value) {
            addCriterion("shelve_date_first <=", value, "shelveDateFirst");
            return (Criteria) this;
        }

        public Criteria andShelveDateFirstIn(List<Date> values) {
            addCriterion("shelve_date_first in", values, "shelveDateFirst");
            return (Criteria) this;
        }

        public Criteria andShelveDateFirstNotIn(List<Date> values) {
            addCriterion("shelve_date_first not in", values, "shelveDateFirst");
            return (Criteria) this;
        }

        public Criteria andShelveDateFirstBetween(Date value1, Date value2) {
            addCriterion("shelve_date_first between", value1, value2, "shelveDateFirst");
            return (Criteria) this;
        }

        public Criteria andShelveDateFirstNotBetween(Date value1, Date value2) {
            addCriterion("shelve_date_first not between", value1, value2, "shelveDateFirst");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindIsNull() {
            addCriterion("is_shelve_remind is null");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindIsNotNull() {
            addCriterion("is_shelve_remind is not null");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindEqualTo(String value) {
            addCriterion("is_shelve_remind =", value, "isShelveRemind");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindNotEqualTo(String value) {
            addCriterion("is_shelve_remind <>", value, "isShelveRemind");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindGreaterThan(String value) {
            addCriterion("is_shelve_remind >", value, "isShelveRemind");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindGreaterThanOrEqualTo(String value) {
            addCriterion("is_shelve_remind >=", value, "isShelveRemind");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindLessThan(String value) {
            addCriterion("is_shelve_remind <", value, "isShelveRemind");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindLessThanOrEqualTo(String value) {
            addCriterion("is_shelve_remind <=", value, "isShelveRemind");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindLike(String value) {
            addCriterion("is_shelve_remind like", value, "isShelveRemind");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindNotLike(String value) {
            addCriterion("is_shelve_remind not like", value, "isShelveRemind");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindIn(List<String> values) {
            addCriterion("is_shelve_remind in", values, "isShelveRemind");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindNotIn(List<String> values) {
            addCriterion("is_shelve_remind not in", values, "isShelveRemind");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindBetween(String value1, String value2) {
            addCriterion("is_shelve_remind between", value1, value2, "isShelveRemind");
            return (Criteria) this;
        }

        public Criteria andIsShelveRemindNotBetween(String value1, String value2) {
            addCriterion("is_shelve_remind not between", value1, value2, "isShelveRemind");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNull() {
            addCriterion("is_del is null");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNotNull() {
            addCriterion("is_del is not null");
            return (Criteria) this;
        }

        public Criteria andIsDelEqualTo(String value) {
            addCriterion("is_del =", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotEqualTo(String value) {
            addCriterion("is_del <>", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThan(String value) {
            addCriterion("is_del >", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThanOrEqualTo(String value) {
            addCriterion("is_del >=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThan(String value) {
            addCriterion("is_del <", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThanOrEqualTo(String value) {
            addCriterion("is_del <=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLike(String value) {
            addCriterion("is_del like", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotLike(String value) {
            addCriterion("is_del not like", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelIn(List<String> values) {
            addCriterion("is_del in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotIn(List<String> values) {
            addCriterion("is_del not in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelBetween(String value1, String value2) {
            addCriterion("is_del between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotBetween(String value1, String value2) {
            addCriterion("is_del not between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andElecRewardIdIsNull() {
            addCriterion("elec_reward_id is null");
            return (Criteria) this;
        }

        public Criteria andElecRewardIdIsNotNull() {
            addCriterion("elec_reward_id is not null");
            return (Criteria) this;
        }

        public Criteria andElecRewardIdEqualTo(Integer value) {
            addCriterion("elec_reward_id =", value, "elecRewardId");
            return (Criteria) this;
        }

        public Criteria andElecRewardIdNotEqualTo(Integer value) {
            addCriterion("elec_reward_id <>", value, "elecRewardId");
            return (Criteria) this;
        }

        public Criteria andElecRewardIdGreaterThan(Integer value) {
            addCriterion("elec_reward_id >", value, "elecRewardId");
            return (Criteria) this;
        }

        public Criteria andElecRewardIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("elec_reward_id >=", value, "elecRewardId");
            return (Criteria) this;
        }

        public Criteria andElecRewardIdLessThan(Integer value) {
            addCriterion("elec_reward_id <", value, "elecRewardId");
            return (Criteria) this;
        }

        public Criteria andElecRewardIdLessThanOrEqualTo(Integer value) {
            addCriterion("elec_reward_id <=", value, "elecRewardId");
            return (Criteria) this;
        }

        public Criteria andElecRewardIdIn(List<Integer> values) {
            addCriterion("elec_reward_id in", values, "elecRewardId");
            return (Criteria) this;
        }

        public Criteria andElecRewardIdNotIn(List<Integer> values) {
            addCriterion("elec_reward_id not in", values, "elecRewardId");
            return (Criteria) this;
        }

        public Criteria andElecRewardIdBetween(Integer value1, Integer value2) {
            addCriterion("elec_reward_id between", value1, value2, "elecRewardId");
            return (Criteria) this;
        }

        public Criteria andElecRewardIdNotBetween(Integer value1, Integer value2) {
            addCriterion("elec_reward_id not between", value1, value2, "elecRewardId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}