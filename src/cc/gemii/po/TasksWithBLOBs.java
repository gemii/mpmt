package cc.gemii.po;

public class TasksWithBLOBs extends Tasks {
    private String detailRemark;

    private String operateRemark;

    private String putRemark;

    public String getDetailRemark() {
        return detailRemark;
    }

    public void setDetailRemark(String detailRemark) {
        this.detailRemark = detailRemark == null ? null : detailRemark.trim();
    }

    public String getOperateRemark() {
        return operateRemark;
    }

    public void setOperateRemark(String operateRemark) {
        this.operateRemark = operateRemark == null ? null : operateRemark.trim();
    }

    public String getPutRemark() {
        return putRemark;
    }

    public void setPutRemark(String putRemark) {
        this.putRemark = putRemark == null ? null : putRemark.trim();
    }
}