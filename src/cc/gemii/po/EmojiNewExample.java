package cc.gemii.po;

import java.util.ArrayList;
import java.util.List;

public class EmojiNewExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public EmojiNewExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andUnicodeIsNull() {
            addCriterion("unicode is null");
            return (Criteria) this;
        }

        public Criteria andUnicodeIsNotNull() {
            addCriterion("unicode is not null");
            return (Criteria) this;
        }

        public Criteria andUnicodeEqualTo(String value) {
            addCriterion("unicode =", value, "unicode");
            return (Criteria) this;
        }

        public Criteria andUnicodeNotEqualTo(String value) {
            addCriterion("unicode <>", value, "unicode");
            return (Criteria) this;
        }

        public Criteria andUnicodeGreaterThan(String value) {
            addCriterion("unicode >", value, "unicode");
            return (Criteria) this;
        }

        public Criteria andUnicodeGreaterThanOrEqualTo(String value) {
            addCriterion("unicode >=", value, "unicode");
            return (Criteria) this;
        }

        public Criteria andUnicodeLessThan(String value) {
            addCriterion("unicode <", value, "unicode");
            return (Criteria) this;
        }

        public Criteria andUnicodeLessThanOrEqualTo(String value) {
            addCriterion("unicode <=", value, "unicode");
            return (Criteria) this;
        }

        public Criteria andUnicodeLike(String value) {
            addCriterion("unicode like", value, "unicode");
            return (Criteria) this;
        }

        public Criteria andUnicodeNotLike(String value) {
            addCriterion("unicode not like", value, "unicode");
            return (Criteria) this;
        }

        public Criteria andUnicodeIn(List<String> values) {
            addCriterion("unicode in", values, "unicode");
            return (Criteria) this;
        }

        public Criteria andUnicodeNotIn(List<String> values) {
            addCriterion("unicode not in", values, "unicode");
            return (Criteria) this;
        }

        public Criteria andUnicodeBetween(String value1, String value2) {
            addCriterion("unicode between", value1, value2, "unicode");
            return (Criteria) this;
        }

        public Criteria andUnicodeNotBetween(String value1, String value2) {
            addCriterion("unicode not between", value1, value2, "unicode");
            return (Criteria) this;
        }

        public Criteria andUtf8IsNull() {
            addCriterion("utf8 is null");
            return (Criteria) this;
        }

        public Criteria andUtf8IsNotNull() {
            addCriterion("utf8 is not null");
            return (Criteria) this;
        }

        public Criteria andUtf8EqualTo(String value) {
            addCriterion("utf8 =", value, "utf8");
            return (Criteria) this;
        }

        public Criteria andUtf8NotEqualTo(String value) {
            addCriterion("utf8 <>", value, "utf8");
            return (Criteria) this;
        }

        public Criteria andUtf8GreaterThan(String value) {
            addCriterion("utf8 >", value, "utf8");
            return (Criteria) this;
        }

        public Criteria andUtf8GreaterThanOrEqualTo(String value) {
            addCriterion("utf8 >=", value, "utf8");
            return (Criteria) this;
        }

        public Criteria andUtf8LessThan(String value) {
            addCriterion("utf8 <", value, "utf8");
            return (Criteria) this;
        }

        public Criteria andUtf8LessThanOrEqualTo(String value) {
            addCriterion("utf8 <=", value, "utf8");
            return (Criteria) this;
        }

        public Criteria andUtf8Like(String value) {
            addCriterion("utf8 like", value, "utf8");
            return (Criteria) this;
        }

        public Criteria andUtf8NotLike(String value) {
            addCriterion("utf8 not like", value, "utf8");
            return (Criteria) this;
        }

        public Criteria andUtf8In(List<String> values) {
            addCriterion("utf8 in", values, "utf8");
            return (Criteria) this;
        }

        public Criteria andUtf8NotIn(List<String> values) {
            addCriterion("utf8 not in", values, "utf8");
            return (Criteria) this;
        }

        public Criteria andUtf8Between(String value1, String value2) {
            addCriterion("utf8 between", value1, value2, "utf8");
            return (Criteria) this;
        }

        public Criteria andUtf8NotBetween(String value1, String value2) {
            addCriterion("utf8 not between", value1, value2, "utf8");
            return (Criteria) this;
        }

        public Criteria andUtf16IsNull() {
            addCriterion("utf16 is null");
            return (Criteria) this;
        }

        public Criteria andUtf16IsNotNull() {
            addCriterion("utf16 is not null");
            return (Criteria) this;
        }

        public Criteria andUtf16EqualTo(String value) {
            addCriterion("utf16 =", value, "utf16");
            return (Criteria) this;
        }

        public Criteria andUtf16NotEqualTo(String value) {
            addCriterion("utf16 <>", value, "utf16");
            return (Criteria) this;
        }

        public Criteria andUtf16GreaterThan(String value) {
            addCriterion("utf16 >", value, "utf16");
            return (Criteria) this;
        }

        public Criteria andUtf16GreaterThanOrEqualTo(String value) {
            addCriterion("utf16 >=", value, "utf16");
            return (Criteria) this;
        }

        public Criteria andUtf16LessThan(String value) {
            addCriterion("utf16 <", value, "utf16");
            return (Criteria) this;
        }

        public Criteria andUtf16LessThanOrEqualTo(String value) {
            addCriterion("utf16 <=", value, "utf16");
            return (Criteria) this;
        }

        public Criteria andUtf16Like(String value) {
            addCriterion("utf16 like", value, "utf16");
            return (Criteria) this;
        }

        public Criteria andUtf16NotLike(String value) {
            addCriterion("utf16 not like", value, "utf16");
            return (Criteria) this;
        }

        public Criteria andUtf16In(List<String> values) {
            addCriterion("utf16 in", values, "utf16");
            return (Criteria) this;
        }

        public Criteria andUtf16NotIn(List<String> values) {
            addCriterion("utf16 not in", values, "utf16");
            return (Criteria) this;
        }

        public Criteria andUtf16Between(String value1, String value2) {
            addCriterion("utf16 between", value1, value2, "utf16");
            return (Criteria) this;
        }

        public Criteria andUtf16NotBetween(String value1, String value2) {
            addCriterion("utf16 not between", value1, value2, "utf16");
            return (Criteria) this;
        }

        public Criteria andSbunicodeIsNull() {
            addCriterion("sbunicode is null");
            return (Criteria) this;
        }

        public Criteria andSbunicodeIsNotNull() {
            addCriterion("sbunicode is not null");
            return (Criteria) this;
        }

        public Criteria andSbunicodeEqualTo(String value) {
            addCriterion("sbunicode =", value, "sbunicode");
            return (Criteria) this;
        }

        public Criteria andSbunicodeNotEqualTo(String value) {
            addCriterion("sbunicode <>", value, "sbunicode");
            return (Criteria) this;
        }

        public Criteria andSbunicodeGreaterThan(String value) {
            addCriterion("sbunicode >", value, "sbunicode");
            return (Criteria) this;
        }

        public Criteria andSbunicodeGreaterThanOrEqualTo(String value) {
            addCriterion("sbunicode >=", value, "sbunicode");
            return (Criteria) this;
        }

        public Criteria andSbunicodeLessThan(String value) {
            addCriterion("sbunicode <", value, "sbunicode");
            return (Criteria) this;
        }

        public Criteria andSbunicodeLessThanOrEqualTo(String value) {
            addCriterion("sbunicode <=", value, "sbunicode");
            return (Criteria) this;
        }

        public Criteria andSbunicodeLike(String value) {
            addCriterion("sbunicode like", value, "sbunicode");
            return (Criteria) this;
        }

        public Criteria andSbunicodeNotLike(String value) {
            addCriterion("sbunicode not like", value, "sbunicode");
            return (Criteria) this;
        }

        public Criteria andSbunicodeIn(List<String> values) {
            addCriterion("sbunicode in", values, "sbunicode");
            return (Criteria) this;
        }

        public Criteria andSbunicodeNotIn(List<String> values) {
            addCriterion("sbunicode not in", values, "sbunicode");
            return (Criteria) this;
        }

        public Criteria andSbunicodeBetween(String value1, String value2) {
            addCriterion("sbunicode between", value1, value2, "sbunicode");
            return (Criteria) this;
        }

        public Criteria andSbunicodeNotBetween(String value1, String value2) {
            addCriterion("sbunicode not between", value1, value2, "sbunicode");
            return (Criteria) this;
        }

        public Criteria andFilenameIsNull() {
            addCriterion("filename is null");
            return (Criteria) this;
        }

        public Criteria andFilenameIsNotNull() {
            addCriterion("filename is not null");
            return (Criteria) this;
        }

        public Criteria andFilenameEqualTo(String value) {
            addCriterion("filename =", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameNotEqualTo(String value) {
            addCriterion("filename <>", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameGreaterThan(String value) {
            addCriterion("filename >", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameGreaterThanOrEqualTo(String value) {
            addCriterion("filename >=", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameLessThan(String value) {
            addCriterion("filename <", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameLessThanOrEqualTo(String value) {
            addCriterion("filename <=", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameLike(String value) {
            addCriterion("filename like", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameNotLike(String value) {
            addCriterion("filename not like", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameIn(List<String> values) {
            addCriterion("filename in", values, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameNotIn(List<String> values) {
            addCriterion("filename not in", values, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameBetween(String value1, String value2) {
            addCriterion("filename between", value1, value2, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameNotBetween(String value1, String value2) {
            addCriterion("filename not between", value1, value2, "filename");
            return (Criteria) this;
        }

        public Criteria andMapcodeIsNull() {
            addCriterion("mapcode is null");
            return (Criteria) this;
        }

        public Criteria andMapcodeIsNotNull() {
            addCriterion("mapcode is not null");
            return (Criteria) this;
        }

        public Criteria andMapcodeEqualTo(String value) {
            addCriterion("mapcode =", value, "mapcode");
            return (Criteria) this;
        }

        public Criteria andMapcodeNotEqualTo(String value) {
            addCriterion("mapcode <>", value, "mapcode");
            return (Criteria) this;
        }

        public Criteria andMapcodeGreaterThan(String value) {
            addCriterion("mapcode >", value, "mapcode");
            return (Criteria) this;
        }

        public Criteria andMapcodeGreaterThanOrEqualTo(String value) {
            addCriterion("mapcode >=", value, "mapcode");
            return (Criteria) this;
        }

        public Criteria andMapcodeLessThan(String value) {
            addCriterion("mapcode <", value, "mapcode");
            return (Criteria) this;
        }

        public Criteria andMapcodeLessThanOrEqualTo(String value) {
            addCriterion("mapcode <=", value, "mapcode");
            return (Criteria) this;
        }

        public Criteria andMapcodeLike(String value) {
            addCriterion("mapcode like", value, "mapcode");
            return (Criteria) this;
        }

        public Criteria andMapcodeNotLike(String value) {
            addCriterion("mapcode not like", value, "mapcode");
            return (Criteria) this;
        }

        public Criteria andMapcodeIn(List<String> values) {
            addCriterion("mapcode in", values, "mapcode");
            return (Criteria) this;
        }

        public Criteria andMapcodeNotIn(List<String> values) {
            addCriterion("mapcode not in", values, "mapcode");
            return (Criteria) this;
        }

        public Criteria andMapcodeBetween(String value1, String value2) {
            addCriterion("mapcode between", value1, value2, "mapcode");
            return (Criteria) this;
        }

        public Criteria andMapcodeNotBetween(String value1, String value2) {
            addCriterion("mapcode not between", value1, value2, "mapcode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}