package cc.gemii.po;

import java.util.Date;

public class Tasks {
    private Integer id;

    private String title;

    private Integer typeId;

    private Integer brandId;

    private String keywordId;

    private Integer integration;

    private String taskUrl;

    private Date applyEndDate;

    private Integer applyCount;

    private Integer applyTotal;

    private String entityReward;

    private Integer entityTotal;

    private String taskBrief;

    private String moreDetail;

    private Date createTime;

    private Date endDate;

    private String isShelve;

    private Date shelveDate;

    private Date shelveDateFirst;

    private String isShelveRemind;

    private Date updateTime;

    private String isDel;

    private Integer elecRewardId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getKeywordId() {
        return keywordId;
    }

    public void setKeywordId(String keywordId) {
        this.keywordId = keywordId == null ? null : keywordId.trim();
    }

    public Integer getIntegration() {
        return integration;
    }

    public void setIntegration(Integer integration) {
        this.integration = integration;
    }

    public String getTaskUrl() {
        return taskUrl;
    }

    public void setTaskUrl(String taskUrl) {
        this.taskUrl = taskUrl == null ? null : taskUrl.trim();
    }

    public Date getApplyEndDate() {
        return applyEndDate;
    }

    public void setApplyEndDate(Date applyEndDate) {
        this.applyEndDate = applyEndDate;
    }

    public Integer getApplyCount() {
        return applyCount;
    }

    public void setApplyCount(Integer applyCount) {
        this.applyCount = applyCount;
    }

    public Integer getApplyTotal() {
        return applyTotal;
    }

    public void setApplyTotal(Integer applyTotal) {
        this.applyTotal = applyTotal;
    }

    public String getEntityReward() {
        return entityReward;
    }

    public void setEntityReward(String entityReward) {
        this.entityReward = entityReward == null ? null : entityReward.trim();
    }

    public Integer getEntityTotal() {
        return entityTotal;
    }

    public void setEntityTotal(Integer entityTotal) {
        this.entityTotal = entityTotal;
    }

    public String getTaskBrief() {
        return taskBrief;
    }

    public void setTaskBrief(String taskBrief) {
        this.taskBrief = taskBrief == null ? null : taskBrief.trim();
    }

    public String getMoreDetail() {
        return moreDetail;
    }

    public void setMoreDetail(String moreDetail) {
        this.moreDetail = moreDetail == null ? null : moreDetail.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getIsShelve() {
        return isShelve;
    }

    public void setIsShelve(String isShelve) {
        this.isShelve = isShelve == null ? null : isShelve.trim();
    }

    public Date getShelveDate() {
        return shelveDate;
    }

    public void setShelveDate(Date shelveDate) {
        this.shelveDate = shelveDate;
    }

    public Date getShelveDateFirst() {
        return shelveDateFirst;
    }

    public void setShelveDateFirst(Date shelveDateFirst) {
        this.shelveDateFirst = shelveDateFirst;
    }

    public String getIsShelveRemind() {
        return isShelveRemind;
    }

    public void setIsShelveRemind(String isShelveRemind) {
        this.isShelveRemind = isShelveRemind == null ? null : isShelveRemind.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsDel() {
        return isDel;
    }

    public void setIsDel(String isDel) {
        this.isDel = isDel == null ? null : isDel.trim();
    }

    public Integer getElecRewardId() {
        return elecRewardId;
    }

    public void setElecRewardId(Integer elecRewardId) {
        this.elecRewardId = elecRewardId;
    }
}