package cc.gemii.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ImageQrcodeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ImageQrcodeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andFilenameIsNull() {
            addCriterion("filename is null");
            return (Criteria) this;
        }

        public Criteria andFilenameIsNotNull() {
            addCriterion("filename is not null");
            return (Criteria) this;
        }

        public Criteria andFilenameEqualTo(String value) {
            addCriterion("filename =", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameNotEqualTo(String value) {
            addCriterion("filename <>", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameGreaterThan(String value) {
            addCriterion("filename >", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameGreaterThanOrEqualTo(String value) {
            addCriterion("filename >=", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameLessThan(String value) {
            addCriterion("filename <", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameLessThanOrEqualTo(String value) {
            addCriterion("filename <=", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameLike(String value) {
            addCriterion("filename like", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameNotLike(String value) {
            addCriterion("filename not like", value, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameIn(List<String> values) {
            addCriterion("filename in", values, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameNotIn(List<String> values) {
            addCriterion("filename not in", values, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameBetween(String value1, String value2) {
            addCriterion("filename between", value1, value2, "filename");
            return (Criteria) this;
        }

        public Criteria andFilenameNotBetween(String value1, String value2) {
            addCriterion("filename not between", value1, value2, "filename");
            return (Criteria) this;
        }

        public Criteria andFilepathIsNull() {
            addCriterion("filepath is null");
            return (Criteria) this;
        }

        public Criteria andFilepathIsNotNull() {
            addCriterion("filepath is not null");
            return (Criteria) this;
        }

        public Criteria andFilepathEqualTo(String value) {
            addCriterion("filepath =", value, "filepath");
            return (Criteria) this;
        }

        public Criteria andFilepathNotEqualTo(String value) {
            addCriterion("filepath <>", value, "filepath");
            return (Criteria) this;
        }

        public Criteria andFilepathGreaterThan(String value) {
            addCriterion("filepath >", value, "filepath");
            return (Criteria) this;
        }

        public Criteria andFilepathGreaterThanOrEqualTo(String value) {
            addCriterion("filepath >=", value, "filepath");
            return (Criteria) this;
        }

        public Criteria andFilepathLessThan(String value) {
            addCriterion("filepath <", value, "filepath");
            return (Criteria) this;
        }

        public Criteria andFilepathLessThanOrEqualTo(String value) {
            addCriterion("filepath <=", value, "filepath");
            return (Criteria) this;
        }

        public Criteria andFilepathLike(String value) {
            addCriterion("filepath like", value, "filepath");
            return (Criteria) this;
        }

        public Criteria andFilepathNotLike(String value) {
            addCriterion("filepath not like", value, "filepath");
            return (Criteria) this;
        }

        public Criteria andFilepathIn(List<String> values) {
            addCriterion("filepath in", values, "filepath");
            return (Criteria) this;
        }

        public Criteria andFilepathNotIn(List<String> values) {
            addCriterion("filepath not in", values, "filepath");
            return (Criteria) this;
        }

        public Criteria andFilepathBetween(String value1, String value2) {
            addCriterion("filepath between", value1, value2, "filepath");
            return (Criteria) this;
        }

        public Criteria andFilepathNotBetween(String value1, String value2) {
            addCriterion("filepath not between", value1, value2, "filepath");
            return (Criteria) this;
        }

        public Criteria andExpiryDayIsNull() {
            addCriterion("expiry_day is null");
            return (Criteria) this;
        }

        public Criteria andExpiryDayIsNotNull() {
            addCriterion("expiry_day is not null");
            return (Criteria) this;
        }

        public Criteria andExpiryDayEqualTo(Byte value) {
            addCriterion("expiry_day =", value, "expiryDay");
            return (Criteria) this;
        }

        public Criteria andExpiryDayNotEqualTo(Byte value) {
            addCriterion("expiry_day <>", value, "expiryDay");
            return (Criteria) this;
        }

        public Criteria andExpiryDayGreaterThan(Byte value) {
            addCriterion("expiry_day >", value, "expiryDay");
            return (Criteria) this;
        }

        public Criteria andExpiryDayGreaterThanOrEqualTo(Byte value) {
            addCriterion("expiry_day >=", value, "expiryDay");
            return (Criteria) this;
        }

        public Criteria andExpiryDayLessThan(Byte value) {
            addCriterion("expiry_day <", value, "expiryDay");
            return (Criteria) this;
        }

        public Criteria andExpiryDayLessThanOrEqualTo(Byte value) {
            addCriterion("expiry_day <=", value, "expiryDay");
            return (Criteria) this;
        }

        public Criteria andExpiryDayIn(List<Byte> values) {
            addCriterion("expiry_day in", values, "expiryDay");
            return (Criteria) this;
        }

        public Criteria andExpiryDayNotIn(List<Byte> values) {
            addCriterion("expiry_day not in", values, "expiryDay");
            return (Criteria) this;
        }

        public Criteria andExpiryDayBetween(Byte value1, Byte value2) {
            addCriterion("expiry_day between", value1, value2, "expiryDay");
            return (Criteria) this;
        }

        public Criteria andExpiryDayNotBetween(Byte value1, Byte value2) {
            addCriterion("expiry_day not between", value1, value2, "expiryDay");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("description is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("description is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("description =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("description <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("description >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("description >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("description <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("description <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("description like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("description not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("description in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("description not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("description between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("description not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateUserIsNull() {
            addCriterion("create_user is null");
            return (Criteria) this;
        }

        public Criteria andCreateUserIsNotNull() {
            addCriterion("create_user is not null");
            return (Criteria) this;
        }

        public Criteria andCreateUserEqualTo(Integer value) {
            addCriterion("create_user =", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotEqualTo(Integer value) {
            addCriterion("create_user <>", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserGreaterThan(Integer value) {
            addCriterion("create_user >", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserGreaterThanOrEqualTo(Integer value) {
            addCriterion("create_user >=", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLessThan(Integer value) {
            addCriterion("create_user <", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLessThanOrEqualTo(Integer value) {
            addCriterion("create_user <=", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserIn(List<Integer> values) {
            addCriterion("create_user in", values, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotIn(List<Integer> values) {
            addCriterion("create_user not in", values, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserBetween(Integer value1, Integer value2) {
            addCriterion("create_user between", value1, value2, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotBetween(Integer value1, Integer value2) {
            addCriterion("create_user not between", value1, value2, "createUser");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateUserIsNull() {
            addCriterion("update_user is null");
            return (Criteria) this;
        }

        public Criteria andUpdateUserIsNotNull() {
            addCriterion("update_user is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateUserEqualTo(Integer value) {
            addCriterion("update_user =", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserNotEqualTo(Integer value) {
            addCriterion("update_user <>", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserGreaterThan(Integer value) {
            addCriterion("update_user >", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserGreaterThanOrEqualTo(Integer value) {
            addCriterion("update_user >=", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserLessThan(Integer value) {
            addCriterion("update_user <", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserLessThanOrEqualTo(Integer value) {
            addCriterion("update_user <=", value, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserIn(List<Integer> values) {
            addCriterion("update_user in", values, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserNotIn(List<Integer> values) {
            addCriterion("update_user not in", values, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserBetween(Integer value1, Integer value2) {
            addCriterion("update_user between", value1, value2, "updateUser");
            return (Criteria) this;
        }

        public Criteria andUpdateUserNotBetween(Integer value1, Integer value2) {
            addCriterion("update_user not between", value1, value2, "updateUser");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNull() {
            addCriterion("is_del is null");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNotNull() {
            addCriterion("is_del is not null");
            return (Criteria) this;
        }

        public Criteria andIsDelEqualTo(String value) {
            addCriterion("is_del =", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotEqualTo(String value) {
            addCriterion("is_del <>", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThan(String value) {
            addCriterion("is_del >", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThanOrEqualTo(String value) {
            addCriterion("is_del >=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThan(String value) {
            addCriterion("is_del <", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThanOrEqualTo(String value) {
            addCriterion("is_del <=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLike(String value) {
            addCriterion("is_del like", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotLike(String value) {
            addCriterion("is_del not like", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelIn(List<String> values) {
            addCriterion("is_del in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotIn(List<String> values) {
            addCriterion("is_del not in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelBetween(String value1, String value2) {
            addCriterion("is_del between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotBetween(String value1, String value2) {
            addCriterion("is_del not between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andMatterFwTypeIsNull() {
            addCriterion("matter_fw_type is null");
            return (Criteria) this;
        }

        public Criteria andMatterFwTypeIsNotNull() {
            addCriterion("matter_fw_type is not null");
            return (Criteria) this;
        }

        public Criteria andMatterFwTypeEqualTo(Byte value) {
            addCriterion("matter_fw_type =", value, "matterFwType");
            return (Criteria) this;
        }

        public Criteria andMatterFwTypeNotEqualTo(Byte value) {
            addCriterion("matter_fw_type <>", value, "matterFwType");
            return (Criteria) this;
        }

        public Criteria andMatterFwTypeGreaterThan(Byte value) {
            addCriterion("matter_fw_type >", value, "matterFwType");
            return (Criteria) this;
        }

        public Criteria andMatterFwTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("matter_fw_type >=", value, "matterFwType");
            return (Criteria) this;
        }

        public Criteria andMatterFwTypeLessThan(Byte value) {
            addCriterion("matter_fw_type <", value, "matterFwType");
            return (Criteria) this;
        }

        public Criteria andMatterFwTypeLessThanOrEqualTo(Byte value) {
            addCriterion("matter_fw_type <=", value, "matterFwType");
            return (Criteria) this;
        }

        public Criteria andMatterFwTypeIn(List<Byte> values) {
            addCriterion("matter_fw_type in", values, "matterFwType");
            return (Criteria) this;
        }

        public Criteria andMatterFwTypeNotIn(List<Byte> values) {
            addCriterion("matter_fw_type not in", values, "matterFwType");
            return (Criteria) this;
        }

        public Criteria andMatterFwTypeBetween(Byte value1, Byte value2) {
            addCriterion("matter_fw_type between", value1, value2, "matterFwType");
            return (Criteria) this;
        }

        public Criteria andMatterFwTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("matter_fw_type not between", value1, value2, "matterFwType");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentIsNull() {
            addCriterion("matter_fw_content is null");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentIsNotNull() {
            addCriterion("matter_fw_content is not null");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentEqualTo(String value) {
            addCriterion("matter_fw_content =", value, "matterFwContent");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentNotEqualTo(String value) {
            addCriterion("matter_fw_content <>", value, "matterFwContent");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentGreaterThan(String value) {
            addCriterion("matter_fw_content >", value, "matterFwContent");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentGreaterThanOrEqualTo(String value) {
            addCriterion("matter_fw_content >=", value, "matterFwContent");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentLessThan(String value) {
            addCriterion("matter_fw_content <", value, "matterFwContent");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentLessThanOrEqualTo(String value) {
            addCriterion("matter_fw_content <=", value, "matterFwContent");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentLike(String value) {
            addCriterion("matter_fw_content like", value, "matterFwContent");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentNotLike(String value) {
            addCriterion("matter_fw_content not like", value, "matterFwContent");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentIn(List<String> values) {
            addCriterion("matter_fw_content in", values, "matterFwContent");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentNotIn(List<String> values) {
            addCriterion("matter_fw_content not in", values, "matterFwContent");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentBetween(String value1, String value2) {
            addCriterion("matter_fw_content between", value1, value2, "matterFwContent");
            return (Criteria) this;
        }

        public Criteria andMatterFwContentNotBetween(String value1, String value2) {
            addCriterion("matter_fw_content not between", value1, value2, "matterFwContent");
            return (Criteria) this;
        }

        public Criteria andDictSceneIdIsNull() {
            addCriterion("dict_scene_id is null");
            return (Criteria) this;
        }

        public Criteria andDictSceneIdIsNotNull() {
            addCriterion("dict_scene_id is not null");
            return (Criteria) this;
        }

        public Criteria andDictSceneIdEqualTo(Integer value) {
            addCriterion("dict_scene_id =", value, "dictSceneId");
            return (Criteria) this;
        }

        public Criteria andDictSceneIdNotEqualTo(Integer value) {
            addCriterion("dict_scene_id <>", value, "dictSceneId");
            return (Criteria) this;
        }

        public Criteria andDictSceneIdGreaterThan(Integer value) {
            addCriterion("dict_scene_id >", value, "dictSceneId");
            return (Criteria) this;
        }

        public Criteria andDictSceneIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("dict_scene_id >=", value, "dictSceneId");
            return (Criteria) this;
        }

        public Criteria andDictSceneIdLessThan(Integer value) {
            addCriterion("dict_scene_id <", value, "dictSceneId");
            return (Criteria) this;
        }

        public Criteria andDictSceneIdLessThanOrEqualTo(Integer value) {
            addCriterion("dict_scene_id <=", value, "dictSceneId");
            return (Criteria) this;
        }

        public Criteria andDictSceneIdIn(List<Integer> values) {
            addCriterion("dict_scene_id in", values, "dictSceneId");
            return (Criteria) this;
        }

        public Criteria andDictSceneIdNotIn(List<Integer> values) {
            addCriterion("dict_scene_id not in", values, "dictSceneId");
            return (Criteria) this;
        }

        public Criteria andDictSceneIdBetween(Integer value1, Integer value2) {
            addCriterion("dict_scene_id between", value1, value2, "dictSceneId");
            return (Criteria) this;
        }

        public Criteria andDictSceneIdNotBetween(Integer value1, Integer value2) {
            addCriterion("dict_scene_id not between", value1, value2, "dictSceneId");
            return (Criteria) this;
        }

        public Criteria andDictSourceIdIsNull() {
            addCriterion("dict_source_id is null");
            return (Criteria) this;
        }

        public Criteria andDictSourceIdIsNotNull() {
            addCriterion("dict_source_id is not null");
            return (Criteria) this;
        }

        public Criteria andDictSourceIdEqualTo(Integer value) {
            addCriterion("dict_source_id =", value, "dictSourceId");
            return (Criteria) this;
        }

        public Criteria andDictSourceIdNotEqualTo(Integer value) {
            addCriterion("dict_source_id <>", value, "dictSourceId");
            return (Criteria) this;
        }

        public Criteria andDictSourceIdGreaterThan(Integer value) {
            addCriterion("dict_source_id >", value, "dictSourceId");
            return (Criteria) this;
        }

        public Criteria andDictSourceIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("dict_source_id >=", value, "dictSourceId");
            return (Criteria) this;
        }

        public Criteria andDictSourceIdLessThan(Integer value) {
            addCriterion("dict_source_id <", value, "dictSourceId");
            return (Criteria) this;
        }

        public Criteria andDictSourceIdLessThanOrEqualTo(Integer value) {
            addCriterion("dict_source_id <=", value, "dictSourceId");
            return (Criteria) this;
        }

        public Criteria andDictSourceIdIn(List<Integer> values) {
            addCriterion("dict_source_id in", values, "dictSourceId");
            return (Criteria) this;
        }

        public Criteria andDictSourceIdNotIn(List<Integer> values) {
            addCriterion("dict_source_id not in", values, "dictSourceId");
            return (Criteria) this;
        }

        public Criteria andDictSourceIdBetween(Integer value1, Integer value2) {
            addCriterion("dict_source_id between", value1, value2, "dictSourceId");
            return (Criteria) this;
        }

        public Criteria andDictSourceIdNotBetween(Integer value1, Integer value2) {
            addCriterion("dict_source_id not between", value1, value2, "dictSourceId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}