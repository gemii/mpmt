package cc.gemii.po;

import java.util.Date;

public class GoodsConvertRecord extends GoodsConvertRecordKey {
    private Date createTime;

    private String sojumpindex;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getSojumpindex() {
        return sojumpindex;
    }

    public void setSojumpindex(String sojumpindex) {
        this.sojumpindex = sojumpindex == null ? null : sojumpindex.trim();
    }
}