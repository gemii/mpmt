package cc.gemii.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import cc.gemii.po.SysUser;
import cc.gemii.service.PersonViewRecordService;
import cc.gemii.utils.UrlConsts;

/**
 * 
 * <p>Title: LoginInterceptor</p>
 * <p>Description:登陆认证拦截器 </p>
 * @date	20170223
 * @version 1.0
 */
public class LoginInterceptor implements HandlerInterceptor {
	
	private static Logger log = LoggerFactory.getLogger(LoginInterceptor.class);

	@Autowired
	private PersonViewRecordService personViewRecordService;
	
	//进入 Handler方法之前执行
	//用于身份认证、身份授权
	//比如身份认证，如果认证通过表示当前用户没有登陆，需要此方法拦截不再向下执行
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		response.setHeader("Access-Control-Allow-Origin", UrlConsts.ACCESS_ALLOW_ORIGIN);
		
		//获取请求的url
		String url = request.getRequestURI();
		//判断url是否是公开 地址（实际使用时将公开 地址配置配置文件中）
		//这里公开地址是登陆提交的地址
		
		//包括web，则表示不是后台
		if(url.indexOf("/web/") == -1){
			try {
				//处理个人中心页面的加载访问信息
				if(url.contains("personTasksInit") || url.contains("listInterationRecord") || url.contains("receiveTasksInit") || url.contains("getChestnutExplain")
						|| url.contains("putTasksInit") || url.contains("myIntegraTasksInit") || url.contains("listTaskMembersByMemberId") ){
					String membeId = request.getParameter("memberId");//用户id
					personViewRecordService.insertRecord(url, membeId);
				}
			} catch (Exception e) {
				log.error("添加访问日志出错，路径:{}", url);
				e.printStackTrace();
			}
			return true;
		}
		
		//判断session,加false不让新建
		HttpSession session  = request.getSession();
		//从session中取出用户身份信息
		SysUser user = (SysUser) session.getAttribute("user");
		if(user != null){
			//身份存在，放行
			return true;
		}
		
		//执行这里表示用户身份需要认证，跳转登陆页面
		response.getWriter().write("<script>alert('尚未登录')</script>");
		return false;
	}

	//进入Handler方法之后，返回modelAndView之前执行
	//应用场景从modelAndView出发：将公用的模型数据(比如菜单导航)在这里传到视图，也可以在这里统一指定视图
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}
	//执行Handler完成执行此方法
	//应用场景：统一异常处理，统一日志处理
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

}
