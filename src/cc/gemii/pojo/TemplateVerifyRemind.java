package cc.gemii.pojo;

/**
 * 审核通知
 * 
 * @author xudj20170301
 */
public class TemplateVerifyRemind extends TemplateGenericity<TemplateVerifyRemind> {

	public TemplateVerifyRemind() {
		this.templateId = "no2zhOKnZLSLgJDt9XQUmEm4-kESZWdsDbAcwvyxkwM";//正式
	}

	private String templateId;

	private String first;
	private String keyword1;
	private String keyword2;
	private String remark;

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getKeyword1() {
		return keyword1;
	}

	public void setKeyword1(String keyword1) {
		this.keyword1 = keyword1;
	}

	public String getKeyword2() {
		return keyword2;
	}

	public void setKeyword2(String keyword2) {
		this.keyword2 = keyword2;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
