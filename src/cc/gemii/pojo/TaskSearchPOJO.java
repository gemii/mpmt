package cc.gemii.pojo;

/**
 * 任务查询条件
 * 
 * @author lvshilin20170213
 */
public class TaskSearchPOJO extends PagingPOJO {

	private String title; // 根据标题查询

	private Integer typeId; // 根据任务类型查询

	private String shelveDateStr;// 任务上架开始时间

	private String endDateStr; // 任务下架结束时间

	private String isShelve; // 根据上下架或已删除查询

	private String isDel;// 根据是否删除查询

	public String getIsShelve() {
		return isShelve;
	}

	public void setIsShelve(String isShelve) {
		this.isShelve = isShelve;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getShelveDateStr() {
		return shelveDateStr;
	}

	public void setShelveDateStr(String shelveDateStr) {
		this.shelveDateStr = shelveDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

}
