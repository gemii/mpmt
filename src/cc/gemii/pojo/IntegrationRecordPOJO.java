package cc.gemii.pojo;

/**
 * 积分明细表VO
 * @author xudj20170210
 */
public class IntegrationRecordPOJO {
	
	private Long id;				//主键

    private String title;			//标题
    
    private String type;			//积分类型
    
    private String tasksType;		//任务类型

    private String createTimeStr;	//创建时间

    private String integration;		//积分变动

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTasksType() {
		return tasksType;
	}

	public void setTasksType(String tasksType) {
		this.tasksType = tasksType;
	}

	public String getCreateTimeStr() {
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}

	public String getIntegration() {
		return integration;
	}

	public void setIntegration(String integration) {
		this.integration = integration;
	}
}
