package cc.gemii.pojo;

/**
 * <p>ClassName: ElecRecordDTO</p>
 * <p>Description: 卡券记录DTO</p>
 * <p>Company: gemii</p>
 * @author xudj
 * @date 2017年4月28日 上午10:14:22
 */
public class ElecRecordDTO {
	
	private String openId;
	private String nickname;
	private String count;				// 浏览次数
	private String createTimeStr;		// 最新创建时间

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getCreateTimeStr() {
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}

}
