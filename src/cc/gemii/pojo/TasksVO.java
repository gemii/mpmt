package cc.gemii.pojo;

/**
 * 个人中心微任务首页展示
 * 
 * @author xudj20170215
 */
public class TasksVO {
	private Integer id; // 主键
	private String title; // 标题
	private String shelveDateStr; // 上架时间
	private String endDateStr; // 结束日期
	private String taskBrief; // 任务卡简介
	private Integer typeId; // 问卷类型
	private Integer integration; // 可获积分
	private String elecReward;//电子劵
	private String entityReward;//实体

	private String state; // 状态

	private String applyEndDateStr; // 申领结束日期
	private String TaskUrl; // 任务链接

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShelveDateStr() {
		return shelveDateStr;
	}

	public void setShelveDateStr(String shelveDateStr) {
		this.shelveDateStr = shelveDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public String getTaskBrief() {
		return taskBrief;
	}

	public void setTaskBrief(String taskBrief) {
		this.taskBrief = taskBrief;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Integer getIntegration() {
		return integration;
	}

	public void setIntegration(Integer integration) {
		this.integration = integration;
	}

	public String getElecReward() {
		return elecReward;
	}

	public void setElecReward(String elecReward) {
		this.elecReward = elecReward;
	}

	public String getEntityReward() {
		return entityReward;
	}

	public void setEntityReward(String entityReward) {
		this.entityReward = entityReward;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getApplyEndDateStr() {
		return applyEndDateStr;
	}

	public void setApplyEndDateStr(String applyEndDateStr) {
		this.applyEndDateStr = applyEndDateStr;
	}

	public String getTaskUrl() {
		return TaskUrl;
	}

	public void setTaskUrl(String taskUrl) {
		TaskUrl = taskUrl;
	}

}
