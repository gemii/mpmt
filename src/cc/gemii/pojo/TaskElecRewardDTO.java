package cc.gemii.pojo;

/**
 * <p>ClassName: TaskElecRewardDTO</p>
 * <p>Description: 卡券展示</p>
 * <p>Company: gemii</p>
 * @author xudj
 * @date 2017年4月24日 下午6:00:30
 */
public class TaskElecRewardDTO {

	private Integer id;
	
	private String elecName;		// 卡券名称
	
	private String taskName;		// 所属任务
	
	private String elecUrl;			// 卡券url
	
	private String dictBrandName;	// 品牌
	
	private Integer clickTotal;		// 点击数量
	
	private Integer clickPersonTotal;// 点击人数
	
	private String start2EndDateStr; // 持续时间
	
	private Integer taskId;			// 微任务id

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getElecName() {
		return elecName;
	}

	public void setElecName(String elecName) {
		this.elecName = elecName;
	}

	public String getElecUrl() {
		return elecUrl;
	}

	public void setElecUrl(String elecUrl) {
		this.elecUrl = elecUrl;
	}

	public String getDictBrandName() {
		return dictBrandName;
	}

	public void setDictBrandName(String dictBrandName) {
		this.dictBrandName = dictBrandName;
	}

	public Integer getClickTotal() {
		return clickTotal;
	}

	public void setClickTotal(Integer clickTotal) {
		this.clickTotal = clickTotal;
	}

	public Integer getClickPersonTotal() {
		return clickPersonTotal;
	}

	public void setClickPersonTotal(Integer clickPersonTotal) {
		this.clickPersonTotal = clickPersonTotal;
	}

	public String getStart2EndDateStr() {
		return start2EndDateStr;
	}

	public void setStart2EndDateStr(String start2EndDateStr) {
		this.start2EndDateStr = start2EndDateStr;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

}
