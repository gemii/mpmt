package cc.gemii.pojo;

/**
 * 任务处理通知模板
 * @author xudj20170301
 */
public class TemplateTaskHandleRemind extends TemplateGenericity<TemplateTaskHandleRemind>{
	
	public TemplateTaskHandleRemind(){
//		this.templateId = "L7lyX3ttOullqDJbb5oNiWWWzDRZoyIvEXCYLZLWJOk";//正式
		this.templateId = "ZFYeegq_liVdoFGXMST9bjwa7r0ay2rlrhKBUoerXC0"; //任务状态提醒模板
	}
	
	private String templateId;
	
	private String first;
	private String keyword1;
	private String keyword2;
	private String keyword3;
	private String keyword4;
	private String remark;
	
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	public String getFirst() {
		return first;
	}
	public void setFirst(String first) {
		this.first = first;
	}
	public String getKeyword1() {
		return keyword1;
	}
	public void setKeyword1(String keyword1) {
		this.keyword1 = keyword1;
	}
	public String getKeyword2() {
		return keyword2;
	}
	public void setKeyword2(String keyword2) {
		this.keyword2 = keyword2;
	}
	public String getKeyword3() {
		return keyword3;
	}
	public void setKeyword3(String keyword3) {
		this.keyword3 = keyword3;
	}
	public String getKeyword4() {
		return keyword4;
	}
	public void setKeyword4(String keyword4) {
		this.keyword4 = keyword4;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
