package cc.gemii.pojo;

/**
 * 业务数据传输DTO
 * @author xudj20170406
 */
public class ImageQrcodeDTO extends PagingPOJO{
	
	private Long id;
	private String description; 	// 说明
	private Integer dictSceneId;	// 投放场景
	private Integer expiryDay;		// 有效期
	private String createTimeStr; 	// 创建时间Str
	private String isDel;			//是否删除
	
	private String attTimeStartStr;	//关注开始时间
	private String attTimeEndStr;	//关注结束时间

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getDictSceneId() {
		return dictSceneId;
	}

	public void setDictSceneId(Integer dictSceneId) {
		this.dictSceneId = dictSceneId;
	}

	public Integer getExpiryDay() {
		return expiryDay;
	}

	public void setExpiryDay(Integer expiryDay) {
		this.expiryDay = expiryDay;
	}

	public String getCreateTimeStr() {
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}
	
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public String getAttTimeStartStr() {
		return attTimeStartStr;
	}

	public void setAttTimeStartStr(String attTimeStartStr) {
		this.attTimeStartStr = attTimeStartStr;
	}

	public String getAttTimeEndStr() {
		return attTimeEndStr;
	}

	public void setAttTimeEndStr(String attTimeEndStr) {
		this.attTimeEndStr = attTimeEndStr;
	}
	
}
