package cc.gemii.pojo;

/**
 * <p>ClassName: ScanDataVO</p>
 * <p>Description: 扫码数据展示</p>
 * <p>Company: gemii</p>
 * @author xudj
 * @date 2017年4月10日 上午10:00:49
 */
public class ScanDataVO {
	private String openid;		//用户openid
	private String nickname;			//昵称
	private Integer scanCount;			//扫码次数
	private String attTimeStr;		//关注时间

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Integer getScanCount() {
		return scanCount;
	}

	public void setScanCount(Integer scanCount) {
		this.scanCount = scanCount;
	}

	public String getAttTimeStr() {
		return attTimeStr;
	}

	public void setAttTimeStr(String attTimeStr) {
		this.attTimeStr = attTimeStr;
	}
}
