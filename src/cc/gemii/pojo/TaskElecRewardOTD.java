package cc.gemii.pojo;

/**
 * <p>ClassName: TaskElecRewardOTD</p>
 * <p>Description: 卡券管理查询实体</p>
 * <p>Company: gemii</p>
 * @author xudj
 * @date 2017年4月24日 下午5:54:06
 */
public class TaskElecRewardOTD extends PagingPOJO{

	private String elecName;		// 卡券名称
	private String taskName;		// 所属任务
	private Integer dictBrandId;	// 卡券品牌
	
	public String getElecName() {
		return elecName;
	}

	public void setElecName(String elecName) {
		this.elecName = elecName;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Integer getDictBrandId() {
		return dictBrandId;
	}

	public void setDictBrandId(Integer dictBrandId) {
		this.dictBrandId = dictBrandId;
	}

}
