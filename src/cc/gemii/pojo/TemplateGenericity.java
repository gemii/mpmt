package cc.gemii.pojo;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * 通过反射获取泛型实例
 * 发送模板消息专用
 * @author xudj
 */
public class TemplateGenericity<T> {
    /**
     * 当前操作的实体的类型信息
     */
    private Class<T> clazz;

	@SuppressWarnings("unchecked")
	public TemplateGenericity() {
        // 通过反射机制获取子类传递过来的实体类的类型信息
		Type genType = this.getClass().getGenericSuperclass();  
        this.clazz = (Class<T>)((ParameterizedType) genType).getActualTypeArguments()[0];
    }

	
	/**
	 * 获取指定实例的所有属性名及对应值的Map实例
	 * @return
	 */
	public Map<String, Object> getFieldValueMap() {
		return getFieldValueMap(null);
	}
	
    /**
     * 获取指定实例的所有属性名及对应值的Map实例
     * @param colors 颜色
     * @return 字段名及对应模板类值的Map实例
     */
    public Map<String, Object> getFieldValueMap(Map<String, String> colors) {
        // key是属性名，value是对应值
        Map<String, Object> fieldValueMap = new HashMap<String, Object>();

        // 获取当前加载的实体类中所有属性（字段）
        Field[] fields = this.clazz.getDeclaredFields();

        for (int i = 0; i < fields.length; i++) {
            Field f = fields[i];
            String key = f.getName();// 属性名 
            Object value = null;//属性值  
            if (!"templateId".equals(key)) {// 忽略序列化版本ID号
                f.setAccessible(true);//取消Java语言访问检查
                try {
                    value = f.get(this);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                TempMessage tempMessage = new TempMessage();
                //获取颜色,有则赋值，默认为黑色#000000
                if(colors != null){
                	tempMessage.setColor(colors.get(key) == null?"#000000":colors.get(key));
                }else{
                	tempMessage.setColor("#000000");
                }
                tempMessage.setValue((String)value);
                fieldValueMap.put(key, tempMessage);
            }
        }
        return fieldValueMap;
    }
}
