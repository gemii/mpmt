package cc.gemii.pojo;

/**
 * 任务即将到期通知
 * 
 * @author xudj20170301
 */
public class TemplateTaskExpireRemind extends TemplateGenericity<TemplateTaskExpireRemind> {

	public TemplateTaskExpireRemind() {
		templateId = "zlDUAAZCX7-X5z7drvJnhfoXSgCmxTbcq1OwbxiQLSQ";//正式
	}

	private String templateId;

	private String first;
	private String keyword1;
	private String keyword2;
	private String keyword3;
	private String keyword4;
	private String remark;

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getKeyword1() {
		return keyword1;
	}

	public void setKeyword1(String keyword1) {
		this.keyword1 = keyword1;
	}

	public String getKeyword2() {
		return keyword2;
	}

	public void setKeyword2(String keyword2) {
		this.keyword2 = keyword2;
	}

	public String getKeyword3() {
		return keyword3;
	}

	public void setKeyword3(String keyword3) {
		this.keyword3 = keyword3;
	}

	public String getKeyword4() {
		return keyword4;
	}

	public void setKeyword4(String keyword4) {
		this.keyword4 = keyword4;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
