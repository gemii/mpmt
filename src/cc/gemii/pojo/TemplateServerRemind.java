/**
 * Project Name:weixin
 * File Name:TemplateServerRemind.java
 * Package Name:cc.gemii.pojo
 * Date:2017年12月25日下午8:24:04
 * Copyright (c) 2017, chenxj All Rights Reserved.
 *
*/

package cc.gemii.pojo;
/**
 * ClassName:TemplateServerRemind <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2017年12月25日 下午8:24:04 <br/>
 * @author   xudj
 * @version  
 * @since    JDK 1.8
 * @see 	 
 */
public class TemplateServerRemind extends TemplateGenericity<TemplateServerRemind> {

	/**
	 * 服务状态提醒
	 */
	public TemplateServerRemind() {
		this.templateId = "EZJtDG7VKzQrlxAg6KVb5RFwCRaKPU2KJbnY-RVE3Io";
	}
	
	private String templateId;
	
	private String first;
	
	private String Good;
	
	private String contentType;
	
	private String remark;

	
	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getGood() {
		return Good;
	}

	public void setGood(String good) {
		Good = good;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}

