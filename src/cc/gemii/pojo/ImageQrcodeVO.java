package cc.gemii.pojo;

import java.util.Date;

/**
 * 二维码文件pojo
 * 
 * @author xudj20170327
 */
public class ImageQrcodeVO {

	private Long id; // id
	private Byte expiryDay; // 二维码有效期
	private String description; // 说明
	private Date createTime;	//创建时间
	private String createTimeStr; // 创建时间Str
	private String createUser; // 创建人
	private String updateTimeStr; // 创建时间Str
	private String updateUser; // 创建人
	private String isDel; // 是否删除
	private String matterFwType; // 类型
	private String matterFwId;//素材id
	private String matterFwContent; // 内容
	private String dictSceneValue; // 投放场景
	private String fileName;//文件名
    private String filePath;//文件路径
	private Integer dictSceneId;//场景id
    private Integer dictSourceId;//粉丝来源id
	
	private String expiryTimeStr;		//到期时间
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Byte getExpiryDay() {
		return expiryDay;
	}

	public void setExpiryDay(Byte expiryDay) {
		this.expiryDay = expiryDay;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreateTimeStr() {
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateTimeStr() {
		return updateTimeStr;
	}

	public void setUpdateTimeStr(String updateTimeStr) {
		this.updateTimeStr = updateTimeStr;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public String getMatterFwType() {
		return matterFwType;
	}

	public void setMatterFwType(String matterFwType) {
		this.matterFwType = matterFwType;
	}

	public String getMatterFwContent() {
		return matterFwContent;
	}

	public void setMatterFwContent(String matterFwContent) {
		this.matterFwContent = matterFwContent;
	}

	public String getDictSceneValue() {
		return dictSceneValue;
	}

	public void setDictSceneValue(String dictSceneValue) {
		this.dictSceneValue = dictSceneValue;
	}

	public String getExpiryTimeStr() {
		return expiryTimeStr;
	}

	public void setExpiryTimeStr(String expiryTimeStr) {
		this.expiryTimeStr = expiryTimeStr;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Integer getDictSceneId() {
		return dictSceneId;
	}

	public void setDictSceneId(Integer dictSceneId) {
		this.dictSceneId = dictSceneId;
	}

	public String getMatterFwId() {
		return matterFwId;
	}

	public void setMatterFwId(String matterFwId) {
		this.matterFwId = matterFwId;
	}

	public Integer getDictSourceId() {
		return dictSourceId;
	}

	public void setDictSourceId(Integer dictSourceId) {
		this.dictSourceId = dictSourceId;
	}
}
