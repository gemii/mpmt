package cc.gemii.mapper;

import cc.gemii.po.ElecRecord;
import cc.gemii.po.ElecRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ElecRecordMapper {
    int countByExample(ElecRecordExample example);

    int deleteByExample(ElecRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ElecRecord record);

    int insertSelective(ElecRecord record);

    List<ElecRecord> selectByExample(ElecRecordExample example);

    ElecRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ElecRecord record, @Param("example") ElecRecordExample example);

    int updateByExample(@Param("record") ElecRecord record, @Param("example") ElecRecordExample example);

    int updateByPrimaryKeySelective(ElecRecord record);

    int updateByPrimaryKey(ElecRecord record);
}