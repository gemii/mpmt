package cc.gemii.mapper;

import cc.gemii.po.PersonViewRecord;
import cc.gemii.po.PersonViewRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PersonViewRecordMapper {
    int countByExample(PersonViewRecordExample example);

    int deleteByExample(PersonViewRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PersonViewRecord record);

    int insertSelective(PersonViewRecord record);

    List<PersonViewRecord> selectByExample(PersonViewRecordExample example);

    PersonViewRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PersonViewRecord record, @Param("example") PersonViewRecordExample example);

    int updateByExample(@Param("record") PersonViewRecord record, @Param("example") PersonViewRecordExample example);

    int updateByPrimaryKeySelective(PersonViewRecord record);

    int updateByPrimaryKey(PersonViewRecord record);
}