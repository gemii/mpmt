package cc.gemii.mapper;

import cc.gemii.po.ImageQrcode;
import cc.gemii.po.ImageQrcodeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ImageQrcodeMapper {
    int countByExample(ImageQrcodeExample example);

    int deleteByExample(ImageQrcodeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ImageQrcode record);

    int insertSelective(ImageQrcode record);

    List<ImageQrcode> selectByExample(ImageQrcodeExample example);

    ImageQrcode selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ImageQrcode record, @Param("example") ImageQrcodeExample example);

    int updateByExample(@Param("record") ImageQrcode record, @Param("example") ImageQrcodeExample example);

    int updateByPrimaryKeySelective(ImageQrcode record);

    int updateByPrimaryKey(ImageQrcode record);
}