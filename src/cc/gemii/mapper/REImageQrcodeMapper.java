package cc.gemii.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cc.gemii.po.ImageQrcode;
import cc.gemii.pojo.ImageQrcodeDTO;
import cc.gemii.pojo.ImageQrcodeVO;
import cc.gemii.pojo.ScanDataVO;


/**
 * 重构二维码管理mapper
 * @author xudj20170328
 */
public interface REImageQrcodeMapper {

	ImageQrcodeVO selectDetailById(Integer id);

	List<ImageQrcodeVO> listImageQrcode(ImageQrcodeDTO imageQrcodeDTO);

	int listImageQrcodeCount(ImageQrcodeDTO imageQrcodeDTO);

	List<Map<String, Object>> exportFansAttData(HashMap<String, Object> cons);

	List<Map<String, Object>> exportFansScanData(HashMap<String, Object> cons);

	List<Map<String, Object>> exportFansNotAttData(HashMap<String, Object> cons);

	int listScanFansCount(ImageQrcodeDTO imageQrcodeDTO);

	List<ScanDataVO> listScanFans(ImageQrcodeDTO imageQrcodeDTO);

	int updateById(ImageQrcode imageQrcode);
	
}