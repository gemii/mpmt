package cc.gemii.mapper;

import cc.gemii.po.Tasks;
import cc.gemii.po.TasksExample;
import cc.gemii.po.TasksWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TasksMapper {
    int countByExample(TasksExample example);

    int deleteByExample(TasksExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TasksWithBLOBs record);

    int insertSelective(TasksWithBLOBs record);

    List<TasksWithBLOBs> selectByExampleWithBLOBs(TasksExample example);

    List<Tasks> selectByExample(TasksExample example);

    TasksWithBLOBs selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TasksWithBLOBs record, @Param("example") TasksExample example);

    int updateByExampleWithBLOBs(@Param("record") TasksWithBLOBs record, @Param("example") TasksExample example);

    int updateByExample(@Param("record") Tasks record, @Param("example") TasksExample example);

    int updateByPrimaryKeySelective(TasksWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(TasksWithBLOBs record);

    int updateByPrimaryKey(Tasks record);
}