package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.TasksWithBLOBs;
import cc.gemii.pojo.TaskSearchPOJO;
import cc.gemii.pojo.TasksVO;

/**
 * 重构微任务Mapper
 * @author lvshilin
 */
public interface RETasksMapper {
	
	int getTasksManageCount(TaskSearchPOJO taskSearchPOJO);
	
	List<Map<String, Object>> getTasksManageList(TaskSearchPOJO taskSearchPOJO);
	
	List<Integer> getShelveRemindTasksToday();

	String selectTasksIsShelveById(Integer taskId);

	List<TasksVO> selectPersonTasksInit(String memberId);

	List<Map<String, Object>> selectMyIntegraTasksInit(String memberId);

	int updateTaskToShelve();

	int updateTaskToUnShelve();

	int insertTasksGetId(TasksWithBLOBs tasks);

	Map<String, Object> selectTasksToReceive(@Param("taskId") Integer taskId, @Param("memberId") String memberId);

}
