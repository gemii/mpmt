package cc.gemii.mapper;

import cc.gemii.po.EmojiNew;
import cc.gemii.po.EmojiNewExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EmojiNewMapper {
    int countByExample(EmojiNewExample example);

    int deleteByExample(EmojiNewExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(EmojiNew record);

    int insertSelective(EmojiNew record);

    List<EmojiNew> selectByExampleWithBLOBs(EmojiNewExample example);

    List<EmojiNew> selectByExample(EmojiNewExample example);

    EmojiNew selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") EmojiNew record, @Param("example") EmojiNewExample example);

    int updateByExampleWithBLOBs(@Param("record") EmojiNew record, @Param("example") EmojiNewExample example);

    int updateByExample(@Param("record") EmojiNew record, @Param("example") EmojiNewExample example);

    int updateByPrimaryKeySelective(EmojiNew record);

    int updateByPrimaryKeyWithBLOBs(EmojiNew record);

    int updateByPrimaryKey(EmojiNew record);
}