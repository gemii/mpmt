package cc.gemii.mapper;

import cc.gemii.po.IntegrationRecord;
import cc.gemii.po.IntegrationRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface IntegrationRecordMapper {
    int countByExample(IntegrationRecordExample example);

    int deleteByExample(IntegrationRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(IntegrationRecord record);

    int insertSelective(IntegrationRecord record);

    List<IntegrationRecord> selectByExample(IntegrationRecordExample example);

    IntegrationRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") IntegrationRecord record, @Param("example") IntegrationRecordExample example);

    int updateByExample(@Param("record") IntegrationRecord record, @Param("example") IntegrationRecordExample example);

    int updateByPrimaryKeySelective(IntegrationRecord record);

    int updateByPrimaryKey(IntegrationRecord record);
}