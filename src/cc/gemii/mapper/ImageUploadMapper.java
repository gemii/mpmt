package cc.gemii.mapper;

import cc.gemii.po.ImageUpload;
import cc.gemii.po.ImageUploadExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ImageUploadMapper {
    int countByExample(ImageUploadExample example);

    int deleteByExample(ImageUploadExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ImageUpload record);

    int insertSelective(ImageUpload record);

    List<ImageUpload> selectByExample(ImageUploadExample example);

    ImageUpload selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ImageUpload record, @Param("example") ImageUploadExample example);

    int updateByExample(@Param("record") ImageUpload record, @Param("example") ImageUploadExample example);

    int updateByPrimaryKeySelective(ImageUpload record);

    int updateByPrimaryKey(ImageUpload record);
}