package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

/**
 * 重构领取任务记录表
 * @author xudj20170213
 */
public interface RETaskMemberRecordMapper {

	List<Map<String, Object>> getTasksRemindExpire();

	List<Map<String, Object>> selectTasksMembers(Integer taskId);
	
	Map<String, Object> selectTaskPutDetail(Long id);

	String selectOpenidById(Long id);

	int countTaskMembersByMemberId(Map<String, Object> cons);

	List<Map<String, Object>> listTaskMembersByMemberId(Map<String, Object> cons);

}