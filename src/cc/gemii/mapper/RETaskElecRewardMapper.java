package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import cc.gemii.po.TaskElecReward;
import cc.gemii.pojo.ElecRecordDTO;
import cc.gemii.pojo.TaskElecRewardDTO;
import cc.gemii.pojo.TaskElecRewardOTD;

/**
 * <p>ClassName: RETaskElecRewardMapper</p>
 * <p>Description: 重构卡券mapper</p>
 * <p>Company: gemii</p>
 * @author xudj
 * @date 2017年4月24日 下午6:14:41
 */
public interface RETaskElecRewardMapper {

	int listCount(TaskElecRewardOTD taskElecRewardOTD);

	List<TaskElecRewardDTO> list(TaskElecRewardOTD taskElecRewardOTD);

	Map<String, Long> selectClickTotal(Integer id);

	List<Map<String, Object>> exportElecRecord(Integer id);

	int selectElecRecordCount(Map<String, Object> cons);

	List<ElecRecordDTO> selectElecRecord(Map<String, Object> cons);

	List<Map<String, Object>> selectAllElecReward();

	TaskElecReward selectElecRewardByTaskId(Integer id);
	
}
