package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import cc.gemii.pojo.IntegrationRecordPOJO;

public interface REIntegrationRecordMapper {

	int countPagesByUnionId(Map<String, Object> cons);

	List<IntegrationRecordPOJO> listPagesByUnoinId(Map<String, Object> cons);

	List<String> selectIntegraLaskWeekByMemberId(String memberId);

	Map<String, Object> selectGoodsVerify(String openid);

}