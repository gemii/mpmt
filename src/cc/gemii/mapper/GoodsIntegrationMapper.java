package cc.gemii.mapper;

import cc.gemii.po.GoodsIntegration;
import cc.gemii.po.GoodsIntegrationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GoodsIntegrationMapper {
    int countByExample(GoodsIntegrationExample example);

    int deleteByExample(GoodsIntegrationExample example);

    int deleteByPrimaryKey(String goodId);

    int insert(GoodsIntegration record);

    int insertSelective(GoodsIntegration record);

    List<GoodsIntegration> selectByExample(GoodsIntegrationExample example);

    GoodsIntegration selectByPrimaryKey(String goodId);

    int updateByExampleSelective(@Param("record") GoodsIntegration record, @Param("example") GoodsIntegrationExample example);

    int updateByExample(@Param("record") GoodsIntegration record, @Param("example") GoodsIntegrationExample example);

    int updateByPrimaryKeySelective(GoodsIntegration record);

    int updateByPrimaryKey(GoodsIntegration record);
}