package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Dict;
import cc.gemii.po.EmojiNew;
import cc.gemii.po.GoodsIntegration;
import cc.gemii.po.MatterFw;
import cc.gemii.po.SysUser;

/**
 * 特殊的mapper接口
 * @author xudj20170213
 */
public interface OtherMapper {

	List<Map<String, String>> selectFansFw(List<Integer> ids);
	//查询所有的粉丝
	List<Map<String, String>> selectAllFans();


	Map<String, Object> selectMemberInfo(String memberId);

	void updateMemberIntegration(@Param("unionid") String memberId, @Param("integration") Integer integration);

	//根据字典id查询value值
	String selectDictValueById(Integer valueOf);
	
	//用户名密码查询用户
	SysUser selectByUnameAndPword(@Param("username") String username, @Param("password") String password);

	//查询什么是栗子界面数据
	List<Map<String, String>> getChestnutExplain(Map<String, String> map);

	List<EmojiNew> getUnicodeAll();

	int insertDictGetKey(Dict dict);

	List<MatterFw> selectMatterFwByType(String type);

	GoodsIntegration selectGoodsIntegration();

	int updateMemberSubIntegration(@Param("openId") String openId, @Param("integration") Integer integration);

	String selectUnoinIdByOpenId(String openId);
	
	List<Map<String, String>> selectHasTaskFansFw();
	
	List<String> selectHasCommitTaskFansFw();

}
