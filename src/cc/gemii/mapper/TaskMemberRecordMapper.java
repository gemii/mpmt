package cc.gemii.mapper;

import cc.gemii.po.TaskMemberRecord;
import cc.gemii.po.TaskMemberRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TaskMemberRecordMapper {
    int countByExample(TaskMemberRecordExample example);

    int deleteByExample(TaskMemberRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TaskMemberRecord record);

    int insertSelective(TaskMemberRecord record);

    List<TaskMemberRecord> selectByExampleWithBLOBs(TaskMemberRecordExample example);

    List<TaskMemberRecord> selectByExample(TaskMemberRecordExample example);

    TaskMemberRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TaskMemberRecord record, @Param("example") TaskMemberRecordExample example);

    int updateByExampleWithBLOBs(@Param("record") TaskMemberRecord record, @Param("example") TaskMemberRecordExample example);

    int updateByExample(@Param("record") TaskMemberRecord record, @Param("example") TaskMemberRecordExample example);

    int updateByPrimaryKeySelective(TaskMemberRecord record);

    int updateByPrimaryKeyWithBLOBs(TaskMemberRecord record);

    int updateByPrimaryKey(TaskMemberRecord record);
}