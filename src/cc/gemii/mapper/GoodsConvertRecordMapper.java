package cc.gemii.mapper;

import cc.gemii.po.GoodsConvertRecord;
import cc.gemii.po.GoodsConvertRecordExample;
import cc.gemii.po.GoodsConvertRecordKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GoodsConvertRecordMapper {
    int countByExample(GoodsConvertRecordExample example);

    int deleteByExample(GoodsConvertRecordExample example);

    int deleteByPrimaryKey(GoodsConvertRecordKey key);

    int insert(GoodsConvertRecord record);

    int insertSelective(GoodsConvertRecord record);

    List<GoodsConvertRecord> selectByExample(GoodsConvertRecordExample example);

    GoodsConvertRecord selectByPrimaryKey(GoodsConvertRecordKey key);

    int updateByExampleSelective(@Param("record") GoodsConvertRecord record, @Param("example") GoodsConvertRecordExample example);

    int updateByExample(@Param("record") GoodsConvertRecord record, @Param("example") GoodsConvertRecordExample example);

    int updateByPrimaryKeySelective(GoodsConvertRecord record);

    int updateByPrimaryKey(GoodsConvertRecord record);
}