package cc.gemii.mapper;

import cc.gemii.po.TaskElecReward;
import cc.gemii.po.TaskElecRewardExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TaskElecRewardMapper {
    int countByExample(TaskElecRewardExample example);

    int deleteByExample(TaskElecRewardExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TaskElecReward record);

    int insertSelective(TaskElecReward record);

    List<TaskElecReward> selectByExample(TaskElecRewardExample example);

    TaskElecReward selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TaskElecReward record, @Param("example") TaskElecRewardExample example);

    int updateByExample(@Param("record") TaskElecReward record, @Param("example") TaskElecRewardExample example);

    int updateByPrimaryKeySelective(TaskElecReward record);

    int updateByPrimaryKey(TaskElecReward record);
}