package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.ImageUpload;

/**
 * 重构图片上传mapper
 * @author xudj20170217
 */
public interface REImageUploadMapper {

	List<Map<String, Object>> selectTaskPutImagesByOutId(String outId);

	void insertImageGetId(ImageUpload imageUpload);

	void updateOutIdByIds(@Param("ids") List<Integer> ids, @Param("outId") String outId);
	
}