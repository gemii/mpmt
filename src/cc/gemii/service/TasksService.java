package cc.gemii.service;



import java.util.Map;

import cc.gemii.po.TasksWithBLOBs;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.TaskSearchPOJO;

import com.zyt.mybatis.plugin.Page;

public interface TasksService {

	CommonResult insertTasks(TasksWithBLOBs tasks,String shelveDateStr,String endDateStr, String applyEndDateStr, String[] keywords);

	Page<Map<String, Object>> getTasksManage(TaskSearchPOJO taskSearchPOJO);

	String selectTasksIsShelveById(Integer taskId);

	CommonResult selectTasksToReceive(Integer taskId, String memberId);

	CommonResult selectTasksToPut(Integer taskId);

	CommonResult selectPersonTasksInit(String memberId);

	CommonResult selectMyIntegraTasksInit(String memberId);

	TasksWithBLOBs queryTaskById(Integer id);

	int updateTask(TasksWithBLOBs tasks, String shelveDateStr, String endDateStr, String applyEndDateStr, String[] keywords);

	CommonResult updateIsShelve(Integer id,String isShelve);

	int deleteTask(Integer id, String isDel);

}
