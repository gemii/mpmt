package cc.gemii.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zyt.mybatis.plugin.Page;

import cc.gemii.po.Dict;
import cc.gemii.po.ImageQrcode;
import cc.gemii.po.SysUser;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.ImageQrcodeDTO;
import cc.gemii.pojo.ScanDataVO;

/**
 * 二维码图片处理业务
 * @author xudj20170327
 */
public interface ImageQrcodeService {

	CommonResult insertImageQrcode(ImageQrcode imageQrcode, Dict dict, SysUser user);

	CommonResult selectDetailById(Integer id);

	CommonResult updateImageQrcode(ImageQrcode imageQrcode);

	byte[] downloadImageQrcode(String fileName);

	Object listImageQrcode(ImageQrcodeDTO imageQrcodeDTO);

	List<Map<String, Object>> exportFansAttData(HashMap<String, Object> cons);

	List<Map<String, Object>> exportFansScanData(HashMap<String, Object> cons);

	ImageQrcode selectById(Long id);

	List<Map<String, Object>> exportFansNotAttData(HashMap<String, Object> cons);

	Page<ScanDataVO> listScanFans(ImageQrcodeDTO imageQrcodeDTO);

	CommonResult delImageQrcode(ImageQrcode imageQrcode);
	
}
