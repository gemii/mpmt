package cc.gemii.service;

import com.zyt.mybatis.plugin.Page;

import cc.gemii.po.Tasks;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.IntegrationRecordPOJO;
import cc.gemii.pojo.PagingPOJO;

public interface IntegrationRecordService {

	Page<IntegrationRecordPOJO> listByUnoinId(PagingPOJO pagingPojo, String unionid);

	boolean insertIntegrationRecord(String memberId, Tasks tasks);

	CommonResult selectGoodsVerify(String openid);

	String handleGoodsConvert(String openId, String sojumpindex);

}
