package cc.gemii.service;

import javax.servlet.http.HttpServletResponse;

import cc.gemii.po.TaskElecReward;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.PagingPOJO;
import cc.gemii.pojo.TaskElecRewardOTD;

/**
 * <p>ClassName: TaskElecRewardService</p>
 * <p>Description: 微任务卡券业务接口</p>
 * <p>Company: gemii</p>
 * @author xudj
 * @date 2017年4月24日 下午3:36:01
 */
public interface TaskElecRewardService {

	CommonResult insert(TaskElecReward taskElecReward, Integer id);

	CommonResult list(TaskElecRewardOTD taskElecRewardOTD);

	CommonResult selectDescrition(Integer id);

	CommonResult update(TaskElecReward taskElecReward);

	void exportElecRecord(HttpServletResponse response, int[] ids) throws Exception;

	CommonResult selectElecRecord(PagingPOJO pagingPOJO, Integer taskId);

	void insertElecRecord(Integer id, String openid);

	CommonResult selectAllElecReward();

	TaskElecReward selectElecRewardByTaskId(Integer id);

}
