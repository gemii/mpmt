package cc.gemii.service;

import cc.gemii.pojo.CommonResult;

public interface ImageUploadService {

	CommonResult insertImage(String data, String dir);

	String updateImagesOutId(String[] data);
	
}
