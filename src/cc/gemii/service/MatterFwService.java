package cc.gemii.service;

import cc.gemii.pojo.CommonResult;

/**
 * 素材服务业务层接口
 * @author xudj20170407
 */
public interface MatterFwService {

	CommonResult selectMatterFwByType(String type);
	
}
