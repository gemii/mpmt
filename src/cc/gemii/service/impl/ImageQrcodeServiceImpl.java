package cc.gemii.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.ImageQrcodeMapper;
import cc.gemii.mapper.REImageQrcodeMapper;
import cc.gemii.po.Dict;
import cc.gemii.po.ImageQrcode;
import cc.gemii.po.SysUser;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.ImageQrcodeDTO;
import cc.gemii.pojo.ImageQrcodeVO;
import cc.gemii.pojo.ScanDataVO;
import cc.gemii.service.ImageQrcodeService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.HttpClientUtil;
import cc.gemii.utils.StrConsts;
import cc.gemii.utils.UrlConsts;

import com.alibaba.fastjson.JSON;
import com.zyt.mybatis.plugin.Page;

/**
 * 二维码图片处理业务
 * @author xudj20170327
 */
@Service
public class ImageQrcodeServiceImpl implements ImageQrcodeService{
	
	private static final Logger log = LoggerFactory.getLogger(ImageQrcodeServiceImpl.class);
	
	@Autowired
	private ImageQrcodeMapper imageQrcodeMapper;

	@Value("${QRCODE_SAVE_PATH}")
	private String QRCODE_SAVE_PATH;
	
	@Autowired
	private REImageQrcodeMapper reImageQrcodeMapper;
	
	/**
	 * 新增二维码，并添加二维码记录
	 * @param imageQrcode：图片二维码
	 */
	@Override
	public CommonResult insertImageQrcode(ImageQrcode imageQrcode, Dict dict, SysUser user) {
		String url = "";//url
		String filePath = QRCODE_SAVE_PATH;
		if(imageQrcode.getExpiryDay() == 0){//永久
			url = UrlConsts.CREATE_FOREVER_QRCODE_AND_GETNAME.replace("SCENESTR", dict.getValue())
						.replace("PATH", filePath);
		}else{//临时二维码
			String filePathSuffix = CommonUtil.dateFormat(new Date(), CommonUtil.dateFormat_yyMM);
			filePath = filePath + filePathSuffix + "/";
			url = UrlConsts.CREATE_QRCODE_AND_GETNAME.replace("SCENEID", dict.getValue())
					.replace("PATH", filePath).replace("DAY", imageQrcode.getExpiryDay()+"");
		}
		//请求生成二维码
		String resJson = HttpClientUtil.doGet(url);
		@SuppressWarnings("unchecked")
		Map<String, Object> result = (Map<String, Object>) JSON.parse(resJson);
		if(result == null || (Integer)result.get("status") != 200){
			return CommonResult.build(400, "二维码生成异常");
		}
		//插入二维码记录表
		imageQrcode.setFilename((String)result.get("data"));
		imageQrcode.setFilepath(filePath);
		imageQrcode.setCreateUser(user.getId());
		imageQrcode.setUpdateUser(user.getId());
		imageQrcode.setIsDel("F");
		imageQrcode.setDictSourceId(dict.getId());//用户来源dict
		int count = imageQrcodeMapper.insertSelective(imageQrcode);
		if(count == 0){
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		return CommonResult.ok();
	}

	/**
	 * 根据id查询详细
	 * @param id：主键
	 * @author xudj20170328
	 */
	@Override
	public CommonResult selectDetailById(Integer id) {
		ImageQrcodeVO imageQrcodeVO = reImageQrcodeMapper.selectDetailById(id);
		if(imageQrcodeVO == null){
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		//计算到期时间
		int expiryDay = imageQrcodeVO.getExpiryDay();
		if(expiryDay != 0){//临时
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(imageQrcodeVO.getCreateTime());
			//加天数
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			calendar.set(Calendar.DAY_OF_MONTH, day + expiryDay);
			Date expiryTime = calendar.getTime();
			//到期时间
			imageQrcodeVO.setExpiryTimeStr(CommonUtil.dateFormat(expiryTime, CommonUtil.dateFormat_yyyy_MM_ddHHmmss));
		}
		return CommonResult.ok(imageQrcodeVO);
	}

	/**
	 * 更新二维码信息
	 * @param imageQrcode:二维码信息
	 * @author xudj20170306
	 */
	@Override
	public CommonResult updateImageQrcode(ImageQrcode imageQrcode) {
		String matterFwContent = imageQrcode.getMatterFwContent();
		if(!CommonUtil.hasText(matterFwContent)){
			imageQrcode.setMatterFwContent(null);
		}
		int count = reImageQrcodeMapper.updateById(imageQrcode);
		if(count == 0){
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		return CommonResult.ok();
	}

	/**
	 * 加载服务器虚拟目录二维码图片
	 * @param filename:x虚拟路径加文件名
	 * @author xudj20170406
	 */ 
	@Override
	public byte[] downloadImageQrcode(String fileName) {
		byte[] getData = null;
		String urlStr = UrlConsts.GET_QRCODE_URL.replace("FILENAME", fileName);
		try {
			URL url = new URL(urlStr);    
	        HttpURLConnection conn = (HttpURLConnection)url.openConnection();    
	                //设置超时间为3秒  
	        conn.setConnectTimeout(3*1000);  
	        //防止屏蔽程序抓取而返回403错误  
	        conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");  
	        //得到输入流  
	        InputStream inputStream = conn.getInputStream();    
	        //获取自己数组  
	        getData = readInputStream(inputStream);      
		} catch (Exception e) {
			log.error("加载服务器虚拟目录二维码图片异常.");
			e.printStackTrace();
		}
	    return getData;
	}
	/** 
     * 从输入流中获取字节数组 
     * @param inputStream 
     * @return 
     * @throws IOException 
     */  
    private static  byte[] readInputStream(InputStream inputStream) throws IOException {    
        byte[] buffer = new byte[1024];    
        int len = 0;    
        ByteArrayOutputStream bos = new ByteArrayOutputStream();    
        while((len = inputStream.read(buffer)) != -1) {    
            bos.write(buffer, 0, len);    
        }    
        bos.close();    
        return bos.toByteArray();    
    } 

	/**
	 * 分页查询二维码列表
	 * @param imageQrcode：查询条件
	 * @param pagingPOJO：分页
	 */
	@Override
	public Page<ImageQrcodeVO> listImageQrcode(ImageQrcodeDTO imageQrcodeDTO) {
		Page<ImageQrcodeVO> pages = new Page<ImageQrcodeVO>(imageQrcodeDTO.getPageNo(), imageQrcodeDTO.getPageSize());
		int total = reImageQrcodeMapper.listImageQrcodeCount(imageQrcodeDTO);
		pages.setTotal(total);
		if(total == 0){
			return pages;
		}
		//查询列表
		List<ImageQrcodeVO> list = reImageQrcodeMapper.listImageQrcode(imageQrcodeDTO);
		pages.setRows(list);
		return pages;
	}

	/**
	 * 查询二维码关注列表
	 * @param cons：查询参数
	 * @author xudj20170406
	 */
	@Override
	public List<Map<String, Object>> exportFansAttData(HashMap<String, Object> cons) {
		return reImageQrcodeMapper.exportFansAttData(cons);
	}
	
	/**
	 * 查询二维码扫码列表
	 * @param cons：查询参数
	 * @author xudj20170406
	 */
	@Override
	public List<Map<String, Object>> exportFansScanData(HashMap<String, Object> cons) {
		return reImageQrcodeMapper.exportFansScanData(cons);
	}

	/**
	 * 查询二维码取关扫码列表
	 * @param cons：查询参数
	 * @author xudj20170407
	 */
	@Override
	public List<Map<String, Object>> exportFansNotAttData(HashMap<String, Object> cons) {
		return reImageQrcodeMapper.exportFansNotAttData(cons);
	}

	/**
	 * 根据id查询
	 */
	@Override
	public ImageQrcode selectById(Long id) {
		return imageQrcodeMapper.selectByPrimaryKey(id);
	}

	/**
	 * <p>Description: 查询扫码数据</p>
	 * @author xudj
	 * @date 2017年4月10日 上午9:54:18
	 * @param id
	 * @return
	 */
	@Override
	public Page<ScanDataVO> listScanFans(ImageQrcodeDTO imageQrcodeDTO) {
		Page<ScanDataVO> page = new Page<ScanDataVO>(imageQrcodeDTO.getPageNo(), imageQrcodeDTO.getPageSize());
		int total = reImageQrcodeMapper.listScanFansCount(imageQrcodeDTO);
		page.setTotal(total);
		//非零校验
		if(total == 0){
			return page;
		}
		List<ScanDataVO> rows = reImageQrcodeMapper.listScanFans(imageQrcodeDTO);
		page.setRows(rows);
		return page;
	}

	/**
	 * <p>Description: 删除(is_del = T)</p>
	 * @author xudj
	 * @date 2017年4月10日 下午2:27:04
	 * @param imageQrcode
	 * @return
	 */
	@Override
	public CommonResult delImageQrcode(ImageQrcode imageQrcode) {
		int count = imageQrcodeMapper.updateByPrimaryKeySelective(imageQrcode);
		if(count == 0){
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		return CommonResult.ok();
	}
	
}
