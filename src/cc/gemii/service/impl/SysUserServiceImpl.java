package cc.gemii.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.OtherMapper;
import cc.gemii.po.SysUser;
import cc.gemii.service.SysUserService;
import cc.gemii.utils.MD5Utils;

/**
 * 登录
 * @author xudj20170222
 */
@Service
public class SysUserServiceImpl implements SysUserService{

	@Autowired
	private OtherMapper otherMapper;
	
	@Override
	public SysUser selectForLogin(String username, String password) {
		//登录判断
		String passMd5 = MD5Utils.md5Encode(password);
		return otherMapper.selectByUnameAndPword(username, passMd5);
	}
}
