package cc.gemii.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.OtherMapper;
import cc.gemii.po.MatterFw;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.MatterFwService;

/**
 * 素材服务业务层
 * @author xudj20170407
 */
@Service
public class MatterFwServiceImpl implements MatterFwService{
	
	@Autowired
	private OtherMapper otherMapper;

	/**
	 * 根据类型查询素材管理
	 * @param type：类型
	 * @author xudj20170407
	 */
	@Override
	public CommonResult selectMatterFwByType(String type) {
		List<MatterFw> list = otherMapper.selectMatterFwByType(type);
		return CommonResult.ok(list);
	}
	
}
