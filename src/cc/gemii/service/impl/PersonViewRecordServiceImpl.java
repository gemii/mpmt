package cc.gemii.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.PersonViewRecordMapper;
import cc.gemii.po.PersonViewRecord;
import cc.gemii.service.PersonViewRecordService;

/**
 * 个人中心浏览记录业务层
 * @author xudj
 */
@Service
public class PersonViewRecordServiceImpl implements PersonViewRecordService{

	@Autowired
	private PersonViewRecordMapper personViewRecordMapper;
	
	/**
	 * 添加个人中心访问记录
	 */
	@Override
	public void insertRecord(String urlView, String membeId) {
		PersonViewRecord record = new PersonViewRecord();
		record.setUrl(urlView);
		record.setMemberId(membeId);
		record.setCreateTime(new Date());
		personViewRecordMapper.insert(record);	
	}

}
