package cc.gemii.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.EmojiNewMapper;
import cc.gemii.mapper.OtherMapper;
import cc.gemii.po.EmojiNew;
import cc.gemii.po.EmojiNewExample;
import cc.gemii.service.CommonService;
import cc.gemii.utils.ExceptEmojiUtil;

/**
 * 一般业务层
 * @author xudj20170309
 */
@Service
public class CommonServiceImpl implements CommonService{

	@Autowired
	private OtherMapper otherMapper;
	
	@Autowired
	private EmojiNewMapper emojiNewMapper;
	/**
	 * 处理表情
	 * @param nickname
	 * @return
	 */
	@Override
	public String handleEmoji(String nickname) {
		if(nickname == null || nickname.equals("")){//为null或者为空串，则返回""
			return "";
		}
		StringBuffer res = new StringBuffer();
		for (int i = 0; i < nickname.length(); i++) {
			char c = nickname.charAt(i);
			String hex = Integer.toHexString(c);
			if(hex.equals("20e3")){
				continue ;
			}
			if(ExceptEmojiUtil.getList().contains(hex)){
				if(!(i == (nickname.length() - 1))){
					res.append(getCode(c,nickname.charAt(i + 1)));
				}else{
					res.append(getCode(c));
				}
			}else{
				res.append(getCode(c));
			}
		}
		String prev_nickname = res.toString();
		for (EmojiNew emoji : getUnicodeAll()) {
			if(prev_nickname.contains(emoji.getUtf16())){
				prev_nickname = prev_nickname.replace(emoji.getUtf16(), emoji.getMapcode());
			}
		}
		return prev_nickname;
	}
	private String getCode(char c1,char c2){
		String hex2 = Integer.toHexString(c2);
		if(hex2.equals("20e3")){
			return "\\u00" + Integer.toHexString(c1) + "\\u" + hex2;
		}else{
			return getCode(c1);
		}
	}
	private String getCode(char c){
		boolean flag = ExceptEmojiUtil.isEmojiCharacter(c);
		String code = "";
		if(!flag){   //不是emoji
			//是否是softbank编码
			String hex = Integer.toHexString(c).toUpperCase();
			if(hex.startsWith("E")){
				String unicode = this.getUnicode(hex);
				if(unicode == null){
					code = String.valueOf(c);
				}else{
					code = unicode;
				}
			}else{
				code = String.valueOf(c);
			}
		}else{     //emoji
			code = "\\u" + Integer.toHexString(c);
		}
		return code;
	}
	
	private List<EmojiNew> getUnicodeAll(){
		return otherMapper.getUnicodeAll();
	}
	
	public String getUnicode(String code) {
		EmojiNewExample example = new EmojiNewExample();
		example.createCriteria()
			.andSbunicodeEqualTo(code);
		List<EmojiNew> codes = emojiNewMapper.selectByExampleWithBLOBs(example);
		if(codes.size() == 0){
			return null;
		}
		return codes.get(0).getMapcode();
	}
}
