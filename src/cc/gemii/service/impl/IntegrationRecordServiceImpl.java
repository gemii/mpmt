package cc.gemii.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.GoodsConvertRecordMapper;
import cc.gemii.mapper.GoodsIntegrationMapper;
import cc.gemii.mapper.IntegrationRecordMapper;
import cc.gemii.mapper.OtherMapper;
import cc.gemii.mapper.REIntegrationRecordMapper;
import cc.gemii.mapper.TasksMapper;
import cc.gemii.po.GoodsConvertRecord;
import cc.gemii.po.GoodsConvertRecordExample;
import cc.gemii.po.GoodsIntegration;
import cc.gemii.po.IntegrationRecord;
import cc.gemii.po.IntegrationRecordExample;
import cc.gemii.po.Tasks;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.IntegrationRecordPOJO;
import cc.gemii.pojo.PagingPOJO;
import cc.gemii.service.IntegrationRecordService;
import cc.gemii.utils.StrConsts;
import cc.gemii.utils.UrlConsts;

import com.zyt.mybatis.plugin.Page;

@Service
public class IntegrationRecordServiceImpl implements IntegrationRecordService{
	private static final Logger log = LoggerFactory.getLogger(IntegrationRecordServiceImpl.class);
	
	@Autowired
	private IntegrationRecordMapper integrationRecordMapper;
	
	@Autowired
	private REIntegrationRecordMapper reIntegrationRecordMapper;
	
	@Autowired
	private TasksMapper tasksMapper;
	
	@Autowired
	private OtherMapper otherMapper;
	
	@Autowired
	private GoodsIntegrationMapper goodsIntegrationMapper;
	
	@Autowired
	private GoodsConvertRecordMapper goodsConvertRecordMapper;
	
	/**
	 * 分页查询积分明细
	 * @author xudj20170210
	 */
	@Override
	public Page<IntegrationRecordPOJO> listByUnoinId(PagingPOJO pagingPojo, String unionId) {
		Page<IntegrationRecordPOJO> pages = new Page<IntegrationRecordPOJO>(pagingPojo.getPageNo(), pagingPojo.getPageSize());
		//查询条件
		Map<String, Object> cons = new HashMap<String, Object>();
		cons.put("pageStart", pagingPojo.getPageStart());
		cons.put("pageSize", pagingPojo.getPageSize());
		cons.put("unionId", unionId);
		//查询条数
		int count = this.reIntegrationRecordMapper.countPagesByUnionId(cons);
		pages.setTotal(count);
		if(count == 0){//无更多数据
			return pages;
		}
		//总记录数
		pages.setRows(this.reIntegrationRecordMapper.listPagesByUnoinId(cons));
		return pages;
	}
	
	/**
	 * 插入完成任务获取的积分
	 * @author xudj20170222
	 * @param memberId：用户id
	 * @param tasks：任务
	 */
	@Override
	public boolean insertIntegrationRecord(String memberId, Tasks tasks) {
		//判断是否已经存存过
		IntegrationRecordExample example = new IntegrationRecordExample();
		example.createCriteria().andMemberIdEqualTo(memberId).andTasksIdEqualTo(tasks.getId());
		List<IntegrationRecord> integrationRecords = integrationRecordMapper.selectByExample(example);
		if(integrationRecords != null && integrationRecords.size() > 0){
			return false;
		}
		//存记录
		IntegrationRecord integrationRecord = new IntegrationRecord();
		integrationRecord.setTitle(tasks.getTitle());
		integrationRecord.setType("2");//任务获取
		integrationRecord.setCreateTime(new Date());
		integrationRecord.setIntegration("+" + tasks.getIntegration());//积分
		integrationRecord.setTasksId(tasks.getId());
		integrationRecord.setMemberId(memberId);
		integrationRecord.setIsVaild("T");//有效
		integrationRecordMapper.insert(integrationRecord);
		return true;
	}

	/**
	 * <p>Description: 查询商品兑换初始化数据</p>
	 * @author xudj
	 * @date 2017年4月8日 下午6:09:59
	 * @return
	 */
	@Override
	public CommonResult selectGoodsVerify(String openId) {
		//返回数据
		Map<String, Object> res = new HashMap<String, Object>();
		//查询粉丝及积分
		Map<String, Object> fansIntegrationMap = reIntegrationRecordMapper.selectGoodsVerify(openId);
		//不是粉丝
		if(fansIntegrationMap == null){
			res.put("process", "0");
			return CommonResult.ok(res);
		}
		//查询最新商品积分数据
		GoodsIntegration goodsIntegration = otherMapper.selectGoodsIntegration();
		if(goodsIntegration == null){//没有可兑换商品
			res.put("process", "3");
			return CommonResult.ok(res);
		}
		//是否已领取
		GoodsConvertRecord goodsConvertSearch = new GoodsConvertRecord();
		goodsConvertSearch.setGoodId(goodsIntegration.getGoodId());
		goodsConvertSearch.setOpenid(openId);
		GoodsConvertRecord goodsConvertRecord = goodsConvertRecordMapper.selectByPrimaryKey(goodsConvertSearch);
		if(goodsConvertRecord != null){
			res.put("process", "4");
			return CommonResult.ok(res);
		}
		//商品数据是否足够
		GoodsConvertRecordExample example = new GoodsConvertRecordExample();
		example.createCriteria().andGoodIdEqualTo(goodsIntegration.getGoodId());
		List<GoodsConvertRecord> list = goodsConvertRecordMapper.selectByExample(example);
		if(list != null && list.size() >= goodsIntegration.getTotal()){
			res.put("process", "3");
			return CommonResult.ok(res);
		}
		
		//积分是否足够
		Integer inetgration = (Integer)fansIntegrationMap.get("integration");
		//积分够
		if(inetgration != null && inetgration >= goodsIntegration.getIntegration()){
			res.put("process", "1");
			res.put("goodId", goodsIntegration.getGoodId());
		}else{//不够
			res.put("process", "2");
		}
		return CommonResult.ok(res);
	}

	/**
	 * <p>Description: 处理商品兑换</p>
	 * @author xudj
	 * @date 2017年4月11日 下午12:03:45
	 * @param userid
	 */
	@Override
	public String handleGoodsConvert(String userid, String sojumpindex) {
		String openId = userid.substring(0, userid.lastIndexOf("_"));
		String goodId = userid.substring(userid.lastIndexOf("_")+1, userid.length());
		//根据商品id查询商品积分
		GoodsIntegration goodsIntegration = goodsIntegrationMapper.selectByPrimaryKey(goodId);
		if(goodsIntegration == null){
			log.warn("扣除积分时，商品查询失败:openId_goodId{}",userid);
			return null;
		}
		//重复提交
		GoodsConvertRecordExample example = new GoodsConvertRecordExample();
		example.createCriteria().andGoodIdEqualTo(goodId)
			.andOpenidEqualTo(openId).andSojumpindexEqualTo(sojumpindex);
		List<GoodsConvertRecord> list = goodsConvertRecordMapper.selectByExample(example);
		if(list != null && list.size() > 0){
			return null;
		}
		//验证商品数量是否足够
		String resStr = this.verifyGoodConvertTotal(openId, goodId, goodsIntegration, sojumpindex);
		if(resStr != null){
			return resStr;
		}
		//减去个人中心积分
		int count = otherMapper.updateMemberSubIntegration(openId, goodsIntegration.getIntegration());
		if(count == 0){
			log.warn("扣除积分失败:openId_goodId{}",userid);
		}
		//根据openid查询unionid
		String unionid = otherMapper.selectUnoinIdByOpenId(openId);
		//添加积分消费记录
		IntegrationRecord integrationRecord = new IntegrationRecord();
		integrationRecord.setTitle(StrConsts.INTEGRATION_RECORD_TITLE_CONVERT);//标题
		integrationRecord.setType("3");
		integrationRecord.setCreateTime(new Date());
		integrationRecord.setIntegration("-"+goodsIntegration.getIntegration());//积分
		integrationRecord.setIsVaild("T");
		integrationRecord.setMemberId(unionid);
		integrationRecordMapper.insert(integrationRecord);
		return null;
	}

	/**
	 * <p>Description: 验证商品数量是否兑换完</p>
	 * @author xudj
	 * @date 2017年4月11日 下午7:59:56
	 * @param openId：openid
	 * @param goodId：商品id
	 * @param goodsIntegration
	 * @param sojumpindex 
	 * @return
	 */
	// TODO 重复兑换
	private String verifyGoodConvertTotal(String openId, String goodId, GoodsIntegration goodsIntegration, String sojumpindex) {
			//商品数据是否足够
			GoodsConvertRecordExample example = new GoodsConvertRecordExample();
			example.createCriteria().andGoodIdEqualTo(goodsIntegration.getGoodId());
			synchronized (this) {
				List<GoodsConvertRecord> list = goodsConvertRecordMapper.selectByExample(example);
				if(list != null && list.size() >= goodsIntegration.getTotal()){
					return UrlConsts.GOODS_CONVERT_BACK_URL;//跳转到商品兑换完提示
				}else{//存储兑换记录
					GoodsConvertRecord goodsConvertRecord = new GoodsConvertRecord();
					goodsConvertRecord.setGoodId(goodId);
					goodsConvertRecord.setOpenid(openId);
					goodsConvertRecord.setSojumpindex(sojumpindex);//问卷星索引
					goodsConvertRecordMapper.insertSelective(goodsConvertRecord);
				}
			}
			return null;
	}
	
}
