package cc.gemii.service.impl;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.ElecRecordMapper;
import cc.gemii.mapper.RETaskElecRewardMapper;
import cc.gemii.mapper.TaskElecRewardMapper;
import cc.gemii.po.ElecRecord;
import cc.gemii.po.TaskElecReward;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.ElecRecordDTO;
import cc.gemii.pojo.PagingPOJO;
import cc.gemii.pojo.TaskElecRewardDTO;
import cc.gemii.pojo.TaskElecRewardOTD;
import cc.gemii.service.TaskElecRewardService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.StrConsts;
import cc.gemii.utils.export.ManySheetExcelUtils;
import cc.gemii.utils.export.ManySheetExportUtils;
import cc.gemii.utils.export.PropSetterUtils;

import com.zyt.mybatis.plugin.Page;

/**
 * <p>ClassName: TaskElecRewardServiceImpl</p>
 * <p>Description: 微任务卡券业务实现</p>
 * <p>Company: gemii</p>
 * @author xudj
 * @date 2017年4月24日 下午3:42:27
 */
@Service
public class TaskElecRewardServiceImpl implements TaskElecRewardService{

	@Autowired
	private TaskElecRewardMapper taskElecRewardMapper;
	@Autowired
	private RETaskElecRewardMapper reTaskElecRewardMapper;
	
	@Autowired
	private ElecRecordMapper elecRecordMapper;
	
	/**
	 * <p>Description: 新增卡券</p>
	 * @author xudj
	 * @date 2017年4月24日 下午3:55:26
	 * @param taskElecReward：卡券信息
	 * @param id：登录信息
	 * @return
	 */
	@Override
	public CommonResult insert(TaskElecReward taskElecReward, Integer id) {
		taskElecReward.setCreateUser(id);
		taskElecReward.setUpdateUser(id);
		// 插入
		int count = taskElecRewardMapper.insertSelective(taskElecReward);
		if(count == 0) {
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		return CommonResult.ok();
	}

	/**
	 * <p>Description: 分页查询卡券列表</p>
	 * @author xudj
	 * @date 2017年4月24日 下午6:09:17
	 * @param taskElecRewardOTD
	 * @return
	 */
	@Override
	public CommonResult list(TaskElecRewardOTD taskElecRewardOTD) {
		Page<TaskElecRewardDTO> page = new Page<TaskElecRewardDTO>(taskElecRewardOTD.getPageNo(), taskElecRewardOTD.getPageSize());
		// 查询数量
		int count = reTaskElecRewardMapper.listCount(taskElecRewardOTD);
		page.setTotal(count);
		if(count == 0){
			return CommonResult.ok(page);
		}
		//查询集合
		List<TaskElecRewardDTO> list = reTaskElecRewardMapper.list(taskElecRewardOTD);
		Map<String, Long> map = null;
		Integer clickTotal = null;
		Integer clickPersonTotal = null;
		for (TaskElecRewardDTO taskElecRewardDTO : list) {
			// 查询浏览信息
			map = reTaskElecRewardMapper.selectClickTotal(taskElecRewardDTO.getTaskId());
			clickTotal = Integer.valueOf(map.get("clickTotal")+"");
			clickPersonTotal = Integer.valueOf(map.get("clickPersonTotal")+"");
			taskElecRewardDTO.setClickTotal(clickTotal);
			taskElecRewardDTO.setClickPersonTotal(clickPersonTotal);
		}
		page.setRows(list);
		return CommonResult.ok(page);
	}

	/**
	 * <p>Description: 查看说明</p>
	 * @author xudj
	 * @date 2017年4月25日 下午3:09:09
	 * @param id
	 * @return
	 */
	@Override
	public CommonResult selectDescrition(Integer id) {
		TaskElecReward taskElecReward = this.taskElecRewardMapper.selectByPrimaryKey(id);
		// 查询失败
		if(taskElecReward == null){
			return CommonResult.build(500, StrConsts.SELECT_REQUEST_ERROR);
		}
		return CommonResult.ok(taskElecReward);
	}

	/**
	 * <p>Description: 更新</p>
	 * @author xudj
	 * @date 2017年4月25日 下午3:43:52
	 * @param taskElecRewardOTD
	 * @return
	 */
	@Override
	public CommonResult update(TaskElecReward taskElecReward) {
		int count = this.taskElecRewardMapper.updateByPrimaryKeySelective(taskElecReward);
		if(count == 0){
			return CommonResult.build(500, StrConsts.UPDATE_REQUEST_ERROR);
		}
		return CommonResult.ok();
	}
	
	/**
	 * <p>Description: 导出卡券记录数据</p>
	 * @author xudj
	 * @date 2017年4月27日 下午4:54:27
	 * @param response
	 * @param idsStr
	 * @throws Exception 
	 */
	@Override
	public void exportElecRecord(HttpServletResponse response, int[] ids) throws Exception {
		OutputStream out = response.getOutputStream();
		ZipOutputStream zipOutputStream = new ZipOutputStream(out);
		String fileName = "卡券点击数据列表" + System.currentTimeMillis() + ".zip";
		response.setContentType("application/octet-stream ");
		response.setContentType("application/OCTET-STREAM;charset=UTF-8");
		response.setHeader("Connection", "close"); // 表示不能用浏览器直接打开
		response.setHeader("Accept-Ranges", "bytes");// 告诉客户端允许断点续传多线程连接下载
		response.setHeader("Content-Disposition", "attachment;filename="
				+ new String(fileName.getBytes("GB2312"), "ISO8859-1"));
		response.setCharacterEncoding("UTF-8");  
  		
  		PropSetterUtils propSetterUtils = new PropSetterUtils();
  		for (Integer id : ids) {
  			// 根据id查询
  			TaskElecReward taskElecReward = this.reTaskElecRewardMapper.selectElecRewardByTaskId(id);
  			if (taskElecReward == null) {
				continue;
			}
  			//查询卡券点击列表
  			List<Map<String, Object>> elecRecordList = this.reTaskElecRewardMapper.exportElecRecord(id);

  			//导出excel
  			List<ManySheetExcelUtils> excelUtils = new ArrayList<ManySheetExcelUtils>();  
  	        excelUtils.add(new ManySheetExcelUtils("卡券点击列表", propSetterUtils.getElecRecordProps(), elecRecordList));  
  			
  	        Workbook workbook = new ManySheetExportUtils().createExcel(excelUtils);
			ZipEntry entry = new ZipEntry(taskElecReward.getElecName() + "_" + CommonUtil.dateFormat(new Date(), CommonUtil.dateFormat_yyMMdd) + "_" + id + ".xls");  
            zipOutputStream.putNextEntry(entry);
            workbook.write(zipOutputStream);
  		}
  		 zipOutputStream.flush();
         zipOutputStream.close();
	}

	/**
	 * <p>Description: 查询浏览记录</p>
	 * @author xudj
	 * @date 2017年4月28日 上午10:07:20
	 * @param id：微任务id
	 * @return
	 */
	@Override
	public CommonResult selectElecRecord(PagingPOJO pagingPOJO, Integer taskId) {
		Page<ElecRecordDTO> page = new Page<ElecRecordDTO>(pagingPOJO.getPageNo(), pagingPOJO.getPageSize());
		// 查询条件
		Map<String, Object> cons = new HashMap<String, Object>();
		cons.put("taskId", taskId);
		cons.put("pageStart", pagingPOJO.getPageStart());
		cons.put("pageSize", pagingPOJO.getPageSize());
		// 查询数量
		int count = this.reTaskElecRewardMapper.selectElecRecordCount(cons);
		page.setTotal(count);
		if(count == 0){
			return CommonResult.ok(page);
		}
		// 查询集合
		List<ElecRecordDTO> list = this.reTaskElecRewardMapper.selectElecRecord(cons);
		page.setRows(list);
		return CommonResult.ok(page);
	}

	/**
	 * <p>Description:添加点击记录</p>
	 * @author xudj
	 * @date 2017年5月3日 上午10:54:46
	 * @param id
	 * @param openid
	 */
	@Override
	public void insertElecRecord(Integer id, String openid) {
		ElecRecord elecRecord = new ElecRecord();
		elecRecord.setOpenid(openid);
		elecRecord.setTaskId(id);
		try {
			elecRecordMapper.insertSelective(elecRecord);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * <p>Description: 查询所以卡券列表</p>
	 * @author xudj
	 * @date 2017年5月3日 上午11:44:04
	 * @return
	 */
	@Override
	public CommonResult selectAllElecReward() {
		return CommonResult.ok(reTaskElecRewardMapper.selectAllElecReward());
	}

	/**
	 * <p>Description: 根据微任务id查询卡券链接</p>
	 * @author xudj
	 * @date 2017年5月4日 上午10:38:51
	 * @param id
	 * @return
	 */
	@Override
	public TaskElecReward selectElecRewardByTaskId(Integer id) {
		return reTaskElecRewardMapper.selectElecRewardByTaskId(id);
	}
	
}
