package cc.gemii.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import cc.gemii.component.JedisClient;
import cc.gemii.service.ApiService;
import cc.gemii.utils.HttpClientUtil;
import cc.gemii.utils.UrlConsts;

/**
 * 各种api业务
 * @author xudj20170222
 */
@Service
public class ApiServiceImpl implements ApiService{

	@Autowired
	private JedisClient jedisClient;
	
	@SuppressWarnings("unchecked")
	@Override
	public String getAccessTokenByFw() {
		String fw_access_token = jedisClient.get("fwAccessToken");
		if(fw_access_token == null){
			//从weixin项目拿取数据
			String resJson = HttpClientUtil.doGet(UrlConsts.GET_ACCESSTOKEN_TIME_FW);
			Map<String, Object> res = (Map<String, Object>) JSON.parse(resJson);
			fw_access_token = (String) res.get("fwAccessToken");
			String expireTime = (String) res.get("expireTime");
 			jedisClient.set("fwAccessToken", fw_access_token);
			jedisClient.expire("fwAccessToken", Integer.valueOf(expireTime));
			return fw_access_token;
		}else {
			return fw_access_token;
		}
	}

}
