package cc.gemii.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.DictMapper;
import cc.gemii.mapper.OtherMapper;
import cc.gemii.po.Dict;
import cc.gemii.po.DictExample;
import cc.gemii.po.ImageQrcode;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.DictService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.StrConsts;

/**
 * 字典业务层
 * @author xudj20170217
 */
@Service
public class DictServiceImpl implements DictService{
	
	@Autowired
	private DictMapper dictMapper;
	
	@Autowired
	private OtherMapper otherMapper;

	/**
	 * 根据类型查询
	 * @author xudj20170217
	 */
	@Override
	public List<Dict> getDictsByType(String type) {
		DictExample example = new DictExample();
		example.createCriteria().andTypeEqualTo(type);
		return dictMapper.selectByExample(example);
	}
	
	/**
	 * 根据主键查询
	 * @return
	 */
	@Override
	public Dict getDictById(Integer id){
		return dictMapper.selectByPrimaryKey(id);
	}

	/**
	 * 查询什么是栗子积分规则
	 * @param memberId:未处理(拦截器日志记录)
	 */
	@Override
	public CommonResult getChestnutExplain() {
		List<Map<String, String>> res = new ArrayList<Map<String,String>>();
		//参数
		Map<String, String> cons = new HashMap<String, String>();
		cons.put("invite", StrConsts.DICT_TYPE_INVITE);
		cons.put("register", StrConsts.DICT_TYPE_REGISTER);
		cons.put("questionsurvey", StrConsts.DICT_TYPE_QUESTION_SURVEY);
		cons.put("shareforward", StrConsts.DICT_TYPE_SHARE_FORWARD);
		cons.put("integralrule", StrConsts.DICT_TYPE_INTEGRALRULE);
		cons.put("evpi", StrConsts.DICT_TYPE_EVPI);
		//查询
		res = otherMapper.getChestnutExplain(cons);
		return CommonResult.ok(res);
	}

	/**
	 * 新增dict
	 */
	@Override
	public CommonResult insertDictForQrcode(Dict dict, ImageQrcode imageQrcode) {
		//生成随机数字，作为二维码的场景值
		String sceneId = this.getQrsceneId();
		dict.setValue(sceneId);
		//根据type、name、value，查询唯一
		DictExample example = new DictExample();
		example.createCriteria().andTypeEqualTo(dict.getType())
			.andValueEqualTo(dict.getValue());
		List<Dict> dicts = dictMapper.selectByExample(example);
		//已经存在相同场景数据，请重新设置
		if(dicts != null && dicts.size() > 0){
			return CommonResult.build(Integer.valueOf(400), "已经存在相同场景数据，新增失败");
		}
		dict.setDescription("服务号粉丝来源");
		int count = otherMapper.insertDictGetKey(dict);
		if(count == 0) {
			return CommonResult.build(Integer.valueOf(500), StrConsts.HTTP_REQUEST_ERROR);
		}
		return CommonResult.ok();
	}
	
	/**
	 * dict获取唯一临时二维码场景值
	 * @author xudj20170327
	 */
	private String getQrsceneId(){
		while(true){
			//获取一个随机数
			String scene_id = "2" + CommonUtil.getRamomIntByLen(7);
			//根据dict的value查询
			DictExample example = new DictExample();
			example.createCriteria().andTypeEqualTo(StrConsts.DICT_TYPE_FANS_FW_SOURCE)
				.andValueEqualTo(scene_id);
			List<Dict> list = dictMapper.selectByExample(example);
			if(list == null || list.size() == 0){
				return scene_id;
			}
		}
	}

	/**
	 * 更新粉丝来源的dict
	 * @param description：二维码描述
	 * @param sceneValue：场景值
	 * @param dictSourceId：粉丝来源
	 */
	@Override
	public void updateDictForFansSource(String description, Integer dictSourceId, String sceneValue) {
		Dict dict = dictMapper.selectByPrimaryKey(dictSourceId);
		if(dict == null){
			return;
		}
		//更新
		dict.setName(description + "_" + sceneValue);
		dictMapper.updateByPrimaryKey(dict);
	}
	
}