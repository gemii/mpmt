package cc.gemii.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.OtherMapper;
import cc.gemii.mapper.REImageUploadMapper;
import cc.gemii.mapper.RETaskMemberRecordMapper;
import cc.gemii.mapper.TaskElecRewardMapper;
import cc.gemii.mapper.TaskMemberRecordMapper;
import cc.gemii.mapper.TasksMapper;
import cc.gemii.po.TaskElecReward;
import cc.gemii.po.TaskMemberRecord;
import cc.gemii.po.TaskMemberRecordExample;
import cc.gemii.po.Tasks;
import cc.gemii.po.TasksWithBLOBs;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.PagingPOJO;
import cc.gemii.pojo.TemplateVerifyRemind;
import cc.gemii.service.IntegrationRecordService;
import cc.gemii.service.TaskMemberRecordService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.StrConsts;
import cc.gemii.utils.TemplateUtil;
import cc.gemii.utils.UrlConsts;

import com.zyt.mybatis.plugin.Page;

@Service
public class TaskMemberRecordServiceImpl implements TaskMemberRecordService {
	
	@Autowired
	private TaskMemberRecordMapper taskMemberRecordMapper;
	@Autowired
	private RETaskMemberRecordMapper reTaskMemberRecordMapper;
	@Autowired
	private REImageUploadMapper reImageUploadMapper;
	@Autowired
	private TasksMapper tasksMapper;
	@Autowired
	private IntegrationRecordService integrationRecordService;
	@Autowired
	private OtherMapper otherMapper;
	
	@Autowired
	private TaskElecRewardMapper taskElecRewardMapper;
	
	/**
	 * 后台管理处理审核结果实现
	 * @author lvshilin20170210
	 * @change xudj20170217
	 */
	public CommonResult updateResult(Long id, String result) {
		//查询状态是否是待审核
		TaskMemberRecord taskMemberRecord = taskMemberRecordMapper.selectByPrimaryKey(id);
		String memberId = taskMemberRecord.getMemberId();
		Integer state = taskMemberRecord.getState();
		if(state == null || state == 0){
			return CommonResult.build(400, "不可审核");
		}
		if(state == 2){
			return CommonResult.build(400, "不可审核已通过任务");
		}
		taskMemberRecord.setVerifyTime(new Date());//审核时间
		if(result.equals("T")){
			taskMemberRecord.setState(2);//状态(2审核通过)
		}else{
			taskMemberRecord.setState(3);//状态(3审核不通过)
		}
		//更新领取记录
		int count = taskMemberRecordMapper.updateByPrimaryKeySelective(taskMemberRecord);
		if(count == 0){
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		//查询openid
		String openid = reTaskMemberRecordMapper.selectOpenidById(id);
		//通过后添加积分记录
		Tasks tasks = tasksMapper.selectByPrimaryKey(taskMemberRecord.getTaskId());
		//审核通过
		if(result.equals("T")){
			//有积分奖励的情况下，添加总积分
			Integer integration = tasks.getIntegration();
			if(integration != null && integration != 0){
				boolean flag = this.integrationRecordService.insertIntegrationRecord(memberId, tasks);
				if(!flag){//false表示已经查过审核的该任务
					return CommonResult.ok();
				}
				otherMapper.updateMemberIntegration(memberId, integration);
			}
			String remark = "耶，恭喜您获得REWARD，请再接再厉！点击查看明细~";
			String reward = "";	//奖励
			String url = null;	//模板url
			if(integration != null && integration != 0){
				reward = integration + "积分";
				url = UrlConsts.INTEGRATION_RECOED_FROM_VERIFY_PASS;
			}else if(tasks.getElecRewardId() != null){
				// 电子券
				TaskElecReward taskElecReward = taskElecRewardMapper.selectByPrimaryKey(tasks.getElecRewardId());
				if(taskElecReward == null){
					return CommonResult.build(400, "模板发送失败，电子券未找到");
				}
				reward = taskElecReward.getElecName();
				remark = "恭喜您获得REWARD优惠券，点击可领取。";
				url = UrlConsts.GET_LOAD_ELEC_URL.replace("TASKID", tasks.getId()+"")
						.replace("OPENID", openid);	// 领取卡券的路径
			}else if(CommonUtil.hasText(tasks.getEntityReward())){
				reward = tasks.getEntityReward();
			}
			// 发送审核通过模板消息(只能发一次)
			TemplateVerifyRemind verifyRemind = new TemplateVerifyRemind();
			verifyRemind.setFirst("亲爱的妈妈，您提交的任务\""+ tasks.getTitle() +"\"已审核");
			verifyRemind.setKeyword1("已通过");
			verifyRemind.setKeyword2(CommonUtil.dateFormat(new Date(), CommonUtil.dateFormat_yyyy_MM_ddHHmm));
			verifyRemind.setRemark(remark.replace("REWARD", reward));
			
			// add by xudj20170511 母亲节活动时间判断 start
			Calendar startCalendar = new GregorianCalendar(2017, 4, 12, 10, 0, 0);
			Calendar endCalendar = new GregorianCalendar(2017, 4, 15, 23, 59, 59);
			Date startDate = new Date(startCalendar.getTimeInMillis());
			Date endDate = new Date(endCalendar.getTimeInMillis());
			Date nowDate = new Date();
			if(startDate.before(nowDate) && endDate.after(nowDate)){//活动期间
				verifyRemind.setRemark("恭喜您获得"+integration+"颗栗子，母亲节期间可以参与相关抽奖哦！点击查看明细~");
				url = UrlConsts.MOAC_ACTIVITY_H5_URL;
			}
			// add by xudj20170511 母亲节活动时间判断 end
			TemplateUtil.sendTemplateGen(openid, verifyRemind.getFieldValueMap(), verifyRemind.getTemplateId(), url);
		}else{
			// 发送审核不通过模板消息
			TemplateVerifyRemind verifyRemind = new TemplateVerifyRemind();
			verifyRemind.setFirst("亲爱的妈妈，您提交的任务\""+ tasks.getTitle() +"\"已审核");
			verifyRemind.setKeyword1("未通过，截图不符合任务要求");
			verifyRemind.setKeyword2(CommonUtil.dateFormat(new Date(), CommonUtil.dateFormat_yyyy_MM_ddHHmm));
			verifyRemind.setRemark("别灰心，您离成功仅一步之遥，机会多多，还有其它任务等着您！");
			TemplateUtil.sendTemplateGen(openid, verifyRemind.getFieldValueMap(), verifyRemind.getTemplateId(), UrlConsts.PERSON_URL_FROM_VERIFY_NOTPASS);
		}
		return CommonResult.ok();
	}

	/**
	 * 判断任务是否已下架
	 * 否更新任务提交记录
	 * @author xudj20170214
	 */
	@Override
	public CommonResult updatePutTask(TaskMemberRecord record) {
		//条件更新
		TaskMemberRecordExample example = new TaskMemberRecordExample();
		example.createCriteria().andMemberIdEqualTo(record.getMemberId())
				.andTaskIdEqualTo(record.getTaskId());
		int count = taskMemberRecordMapper.updateByExampleSelective(record, example);
		if(count == 0){
			return CommonResult.build(Integer.valueOf(500), StrConsts.HTTP_REQUEST_ERROR);
		}
		return CommonResult.ok();
	}

	/**
	 * 领取微任务
	 * @author xudj20170214
	 */
	@Override
	public CommonResult insertTaskMemberRecord(String memberId, Integer taskId) {
		//判断任务是否已经领取了
		TaskMemberRecordExample example = new TaskMemberRecordExample();
		example.createCriteria().andTaskIdEqualTo(taskId)
			.andMemberIdEqualTo(memberId);
		int recordCount = taskMemberRecordMapper.countByExample(example);
		if(recordCount != 0){
			return CommonResult.build(Integer.valueOf(400), "不可重复领取");
		}
		//插入
		TaskMemberRecord record = new TaskMemberRecord();
		record.setReceiveTime(new Date());
		record.setState(0);		//状态为任务中
		record.setMemberId(memberId);
		record.setTaskId(taskId);
		int count = taskMemberRecordMapper.insert(record);
		if(count == 0){
			return CommonResult.build(Integer.valueOf(500), StrConsts.HTTP_REQUEST_ERROR);
		}
		return CommonResult.ok();
	}
	/**
	 * 选中任务被 领取管理 展示
	 * @author lvshilin20170215
	 */
	@Override
	public CommonResult selectTasksMembers(Integer taskId) {
		//返回数据
		Map<String, Object> res = new HashMap<String, Object>();
		//根据任务id查询任务信息
		TasksWithBLOBs tasks = tasksMapper.selectByPrimaryKey(taskId);
		//处理任务奖励
		Integer integration = tasks.getIntegration();
		String entityReward = tasks.getEntityReward();
		Integer elecRewardId = tasks.getElecRewardId();
		String reward = "";//奖励
		if(integration != null && integration != 0){//积分
			reward = reward + integration + "积分｜";
		}
		if(CommonUtil.hasText(entityReward)){//实物
			reward = reward + entityReward + "｜";
		}
		if(elecRewardId != null){//电子卷
			TaskElecReward taskElecReward = this.taskElecRewardMapper.selectByPrimaryKey(elecRewardId);
			reward = reward + (taskElecReward.getElecName()==null?"":taskElecReward.getElecName()) + "｜";
		}
		if(CommonUtil.hasText(reward)){
			reward = reward.substring(0, reward.lastIndexOf("｜"));
		}
		res.put("reward", reward);
		res.put("title", tasks.getTitle());
		//总领取记录数
		List<Map<String, Object>> list = reTaskMemberRecordMapper.selectTasksMembers(taskId);
		res.put("tmrs", list);
		return CommonResult.ok(res);
	}
	
	/**
	 * 查询任务上传详情
	 * @author lvshilin20170216
	 * @change xudj20170217
	 */
	@Override
	public CommonResult selectTaskPutDetail(Long id) {
		Map<String, Object> resMap = new HashMap<String, Object>(); 
		//查询任务信息
		Map<String, Object> task = reTaskMemberRecordMapper.selectTaskPutDetail(id);
		if(task == null){
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		resMap.put("task", task);
		//查询图片
		resMap.put("images", null);
		String outId = (String)task.get("outId");
		if(CommonUtil.hasText(outId)){
			List<Map<String, Object>> images = reImageUploadMapper.selectTaskPutImagesByOutId(outId);
			resMap.put("images", images);
		}
		return CommonResult.ok(resMap);
	}

	/**
	 * 查询个人中心历史任务列表
	 */
	@Override
	public Page<Map<String, Object>> listTaskMembersByMemberId(PagingPOJO pagingPojo, String memberId) {
		Page<Map<String, Object>> pages = new Page<Map<String,Object>>(pagingPojo.getPageNo(), pagingPojo.getPageSize());
		//查询条件
		Map<String, Object> cons = new HashMap<String, Object>();
		cons.put("pageStart", pagingPojo.getPageStart());
		cons.put("pageSize", pagingPojo.getPageSize());
		cons.put("memberId", memberId);
		
		//查询条数
		int count = this.reTaskMemberRecordMapper.countTaskMembersByMemberId(cons);
		pages.setTotal(count);
		if(count == 0){//无更多数据
			return pages;
		}
		//总记录数
		pages.setRows(this.reTaskMemberRecordMapper.listTaskMembersByMemberId(cons));
		return pages;
	}
	
	/**
	 * 根据任务id及用户标识id查询领取记录
	 * @author xudj20170308
	 */
	@Override
	public TaskMemberRecord selectByTaskIdAndMemberId(Integer taskId, String memberId) {
		TaskMemberRecordExample example = new TaskMemberRecordExample();
		example.createCriteria().andTaskIdEqualTo(taskId)
				.andMemberIdEqualTo(memberId);
		List<TaskMemberRecord> list = taskMemberRecordMapper.selectByExample(example);
		if(list == null || list.size() == 0){
			return null;
		}
		return list.get(0);
	}

}
