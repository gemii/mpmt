package cc.gemii.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.ImageUploadMapper;
import cc.gemii.mapper.REImageUploadMapper;
import cc.gemii.po.ImageUpload;
import cc.gemii.po.ImageUploadExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.ImageUploadService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.FileUtil;
import cc.gemii.utils.StrConsts;

/**
 * 图片上传业务层
 * @author xudj20170207
 */
@Service
public class ImageUploadServiceImpl implements ImageUploadService{
	
	private static final Logger log = LoggerFactory.getLogger(ImageUploadServiceImpl.class);
	
	@Autowired
	private ImageUploadMapper imageUploadMapper;
	@Autowired
	private REImageUploadMapper reImageUploadMapper;


	/**
	 * 上传单张图片
	 * @param data base64数据
	 * @author xudj20170221
	 */
	@Override
	public CommonResult insertImage(String data, String dir) {
		log.info("上传base64图片开始.");
		Base64 base64 = new Base64();
		//存储实体
		ImageUpload imageUpload = new ImageUpload();
		imageUpload.setCreateTime(new Date());
		imageUpload.setUseRemark("微任务");
		try {
			//image/jpeg;base64,
			String data1 = data.substring(0, data.indexOf(",") + 1);
			String fileName = CommonUtil.getRandomNum(6) + CommonUtil.dateFormat(new Date(), CommonUtil.dateFormat_yyyyMMddHHmmss);
			String fileNameSuffix = data1.substring(data1.indexOf("/")+1, data1.indexOf(";"));
			fileName = fileName + "." + fileNameSuffix;
			//数据
			String data2 = data.substring(data1.length());
			byte[] b = base64.decode(data2);
			//上传文件,获取访问路径
			String imagePath = FileUtil.uploadFile(dir, fileName, b);
			
			//保存到数据库
			imageUpload.setFileName(fileName);
			imageUpload.setFilePath(imagePath);
			this.reImageUploadMapper.insertImageGetId(imageUpload);
		} catch (Exception e) {
			log.error("存储base64类型的图片异常");
			e.printStackTrace();
			return CommonResult.build(Integer.valueOf(500), "图片上传异常");
		}
		//成功
		Long id = imageUpload.getId();
		if(id == null){
			return CommonResult.build(Integer.valueOf(500), StrConsts.HTTP_REQUEST_ERROR);
		}
		return CommonResult.ok(id);
	}

	/**
	 * 线程安全 获取唯一外键
	 * @return
	 */
	public synchronized String getUniqueOutId(){
		while(true){
			String outId = CommonUtil.getRandomNum(8);
			ImageUploadExample example = new ImageUploadExample();
			example.createCriteria().andOutIdEqualTo(outId);
			List<ImageUpload> list = imageUploadMapper.selectByExample(example);
			if(list == null || list.size() == 0){
				return outId;
			}
		}
	}

	/**
	 * 根据主键更新outid
	 * @author xudj20170221
	 * @param data:上传图片记录表的主键
	 */
	@Override
	public String updateImagesOutId(String[] data) {
		List<Integer> ids = new ArrayList<Integer>();
		for (String id : data) {
			if (!CommonUtil.hasText(id)) {
				log.info("获取上传图片的外键有空串!");
				continue;
			}
			ids.add(Integer.valueOf(id));
		}
		if(ids.size() == 0){
			log.info("获取上传图片的异常了，都为空串!");
			return null;
		}
		String outId = this.getUniqueOutId();
		//更新
		this.reImageUploadMapper.updateOutIdByIds(ids, outId);
		return outId;
	}
	
}