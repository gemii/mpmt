package cc.gemii.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.OtherMapper;
import cc.gemii.mapper.REIntegrationRecordMapper;
import cc.gemii.mapper.RETasksMapper;
import cc.gemii.mapper.TaskMemberRecordMapper;
import cc.gemii.mapper.TasksMapper;
import cc.gemii.po.TasksWithBLOBs;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.TaskSearchPOJO;
import cc.gemii.pojo.TasksVO;
import cc.gemii.service.TasksService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.StrConsts;

import com.zyt.mybatis.plugin.Page;

@Service
public class TasksServiceImpl implements TasksService {

	@Autowired
	private TasksMapper tasksMapper;
	@Autowired
	private RETasksMapper reTasksMapper;
	@Autowired
	private OtherMapper otherMapper;
	@Autowired
	private REIntegrationRecordMapper reIntegrationRecordMapper;
	@Autowired
	private TaskMemberRecordMapper taskMemberRecordMapper;

	/**
	 * 发布一个微任务服务层实现
	 * 
	 * @author lvshilin20170207
	 * @change xudj
	 */
	public CommonResult insertTasks(TasksWithBLOBs tasks, String shelveDateStr,
			String endDateStr, String applyEndDateStr, String[] keywords) {
		//处理多个关键词
		this.getKeywordIds(tasks, keywords);
		// 上架时间
		Date shelveDate = CommonUtil.String2Date(shelveDateStr, CommonUtil.dateFormat_yyyy_MM_ddHHmmss);
		tasks.setShelveDate(shelveDate);
		tasks.setShelveDateFirst(shelveDate);
		tasks.setApplyEndDate(CommonUtil.String2Date(applyEndDateStr, CommonUtil.dateFormat_yyyy_MM_ddHHmmss));
		tasks.setEndDate(CommonUtil.String2Date(endDateStr, CommonUtil.dateFormat_yyyy_MM_ddHHmmss));
		tasks.setCreateTime(new Date());
		tasks.setUpdateTime(new Date());
		tasks.setIsDel("F");// 默认f
		tasks.setIsShelve("F");// 默认下架
		//申领剩余份数
		if(tasks.getApplyTotal() != null){
			tasks.setApplyCount(tasks.getApplyTotal());
		}
		int taskNum = reTasksMapper.insertTasksGetId(tasks);
		if (taskNum == 0 || tasks.getId() == null) {
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		//返回数据
		Map<String, Object> res = new HashMap<String, Object>();
		res.put("taskId", tasks.getId());
		res.put("isShelve", tasks.getIsShelve());
		return CommonResult.ok(res);
	}

	/**
	 * 根据搜索条件查询微任务
	 * 
	 * @author lvshilin20170213
	 */
	public Page<Map<String, Object>> getTasksManage(TaskSearchPOJO taskSearchPOJO) {
		Page<Map<String, Object>> pages = new Page<Map<String, Object>>(
				taskSearchPOJO.getPageNo(), taskSearchPOJO.getPageSize());
		// 查询条数
		int count = this.reTasksMapper.getTasksManageCount(taskSearchPOJO);
		pages.setTotal(count);
		if (count == 0) {// 无更多数据
			return pages;
		}
		List<Map<String, Object>> res = this.reTasksMapper.getTasksManageList(taskSearchPOJO);
		String keywordName = "";//dict的value值
		Integer integration = null;//积分
		String elecReward = null;//电子卷
		String entityReward = null;//实物
		for (Map<String, Object> map : res) {
			String keyword = "";
			String keywordIds = (String) map.get("keywordName");//type的ids
			if(keywordIds == null){
				continue;
			}
			//将关键词加到keywordIds中
			String[] keywords = keywordIds.split(";");//分割
			for (String str : keywords) {
				keywordName = otherMapper.selectDictValueById(Integer.valueOf(str));
				keyword = keyword + keywordName + "｜";
			}
			keyword = keyword.substring(0, keyword.lastIndexOf("｜"));
			//关键词放入map中
			map.put("keywordName", keyword);
			//处理任务奖励
			integration = (Integer) map.get("integration");
			entityReward = (String) map.get("entityReward");
			elecReward = (String) map.get("elecReward");
			String reward = "";//奖励
			if(CommonUtil.hasText(integration)){//积分
				reward = reward + integration + "积分｜";
			}
			if(CommonUtil.hasText(entityReward)){//实物
				reward = reward + entityReward + "｜";
			}
			if(CommonUtil.hasText(elecReward)){//电子卷
				reward = reward + elecReward + "｜";
			}
			if(CommonUtil.hasText(reward)){
				reward = reward.substring(0, reward.lastIndexOf("｜"));
			}
			//任务奖励放入map中
			map.put("integration", reward);
		}
		// 总记录数
		pages.setRows(res);
		return pages;
	}
	
	/**
	 * 通过任务id查询任务信息
	 * 
	 * @author xudj20170214
	 */
	@Override
	public String selectTasksIsShelveById(Integer taskId) {
		return reTasksMapper.selectTasksIsShelveById(taskId);
	}

	/**
	 * 通过任务id查询任务信息 初始化领取任务界面数据
	 * 
	 * @author xudj20170214
	 */
	@Override
	public CommonResult selectTasksToReceive(Integer taskId, String memberId) {
		Map<String, Object> res = reTasksMapper.selectTasksToReceive(taskId, memberId);
		String operateRemark = (String) res.get("operateRemark");
		if(operateRemark != null){
			operateRemark = operateRemark.replace("\n", "<br>");
			res.put("operateRemark", operateRemark);
		}
		// 返回数据
		return CommonResult.ok(res);
	}

	/**
	 * 通过任务id查询任务信息 初始化提交任务界面数据
	 * 
	 * @author xudj20170214
	 */
	@Override
	public CommonResult selectTasksToPut(Integer taskId) {
		TasksWithBLOBs tasksWithBLOBs = tasksMapper.selectByPrimaryKey(taskId);
		if (tasksWithBLOBs == null) {
			return CommonResult.build(Integer.valueOf(500), StrConsts.HTTP_REQUEST_ERROR);
		}
		// 返回数据
		Map<String, Object> res = new HashMap<String, Object>();
		res.put("isShelve", tasksWithBLOBs.getIsShelve());// 是否上下架
		res.put("shelveDateStr", CommonUtil.dateFormat(tasksWithBLOBs.getShelveDate(), CommonUtil.dateFormat_yyyy$MM$ddHHmm));
		res.put("endDateStr", CommonUtil.dateFormat(tasksWithBLOBs.getEndDate(), CommonUtil.dateFormat_yyyy$MM$ddHHmm));
		res.put("putRemark", tasksWithBLOBs.getPutRemark());
		return CommonResult.ok(res);
	}

	/**
	 * 查询个人中心微任务列表
	 * 
	 * @author xudj20170214
	 */
	@Override
	public CommonResult selectPersonTasksInit(String memberId) {
		// 查询上架的微任务及下架待提交微任务
		List<TasksVO> tasks = reTasksMapper.selectPersonTasksInit(memberId);
		return CommonResult.ok(tasks);
	}

	/**
	 * 查询我的栗子界面的数据信息
	 * 
	 * @author xudj20170215
	 */
	@Override
	public CommonResult selectMyIntegraTasksInit(String memberId) {
		// 查询个人信息初始化我的栗子界面
		Map<String, Object> member = otherMapper.selectMemberInfo(memberId);
		if (member == null) {
			return CommonResult.build(Integer.valueOf(500), StrConsts.HTTP_REQUEST_ERROR);
		}
		// 查询最近七天积分
		List<String> integraLaskWeeks = reIntegrationRecordMapper
				.selectIntegraLaskWeekByMemberId(memberId);
		int integraLaskWeek = 0;// 上周积分
		String integraLaskWeekStr = "0";
		for (String integration : integraLaskWeeks) {
			if(integration == null || integration.length() < 2){
				continue;
			}
			String operate = integration.substring(0, 1);
			int num = Integer.valueOf(integration.substring(1));
			if ("+".equals(operate) || "＋".equals(operate)) {
				integraLaskWeek += num;
			} else if ("-".equals(operate) || "－".equals(operate)) {
				integraLaskWeek -= num;
			}
		}
		if (integraLaskWeek > 0) {
			integraLaskWeekStr = "+" + integraLaskWeek;
		} else if (integraLaskWeek < 0) {
			integraLaskWeekStr = "-" + integraLaskWeek;
		}
		member.put("integraLaskWeek", integraLaskWeekStr);
		// 查询我的栗子界面微任务信息
		List<Map<String, Object>> tasks = reTasksMapper
				.selectMyIntegraTasksInit(memberId);
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("member", member);
		resMap.put("tasks", tasks);
		return CommonResult.ok(resMap);
	}

	/**
	 * 根据主键id查询任务信息
	 * 
	 * @author lvshilin20170215
	 */
	@Override
	public TasksWithBLOBs queryTaskById(Integer id) {
		return tasksMapper.selectByPrimaryKey(id);
	}

	/**
	 * 编辑保存
	 * 
	 * @author lvshilin20170215
	 */
	@Override
	public int updateTask(TasksWithBLOBs tasks, String shelveDateStr, String endDateStr, String applyEndDateStr, String[] keywords) {
		//处理多个关键词
		this.getKeywordIds(tasks, keywords);
		// 上架时间
		Date shelveDate = CommonUtil.String2Date(shelveDateStr, CommonUtil.dateFormat_yyyy_MM_ddHHmmss);
		tasks.setShelveDate(shelveDate);
		tasks.setApplyEndDate(CommonUtil.String2Date(applyEndDateStr, CommonUtil.dateFormat_yyyy_MM_ddHHmmss));
		tasks.setEndDate(CommonUtil.String2Date(endDateStr, CommonUtil.dateFormat_yyyy_MM_ddHHmmss));
		//更新时间
		tasks.setUpdateTime(new Date());
		//查询并赋值原有数据
		TasksWithBLOBs tasksWithBlobs = tasksMapper.selectByPrimaryKey(tasks.getId());
		tasks.setCreateTime(tasksWithBlobs.getCreateTime());
		tasks.setShelveDateFirst(tasksWithBlobs.getShelveDateFirst());
		tasks.setIsDel("F");
		if(CommonUtil.hasText(tasks.getApplyEndDate())){//试用类型
			tasks.setApplyCount(tasksWithBlobs.getApplyCount());
		}
		return tasksMapper.updateByPrimaryKeyWithBLOBs(tasks);
	}

	/**
	 * 上下架按钮实现
	 * 
	 * @author lvshilin20170215
	 */
	@Override
	public CommonResult updateIsShelve(Integer id, String isShelve) {
		TasksWithBLOBs task = tasksMapper.selectByPrimaryKey(id);
		task.setIsShelve(isShelve);
		if (isShelve.equals("T")) {
			// 任务时间
			Date nowDate = new Date();
			Date shelveTime = task.getShelveDate();
			Date endTime = task.getEndDate();
			if (nowDate.before(shelveTime) || nowDate.after(endTime)) {
				return CommonResult.build(400, "当前时间不可上架");
			}
		}
		int count = tasksMapper.updateByPrimaryKeySelective(task);
		if (count == 0) {
			return CommonResult.build(500, StrConsts.HTTP_REQUEST_ERROR);
		}
		return CommonResult.ok();
	}

	/**
	 * 删除按钮实现
	 * 
	 * @author lvshilin20170216
	 */
	@Override
	public int deleteTask(Integer id, String isDel) {
		TasksWithBLOBs tasks = tasksMapper.selectByPrimaryKey(id);
		tasks.setIsDel(isDel);
		if (isDel.equals("T")) {
			tasks.setIsShelve("F");// 删除同时下架
		}
		return tasksMapper.updateByPrimaryKeySelective(tasks);
	}

	/**
	 * 处理关键词列表
	 * @author xudj20172028
	 * @param tasks:任务
	 * @param keywords：关键词集合
	 */
	private void getKeywordIds(TasksWithBLOBs tasks, String[] keywords) {
		//处理keywords
		String keywordIds = "";
		for (String keyword : keywords) {
			keywordIds = keywordIds + keyword + ";";
		}
		//去掉最后一个;
		if(!keywordIds.equals("")){
			keywordIds  = keywordIds.substring(0, keywordIds.lastIndexOf(";"));
		}
		tasks.setKeywordId(keywordIds);//设置关键词
	}
}
