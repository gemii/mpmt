package cc.gemii.service;

import cc.gemii.po.SysUser;


/**
 * 系统登录用户业务
 * @author xudj
 */
public interface SysUserService {

	SysUser selectForLogin(String username, String password);

}
