package cc.gemii.service;

import cc.gemii.po.TaskMemberRecord;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.PagingPOJO;


public interface TaskMemberRecordService {

	CommonResult updateResult(Long id,String result);

	CommonResult updatePutTask(TaskMemberRecord taskMemberRecord);

	CommonResult insertTaskMemberRecord(String memberId, Integer taskId);

	CommonResult selectTasksMembers(Integer taskId);

	CommonResult selectTaskPutDetail(Long id);

	Object listTaskMembersByMemberId(PagingPOJO pagingPojo, String memberId);

	TaskMemberRecord selectByTaskIdAndMemberId(Integer taskId, String memberId);

}
