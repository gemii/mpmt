package cc.gemii.service;

import java.util.List;

import cc.gemii.po.Dict;
import cc.gemii.po.ImageQrcode;
import cc.gemii.pojo.CommonResult;

public interface DictService {

	List<Dict> getDictsByType(String type);

	Dict getDictById(Integer typeId);

	CommonResult getChestnutExplain();

	CommonResult insertDictForQrcode(Dict dict, ImageQrcode imageQrcode);

	void updateDictForFansSource(String description, Integer dictSourceId,
			String sceneValue);
	
}
