#!/usr/bin/env python
# encoding=utf-8
import requests
import sys

def download_page(url):
    data = requests.get(url).content
    return data

# get argv
url = sys.argv[1]
file_name = sys.argv[2]

importFile = '<script src="js/jquery-3.0.0.min.js"></script><script src="js/jweixin-1.0.0.js"></script><script src="js/share.js"></script>'
html = download_page(url)
html = html.replace('data-src="','src="http://read.html5.qq.com/image?src=forum&q=5&r=0&imgflag=7&imageUrl=')
html = html.replace(',false);</script>',',false);</script>'+importFile)


# write html file
try:
	with open(file_name, "w") as f:
		f.write(html);
except IOError:
	# exception
    print "10001"
else:
    print "10000"